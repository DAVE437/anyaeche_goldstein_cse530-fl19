#pragma once
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <assert.h>
#include <errno.h>
#include "tupleDesc.h"
#include "catalog.h"
#include "tuple.h"


#define doWB 0
#define pagesize 4096

typedef unsigned char tuple;
typedef unsigned char page;


typedef struct thrHeapFile{
  FILE* fp;
  table* myTable;
  item* curItem;
  int curPageNum;
}thrHeapFile;

typedef struct heapFile{
  FILE* fp;
  char* fName;
  char* fullPath;
  volatile unsigned long numPages;
  int pageSize;
  int numSlots;
  int headOffset;
  tupleDesc* desc;
}heapFile;
struct retTuple;
namespace HF{


  //creates a new heapfile given a name and field restrictions (this initializes a new heapfile!)
  heapFile* createHeapFile(catalog_t* log, char* name, tupleDesc* fields);

  //creates the heapfile struct for an existing heapfile (this is for reaccess)
  //cur page will start at page 0
  heapFile* reaccessHeapFile(catalog_t* log, char* name);
  
  //returns the file* for a given heapFile
  FILE* getFile(heapFile* hf);

  //returns the offset from heapfile start to a given page given a
  //page number
  unsigned long getPageOffset(heapFile* hf, int pageNum);
  
  //returns the field description for a given heap
  tupleDesc* getTupleDesc(heapFile* hf);

  //extend heapfile by n bytes
  int extendHeapFile(heapFile* hf, int n);
  
  //returns an array of bytes size PAGE_SIZE for a given pageNum
  page* readPage(thrHeapFile* lhf, int pageNum);

  //returns the uniqueID for a given heapfile
  int getID(catalog_t* log, heapFile* hf);

  //writes a page to the heapfile, this will always be pagesize
  int writePage(heapFile* hf, FILE* lfp, page* pg, int pageNum);

    //writes current hf page
  int writePage(heapFile* hf);

  //returns bit mask of page
  int getBitMask(page* pg);
  
  //returns true if the page has space for a new uples
  int hasSpace(page* pg, int mask);
  
  //finds a page that has space for a new tuple to be added to it
  int findFreePage(thrHeapFile* lhf);
  
  //adds a new tuple to the heapFile
  int addTuple(thrHeapFile* lhf, tuple* t);

  //deletes a tuple from the given heappage, 1 indicates it exists and was deleted,
  //0 indicates it was not deleted (likely because it does not exist)
  int deleteTuple(thrHeapFile* lhf, tuple* t);
  
  
  //returns the number of pages in the heapfile
  int getNumPages(heapFile* hf);

  //cleanup a heapfile when done using
  void cleanup(thrHeapFile* lhf);


  //cleanup a heapfile when done using
  void cleanupCache(table* t);

  //inidividual threads "heapfile"
  thrHeapFile* initThrHF(registrar* treg, heapFile* hf);
}
