#include "tuple.h"
#include "heapPage.h"
#define INIT_LIST_SIZE 32

tuple* TUP::getIndex(retTuple* rt, int i, int offset){
  return (rt->t+(i*rt->tupSize)+offset);
}

void TUP::fillIndex(retTuple* rt, int offset, tuple* field, int fieldSize){
  memcpy(rt->t+(rt->tupSize*rt->len)+offset, field, fieldSize);
  if(fieldSize+offset==rt->tupSize){
    rt->len++;
    if(rt->len>=rt->size){
      //this potentially can be bad but seems like cleanest way to do it
      rt->t = (tuple*)realloc(rt->t, (rt->size<<1)*rt->tupSize);
      rt->size+=rt->size;
    }
  }
}
void TUP::incrRetTup(retTuple* rt, tuple* t){
  if(rt->len>=rt->size){
    rt->t = (tuple*)realloc(rt->t, (rt->size<<1)*rt->tupSize);
    rt->size+=rt->size;
  }
  memcpy(rt->t+rt->tupSize*rt->len,t, rt->tupSize);
  rt->len++;
}

void TUP::addCurPage(thrHeapFile* lhf, retTuple* rt){
  int pgMask = HF::getBitMask(lhf->curItem->p);
  heapFile* hf= lhf->myTable->tableDef;
  for(int i=0;i<hf->numSlots;i++){
    if((pgMask&(1<<i))){
    memcpy(getIndex(rt, rt->len, 0),
	   HP::tupleLoc(lhf->curItem->p, i, hf),
	   TD::getSizeTuple(hf->desc));
    rt->len++;
    if(rt->len>=rt->size){
      rt->t = (tuple*)realloc(rt->t, (rt->size<<1)*rt->tupSize);
      rt->size+=rt->size;
    }
    }
  }
}



retTuple* TUP::createRT(tupleDesc* td, int allocT){
  if(verbose>2){
    //stderr because I want asap flush
    fprintf(stderr,"Starting create rt: %lu\n", sizeof(retTuple));
  }
  retTuple* rt=(retTuple*)calloc(1, sizeof(retTuple));
  if(!rt){
    fprintf(stderr,"Non recoverable calloc failure!\n");
    exit(0);
  }
  if(verbose>2){
    //stderr because I want asap flush
    fprintf(stderr,"Alloc rt\n");
  }
  rt->size=INIT_LIST_SIZE;
  rt->tupSize=TD::getSizeTuple(td);
  rt->td=td;
 if(verbose>2){
    //stderr because I want asap flush
    fprintf(stderr,"Fields set\n");
  } 
  //dont need to calloc cuz got length
  if(allocT){
    rt->t=(tuple*)calloc(rt->size,rt->tupSize);
  if(verbose>2){
    //stderr because I want asap flush
    fprintf(stderr,"Alloc tuples list\n");
  }
  }
  return rt;
}


int TUP::getTupleSize(tupleDesc* td){
  return TD::getSizeTuple(td);
}

tupleDesc* TUP::getTupleDesc(heapFile* hf){
  return hf->desc;
}

int TUP::getPageId(thrHeapFile* lhf){
  return lhf->curPageNum;
}

int TUP::getPageSlot(page* pg, tupleDesc* td, tuple* t){
  unsigned long pgStart=(unsigned long)pg;
  unsigned long tupleStart=(unsigned long)t;
  pgStart+=4;
  assert(((tupleStart-pgStart)/getTupleSize(td))*getTupleSize(td)==(tupleStart-pgStart));
  return (tupleStart-pgStart)/getTupleSize(td);
}


int TUP::setPageSlot(page* pg, tupleDesc* td, tuple* t, int val){
  int* intPg=(int*)pg;
  int bitVec=intPg[0];
  int loc=getPageSlot(pg,td,t);
  if(((1<<loc)&bitVec==(val<<loc))){
    return 0;
  }
  bitVec=(bitVec&(~(1<<loc)))|(val<<loc);
  intPg[0]=bitVec;
  return 1;
}

void TUP::setFieldData(unsigned char* data, tupleDesc* td, tuple* t, int fieldIndex){
  memcpy(t+TD::fieldOffset(td, fieldIndex), data, TD::fieldSize(td->field_types[fieldIndex]));
}

unsigned char* TUP::getField(tupleDesc* td, tuple* t, int fieldIndex){
  return (unsigned char*)(t+TD::fieldOffset(td, fieldIndex));
}

void TUP::toString(tupleDesc* td, tuple* t, int humanReadable){

  printf("Tuple[%d] -> [] =", TD::getSizeTuple(td));
  for(int i =0;i<td->numFields;i++){
    if(humanReadable){
      int offset= TD::fieldOffset(td,i);
          printf(" +%d -> %s[%d] -> [", TD::fieldOffset(td,i),td->names+i*field_name_length, TD::fieldSize(td->field_types[i]));
	  if(td->field_types[i]){
	    printf("%s", (char*)(t+offset));
	  }
	  else{
	    printf("%d", *(int*)(t+offset));
	  }
	  printf("]");
    }
    else{
    printf(" +%d -> %s[%d] -> [", TD::fieldOffset(td,i),td->names+i*field_name_length, TD::fieldSize(td->field_types[i]));
    int offset= TD::fieldOffset(td,i);
    for(int j =0;j<TD::fieldSize(td->field_types[i]);j++){
      printf("%x", (*(char*)(t+offset+j))&0xFF);
    }
    printf("]");
    }
  }
  printf("\n");

}
