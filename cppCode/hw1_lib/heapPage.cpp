#include "heapPage.h"



int HP::getID(thrHeapFile* lhf){
  return lhf->curPageNum;
}

int HP::getNumSlots(heapFile* hf){
  //  return (hf->pageSize-sizeof(int))/(TD::getSizeTuple(hf->desc));
  int pageBits=hf->pageSize<<3;
  int tupleBits=(TD::getSizeTuple(hf->desc)<<3)+1;
  int prelim=pageBits/tupleBits;
  //I want to be able to store header as int types
  int round32 = ((prelim+31)>>5)<<5;
  if(prelim==(pageBits-round32)/tupleBits){
    return prelim;
  }
  return prelim-1;
}

int HP::getHeaderSize(heapFile* hf){
  return TD::getSizeTD(hf->desc);
}

int HP::slotOccupied(page* pg, int s){
  int bitVec=*((int*)pg);
  return !!((1<<s)&bitVec);
}

int HP::setSlotOccupied(page* pg, int s){
  if(slotOccupied(pg, s)){
    return 0;
  }
  int* arr=((int*)pg);
  arr[0]|=(1<<s);
  return 1;
}

int HP::setSlotUnoccupied(page* pg, int s){
  if(!slotOccupied(pg, s)){
    return 0;
  }
  int* arr=((int*)pg);
  arr[0]&=(~(1<<s));
  return 1;
}


char* HP::tupleLoc(page* pg, int slot, heapFile* hf){
  return (char*)(pg+hf->headOffset+slot*TD::getSizeTuple(hf->desc));

}

void HP::addTuple(int numSlots, page* pg, tuple* t, heapFile* hf){
  if(verbose==2){
    printf("Adding:  ");
    TUP::toString(hf->desc, t, 0);
  }
  for(int i =0;i<numSlots;i++){
    if(!slotOccupied(pg, i)){
      if(verbose>3){
	printf("[%d]: %x\n",i,HF::getBitMask(pg));
      }
       setSlotOccupied(pg, i);

       if(verbose>3){
	 printf("Add After[%d]: %x\n",i, HF::getBitMask(pg));
       }
          if(!slotOccupied(pg, i)){
	 fprintf(stderr,"Error setting slot occupied!\n");
	 exit(0);
       }
       memcpy(tupleLoc(pg, i, hf), t, TD::getSizeTuple(hf->desc));
      return;
    }
  }
}

int HP::deleteTuple(page* pg, tupleDesc* td, tuple* t){
  return TUP::setPageSlot(pg, td, t, 0);
}

tuple* HP::readTuple(int numSlots, page* pg, int slot, heapFile* hf){
  if(numSlots<=slot){
    return NULL;
  }
  return (tuple*)tupleLoc(pg, slot, hf);
}

tuple* HP::readNextTuple(page* pg, tuple* t, tupleDesc* td){
  unsigned long pgLoc=(unsigned long)pg;
  unsigned long tLoc=(unsigned long)t;

  //just ptr arithmatic
  if(((tLoc-pgLoc)+TD::getSizeTuple(td))>(pagesize-TD::getSizeTuple(td))){
    return NULL;
  }
  return (tuple*)(t+TD::getSizeTuple(td));
}
