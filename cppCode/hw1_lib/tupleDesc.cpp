#include "tupleDesc.h"

extern int verbose;

//this is poorly coded and needs to be improved
extern int sizes[2];


tupleDesc* TD::newTupleDesc(int nfields, int pkField, int* fTypes, char** fNames){
  tupleDesc* tDesc=(tupleDesc*)calloc(1,sizeof(tupleDesc));
  if(verbose>2){
    printf("Creating new tDesc[%lu]:\n\tFields: %d\n\tPkField: %d\n\n", sizeof(tupleDesc), nfields, pkField);
  }
  //if given field we can just calculate everything 
  tDesc->numFields=nfields;
  tDesc->tdOffset=2*sizeof(int)+nfields*sizeof(int)+nfields*field_name_length;
  tDesc->field_types=(types*)calloc(nfields, sizeof(int));
  tDesc->names=(char*)calloc(nfields, field_name_length);

  for(int i =0;i<nfields;i++){
    tDesc->field_types[i]=(types)(fTypes[i]);
    strcpy(tDesc->names+i*field_name_length,fNames[i]);
    free(fNames[i]);
  }
  tDesc->pkField=pkField;
  int total=0;
  for(int i =0;i<nfields;i++){
    total+=fieldSize(tDesc->field_types[i]);
  }
  tDesc->tupleSize=total;
  if(verbose>2){
    for(int i =0;i<nfields;i++){
   	printf("Field[%d]-> Name: %s, Type: %d, Size: %d\n",
	       i, indexToName(tDesc, i),fTypes[i],fieldSize(tDesc->field_types[i]));
    }
    printf("\nSizeof Tuple: %d\n", getSizeTuple(tDesc));
  }
  free(fTypes);
  free(fNames);
  return tDesc;
}

tupleDesc* TD::createTupleDesc(unsigned char* info){
  //since we are reaccessing just got to access everything in standard fashion and
  //build up
  int * arr= (int*)info;
  tupleDesc* tDesc=(tupleDesc*)calloc(1,sizeof(tupleDesc));
  int nfields=arr[0];
  int pkField=arr[1];
  int tOffset=2*sizeof(int)+nfields*sizeof(int)+nfields*field_name_length;
  types* fTypes=(types*)calloc(nfields, sizeof(int));
  char* fNames=(char*)calloc(nfields, field_name_length);
  for(int i =0;i<nfields;i++){
    fTypes[i]=(types)arr[2+i];
    memcpy(fNames+i*field_name_length,
	   (char*)(info+((2+nfields)*sizeof(int)+field_name_length*i)),
	   field_name_length);
  }
  tDesc->numFields=nfields;
  tDesc->tdOffset=tOffset;
  tDesc->field_types=fTypes;
  tDesc->names=fNames;
  tDesc->pkField=pkField;
  int total=0;
  for(int i =0;i<nfields;i++){
    total+=fieldSize(fTypes[i]);
  }
  tDesc->tupleSize=total;

  if(verbose>2){
    printf("TupleDesc { \n\tnumFields: %d\n\tdOffset: %d\n\tpkField: %d\n\t }\n", tDesc->numFields, tDesc->tdOffset, tDesc->pkField);
    for(int i =0;i<nfields;i++){
   	printf("Field[%d]-> Name: %s, Type: %d, Size: %d\n",
	       i, indexToName(tDesc, i),fTypes[i],fieldSize(tDesc->field_types[i]));
    }
    printf("\nSizeof Tuple: %d\n", getSizeTuple(tDesc));
  }
  
  free(info);
  return tDesc;
}


int TD::writeTupleDesc(tupleDesc* td, FILE* fp){
  unsigned char* buf=(unsigned char*)calloc(getSizeTD(td), sizeof(char));
  memcpy(buf,td, 8);
  memcpy(buf+8, td->field_types, td->numFields*sizeof(int));
  memcpy(buf+8+td->numFields*sizeof(int), td->names, td->numFields*field_name_length);
  if(verbose>2){
    printf("Writing: %d\n", getSizeTD(td));
  }
  if(!fwrite(buf, getSizeTD(td), 1, fp)){
    fprintf(stderr,"Error initializing heapfile contents!\n");
    return -1;
  }
  fflush(fp);
  free(buf);
  return 0;
}

int TD::fieldSize(types t){
  return sizes[t];

}


int TD::getSizeTD(tupleDesc* td){
  return td->tdOffset;
}

int TD::getSizeTuple(tupleDesc * td){
  return td->tupleSize;
}

char* TD::indexToName(tupleDesc * td, int index){
  return (char*)(td->names+(field_name_length*index));

}

int TD:: nameToIndex(tupleDesc * td, char* name){
  for(int i =0;i<td->numFields;i++){
    if(verbose>4){
      printf("testing: %s vs %s\n", indexToName(td, i), name);
    }
    if(!strcmp(indexToName(td, i),name)){
      return i;
    }
  }
  return -1;
}

int TD::fieldOffset(tupleDesc* td, int index){
  int prevSizes=0;
  for(int i =0;i<index;i++){
    prevSizes+=fieldSize(td->field_types[i]);
  }
  return prevSizes;

}

int TD::getPkField(tupleDesc* td){
  return td->pkField;
}


void TD::toString(tupleDesc* td){
  printf("TupleDesc[%lu] %p-> { \n\tNumFields[%lu]: %d\n\tPkFieldIndex[%lu]: %d\n\tTupleSize[%lu]: %d\n\tTupleDescSize[%lu]: %d\n\tfield_types[%lu] %p -> {",
	 sizeof(tupleDesc), td, sizeof(td->numFields), td->numFields, sizeof(td->pkField), td->pkField, sizeof(td->tupleSize), td->tupleSize, sizeof(td->tdOffset), td->tdOffset, sizeof(td->field_types), td->field_types);
  for(int i =0;i<td->numFields;i++){
    printf("\n\t\tfType[%lu]: %d[%d] at + %d",
	   sizeof(td->field_types[i]), td->field_types[i], fieldSize(td->field_types[i]), fieldOffset(td, i));
  }
  printf("\n\t\t}\n\tfield_names[%lu] %p -> {", sizeof(td->names), td->names);
  for(int i =0;i<td->numFields;i++){
    printf("\n\t\tfNames[%d]: %s[%d]",
	   0, td->names+i*field_name_length, field_name_length);
  }
  printf("\n\t}\n");
}
