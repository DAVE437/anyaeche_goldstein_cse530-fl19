#include "database.h"


int DB::createDB(char* name){
  //basically setting permissions, and make dir, if it exists already we find
  int stat=mkdir(name, S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
  if(stat==-1){
    if(errno==EEXIST){
      return 0;
    }
    fprintf(stderr,"Error making database: %s\n", strerror(errno));
    return -1;
  }
  return 0;
}
