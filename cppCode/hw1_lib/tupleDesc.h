#pragma once
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "type.h"

#define field_name_length 16

typedef struct tupleDesc{
  int numFields;
  int pkField;
  int tupleSize;
  int tdOffset;
  types* field_types;
  char* names;
}tupleDesc;

namespace TD{


  //makes a new tupleDesc for a new heap file
  tupleDesc* newTupleDesc(int nfields, int pkField, int* fTypes, char** fNames);
  
  //creates a tuple from raw data (read from begining of heapfile);
  tupleDesc* createTupleDesc(unsigned char* info);

  //writes the contents of tupledesc to a file*
  int writeTupleDesc(tupleDesc* td, FILE* fp);
    
  //returns the size of the tuple descriptor
  int getSizeTD(tupleDesc* td);

  //returns the size of a given tuple described by the td
  int getSizeTuple(tupleDesc* td);


  //returns the size of a given field type
  int fieldSize(types t);

  //returns the name of a given field index
  char* indexToName(tupleDesc * td, int index);


  //returns the field index for a given field name
  //returns -1 if field does not exist
  int nameToIndex(tupleDesc * td, char* name);


  //returns the offset from heapfile start to first page 
  int fieldOffset(tupleDesc * td,int index);

  //returns the field index of the primary key
  int getPkField(tupleDesc* td);

  //visual representation of tupleDesc
  void toString(tupleDesc* td);

};
