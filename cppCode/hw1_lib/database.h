#pragma once
#include <sys/stat.h>
#include <sys/types.h>
#include <errno.h>
#include "catalog.h"



namespace DB{
  //creates a new database will return -1 if database already exists else will
  //return 0
  int createDB(char* name);
};
