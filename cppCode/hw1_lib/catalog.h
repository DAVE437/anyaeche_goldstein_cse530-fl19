#pragma once

#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <sched.h>
#include <stdarg.h>
#include <errno.h>
#include <unistd.h>
#include <assert.h>
#include "tupleDesc.h"
#include "../hw4_lib/bufferPool.h"


extern int verbose;

struct heapFile;
//catalog struct used for storing relevant info
typedef struct catalog_t{
  FILE* cat_file;
  char* db_name;
  int numTables;
  int allocSize;
  char** tableNames;
  char** pKeyFields;
  int* tableIDs;
  registrar* treg;
}catalog_t;


namespace LOG{
  //creates a new catalog file that will store the tables and a unique ID for each table,
  //and the primary key for each table
  //file name will the '.log_<data base name".txt'
  //if the file already exists will just open the file and return a file* for it.
  catalog_t* createCatalog(char* db_name);

  //adds a table to the catalog. Will pass the heapfile to get info about the table,
  //the tables name (file name), and the primary key of the table
  int addTable(catalog_t* log, char* name, char* pKeyField);

  //adds a table to the catalog. Will pass the heapfile to get info about the table and
  //the tables name (file name). The is overloaded version of above that doesnt
  //request pKeyField
  int addTable(catalog_t* log, char* name);

  //given a table name will return the tables unique ID
  //return -1 if table doesnt not exist
  int getTableId(catalog_t* log, char* name);

  //for a given table id (table) in the catalog will return the description of the fields
  tupleDesc* getTupleDesc(catalog_t* log, int tableID);

  //given a table id will return a heapFile* for the table
  heapFile* getDBFile(catalog_t* log, int tableID);

  //clears the contents from a catalog
  void clear(catalog_t* log);

  //returns the primary key of a table from the catalog given a table ID
  char* getPrimaryKey(catalog_t* log, int tableID);

  //returns the table name from the catalog given a table ID
  char* getTableName(catalog_t* log, int tableID);
}




