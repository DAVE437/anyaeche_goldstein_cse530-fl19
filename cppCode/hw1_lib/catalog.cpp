#include "catalog.h"
#include "heapFile.h"
#define startSize 8

char* getCurDT(){
  time_t t = time(NULL);
  struct tm tm = *localtime(&t);
  char* buf=(char*)calloc(32, sizeof(char));
  sprintf(buf,"%d-%d-%d %d:%d:%d", tm.tm_year + 1900, tm.tm_mon + 1,tm.tm_mday, tm.tm_hour, tm.tm_min, tm.tm_sec);
  return buf;
}

catalog_t* LOG::createCatalog(char* db_name){
  
  char logPath[64]="";
  //universal catalog name
  sprintf(logPath,"%s/.log", db_name);
  if(verbose>2){
    printf("Catalog Path: %s\n", logPath);
  }
  FILE* fp=NULL;
  int alreadyExists=0;
  //if reacess then already exists case is different
  if( access( logPath, F_OK ) != -1 ) {
    alreadyExists=1;
  }
  if(verbose>2){
    printf("Does already exist: %d\n", alreadyExists);
  }
  if(alreadyExists){
    fp=fopen(logPath,"rb+");
  }
  else{
    //open 'a' to create then "rb+" for random access
    fp=fopen(logPath,"a");
    fclose(fp);
    fp=fopen(logPath,"rb+");
  }
  if(!fp){
    fprintf(stderr,"Error opening catalog file for database %s\n", db_name);
    return NULL;
  }
  //different cases for getting size (amount of tables)
  //this is first 5 bytes (also \n marker)
  char buf[32]="";
  if(alreadyExists){
    if(!fgets(buf, 16, fp)){
      fprintf(stderr,"Error reading from catalog\n");
      return NULL;
    }
  }
  else{
    //5 bytes saved for this space, but can just write current digits for now
    sprintf(buf,"%d\n", 0);
    if(!fwrite(buf, strlen(buf), 1, fp)){
      fprintf(stderr,"Error writing to catalog\n");
      return NULL;
    }
  }

  if(verbose>2){
    printf("Creating catalog struct!\n");
  }
  catalog_t* log=(catalog_t*)calloc(1,sizeof(catalog_t));
  log->db_name=(char*)calloc(32, sizeof(char));
  log->cat_file=fp;
  strcpy(log->db_name,db_name);
  log->numTables=atoi(buf);
  if(log->numTables>=startSize){
    log->allocSize=log->numTables<<1;
  }
  else{
    log->allocSize=startSize;
  }
  if(verbose>2){
    printf("Alloc size: %d\n", log->allocSize);
  }
  //we will store table info in memory after first read just for efficiencies sake
  log->tableNames=(char**)calloc(log->allocSize, sizeof(char*));
  log->pKeyFields=(char**)calloc(log->allocSize, sizeof(char*));
  log->tableIDs = (int*)calloc(log->allocSize, sizeof(int));
  log->treg=BP::initReg(log->allocSize, sysconf(_SC_NPROCESSORS_ONLN));
  return log;
}

int LOG::addTable(catalog_t* log, char*name, char* pKeyField){
  //256 bytes between table info
  char* buf=(char*)calloc(256, sizeof(char));
  fseek(log->cat_file, 0, SEEK_SET);
  if(verbose>2){
    //dont expect more than 7 digit number of tables...
    char firstLine[8]="";
    fgets(firstLine, 8, log->cat_file);
    printf("First Line: %s\n", firstLine);
    printf("Active Tables: %d\n", log->numTables);
    
  }
  //just going through table info, store table
  //basically logic here is if a tables info has been read
  //its in buffers, else read from log file
  //then do necessary comparisons (all other log functions are basically done the same way)
  rewind(log->cat_file);
  fseek(log->cat_file, 5, SEEK_CUR);
  for(int i =0;i<log->numTables;i++){
    if(!log->tableNames[i]){
      if(!fread(buf,256, 1,log->cat_file)){
	fprintf(stderr,"Error reading from catalog\n");
	return -1;
      }
      log->tableNames[i]=(char*)calloc(32,sizeof(char));
      log->pKeyFields[i]=(char*)calloc(32,sizeof(char));
      sscanf(buf,"%s %d %s",log->tableNames[i], &log->tableIDs[i], log->pKeyFields[i]);
    }
    if(verbose>2){
    printf("Comparing %s vs %s\n", name, log->tableNames[i]);
    }
    if(!strcmp(name, log->tableNames[i])){
      char newName[128]="", oldName[128]="";

      //renaming to name+date
      sprintf(newName,"%s/%s_%s", log->db_name, log->tableNames[i],getCurDT());
      sprintf(oldName,"%s/%s", log->db_name, log->tableNames[i]);
      if(rename(oldName, newName)==-1){
	fprintf(stderr,"Error adding new heapfile (on rename)!\n");
	return -1;
      }
      if(pKeyField){
	sprintf(buf,"%s %d %s\n", name, log->tableIDs[i], pKeyField);
	if(verbose>2){
	  fprintf(stderr,"New Table Name: [%d]: %s\n", log->tableIDs[i], name);
	}
      }
      else{
	sprintf(buf,"%s %d\n", name, log->tableIDs[i]);
	if(verbose>2){
	  fprintf(stderr,"New Table Name: [%d]: %s\n", log->tableIDs[i], name);
	}
      }
      rewind(log->cat_file);
      fseek(log->cat_file,5+256*i, SEEK_CUR);
      if(!fwrite(buf, 256, 1, log->cat_file)){
	fprintf(stderr,"Error writing to catolog file!\n");
	return -1;
      }
      return 1;
    }
  }
  if(log->numTables+1>=log->allocSize){
    if(verbose>2){
      printf("Resizing %d->%d\n",log->numTables,log->numTables<<1);
    }
    //alloc for our buffers so hopefully dont need to go through file ops each time
    log->allocSize=log->numTables<<1;
    log->treg->size=log->allocSize;
    table** newTables = (table**)calloc(log->allocSize, sizeof(table*));
    char** tempTNames=(char**)calloc(log->allocSize, sizeof(char*));
    char** tempPKF=(char**)calloc(log->allocSize, sizeof(char*));
    int* tempTIDs=(int*)calloc(log->allocSize, sizeof(int));
    for(int i=0;i<log->numTables;i++){
      tempTNames[i]=log->tableNames[i];
      tempTIDs[i]=log->tableIDs[i];
      tempPKF[i]=log->pKeyFields[i];
      newTables[i]=log->treg->tables[i];
    }
    free(log->pKeyFields);
    free(log->tableNames);
    free(log->tableIDs);
    free(log->treg->tables);
    log->treg->tables=newTables;
    log->tableNames=tempTNames;
    log->tableIDs=tempTIDs;
  }
  log->tableNames[log->numTables]=(char*)calloc(32,sizeof(char));
  strcpy(log->tableNames[log->numTables], name);
  if(verbose>2){
    printf("New Table Name: %s\n", log->tableNames[log->numTables]);
  }
  if(log->numTables){
    log->tableIDs[log->numTables]=log->tableIDs[log->numTables-1]+1;
  }
  else{
    log->tableIDs[log->numTables]=1;
  }


  if(pKeyField){
    if(verbose>2){
      printf("Adding a new PKField\n");
    }
    sprintf(buf,"%s %d %s\n", name, log->tableIDs[log->numTables], pKeyField);
    if(verbose>2){
      fprintf(stderr,"New Table Name: [%d]: %s\n", log->tableIDs[log->numTables], name);
    }
  }
  else{
    sprintf(buf,"%s %d \n", name, log->tableIDs[log->numTables]);
    if(verbose>2){
      fprintf(stderr,"New Table Name: [%d]: %s\n", log->tableIDs[log->numTables], name);
    }
  }
  log->numTables++;
  rewind(log->cat_file);
  char newVal[8]="";
  sprintf(newVal,"%d\n", log->numTables);
  if(!fwrite(newVal, 5, 1, log->cat_file)){
      fprintf(stderr,"Error writing to catalog\n");
      return -1;
    }
  fseek(log->cat_file, 0, SEEK_END);
  if(!fwrite(buf, 256, 1, log->cat_file)){
    fprintf(stderr,"Error writing to catolog file!\n");
    return -1;
  }
  fflush(log->cat_file);
  free(buf);
  return 0;
}


int LOG::addTable(catalog_t* log, char* name){
  return addTable(log, name, NULL);
}

int LOG::getTableId(catalog_t* log, char* name){
  char* buf=(char*)calloc(256, sizeof(char));
  char* pkSpace=(char*)calloc(128, sizeof(char));
  for(int i =0;i<log->numTables;i++){
    if(!log->tableNames[i]){
      if(!fgets(buf,256, log->cat_file)){
	fprintf(stderr,"Error reading from catalog\n");
	return -1;
      }
      log->tableNames[i]=(char*)calloc(32,sizeof(char));
      log->pKeyFields[i]=(char*)calloc(32,sizeof(char));
      sscanf(buf,"%s %d %s",log->tableNames[i], &log->tableIDs[i], log->pKeyFields[i]);
    }
    if(!strcmp(name, log->tableNames[i])){
      return log->tableIDs[i];
    }
  }
  return -1;
}

tupleDesc* LOG::getTupleDesc(catalog_t* log, int tableID){
  char* buf=(char*)calloc(512, sizeof(char));
  for(int i =0;i<log->numTables;i++){
    if(!log->tableNames[i]){
      if(!fgets(buf,256, log->cat_file)){
	fprintf(stderr,"Error reading from catalog\n");
	return NULL;
      }
      log->tableNames[i]=(char*)calloc(32,sizeof(char));
      log->pKeyFields[i]=(char*)calloc(32,sizeof(char));
      sscanf(buf,"%s %d %s",log->tableNames[i], &log->tableIDs[i], log->pKeyFields[i]);
    }
    if(log->tableIDs[i]=tableID){
      FILE* fp=fopen(log->tableNames[i],"rb+");
      memset(buf, 0, 512);
      if(!fread(buf, 512, 1, fp)){
	fprintf(stderr,"Error reading from catalog\n");
	return NULL;
      }
      fclose(fp);
      return TD::createTupleDesc((unsigned char*)buf);
    }
  }
  return NULL;
}

heapFile* LOG::getDBFile(catalog_t* log, int tableID){
  char* buf=(char*)calloc(512, sizeof(char));
  for(int i =0;i<log->numTables;i++){
    if(!log->tableNames[i]){
      if(!fgets(buf,256, log->cat_file)){
	fprintf(stderr,"Error reading from catalog\n");
	return NULL;
      }
      log->tableNames[i]=(char*)calloc(32,sizeof(char));
      log->pKeyFields[i]=(char*)calloc(32,sizeof(char));
      sscanf(buf,"%s %d %s",log->tableNames[i], &log->tableIDs[i], log->pKeyFields[i]);
    }
    if(log->tableIDs[i]=tableID){
      FILE* fp=fopen(log->tableNames[i],"rb+");
      memset(buf, 0, 512);
      if(!fread(buf, 512, 1, fp)){
	fprintf(stderr,"Error reading from catalog\n");
	return NULL;
      }
      fclose(fp);
      return HF::createHeapFile(log, log->tableNames[i],TD::createTupleDesc((unsigned char*)buf));
    }
  }
  return NULL;

}

void LOG::clear(catalog_t* log){
  char logPath[128]="";
  sprintf(logPath,"%s/.log", log->db_name);
  fclose(log->cat_file);
  log->cat_file=fopen(logPath,"w");
  fclose(log->cat_file);
  log->cat_file=fopen(log->db_name,"rb+");
}



char* LOG::getPrimaryKey(catalog_t* log, int tableID){
  char* buf=NULL;
  for(int i =0;i<log->numTables;i++){
    if(!log->tableNames[i]){
      buf=(char*)calloc(256, sizeof(char));
      if(!fgets(buf,256, log->cat_file)){
	fprintf(stderr,"Error reading from catalog\n");
	return NULL;
      }
      log->tableNames[i]=(char*)calloc(32,sizeof(char));
      log->pKeyFields[i]=(char*)calloc(32,sizeof(char));
      sscanf(buf,"%s %d %s",log->tableNames[i], &log->tableIDs[i], log->pKeyFields[i]);
    }
    if(log->tableIDs[i]=tableID){
      if(buf){
      free(buf);
      }
	return log->pKeyFields[i];
    }
  }
  return NULL;
}


char* LOG::getTableName(catalog_t* log, int tableID){
  char* buf=NULL;
  for(int i =0;i<log->numTables;i++){
    if(!log->tableNames[i]){
      buf=(char*)calloc(256, sizeof(char));
      if(!fgets(buf,256, log->cat_file)){
	fprintf(stderr,"Error reading from catalog\n");
	return NULL;
      }
      log->tableNames[i]=(char*)calloc(32,sizeof(char));
      log->pKeyFields[i]=(char*)calloc(32,sizeof(char));
      sscanf(buf,"%s %d %s",log->tableNames[i], &log->tableIDs[i], log->pKeyFields[i]);
    }
    if(log->tableIDs[i]=tableID){
      if(buf){
      free(buf);
      }
	return log->tableNames[i];
    }
  }
  return NULL;
}
