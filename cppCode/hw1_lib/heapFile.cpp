#include "heapFile.h"
#include "heapPage.h"


heapFile* HF::createHeapFile(catalog_t* log, char* name, tupleDesc* fields){
  //just creating a file and initializing fields, starts with 1 page
  //also stores table in log
  int res=LOG::addTable(log, name);
  if(res==-1){
    return NULL;
  }
  heapFile* hf=(heapFile*)calloc(1,sizeof(heapFile));
  char fullPath[128]="";
  sprintf(fullPath,"%s/%s", log->db_name, name);
  if(verbose>2){
    printf("New heapFile: %s\n", fullPath);
  }
  hf->fullPath=(char*)calloc(strlen(fullPath), sizeof(char));
  strcpy(hf->fullPath, fullPath);
  FILE* fp =fopen(fullPath,"a");
  fclose(fp);
  fp =fopen(fullPath,"rb+");
  if(!fp){
    fprintf(stderr,"Error creating new heap file!\n");
    return NULL;
  }
  hf->fName=(char*)calloc(32, sizeof(char));
  strcpy(hf->fName, name);
  hf->fp=fp;
  hf->numPages=1;
  hf->pageSize=pagesize;
  hf->desc=fields;
  if(verbose>2){
    printf("Size of tupledesc: %d\n", TD::getSizeTD(hf->desc));
  }
  if(-1==extendHeapFile(hf, hf->pageSize*hf->numPages+TD::getSizeTD(hf->desc))){
    return NULL;
  }
  rewind(hf->fp);
  if(-1==TD::writeTupleDesc(hf->desc, hf->fp)){
    fprintf(stderr,"Error initializing heapfile space!\n");
    return NULL;
  }
  if(verbose>0){
    printf("New Tuple Desc\n");
    TD::toString(hf->desc);
  }

  //changes is basically to know if when changing from cur page
  //we need to write back or not
  hf->numSlots=HP::getNumSlots(hf);
  //round to 32 then convert to bytes
  hf->headOffset=(((hf->numSlots+31)>>5)<<5)>>3;
  if(verbose>2){
    printf("HeapPage Slots: %d -> %d\n", hf->numSlots, hf->headOffset);
  }
  BP::addNewTable(log->treg, hf, 1);
  return hf;
}


heapFile* HF::reaccessHeapFile(catalog_t* log, char* name){
  //instead of being passed tuple desc just grab the first chunk from the file
  //and tuple desc will create a valid version
  heapFile* hf=(heapFile*)calloc(1,sizeof(heapFile));
  char fullPath[128]="";
  sprintf(fullPath,"%s/%s", log->db_name, name);
  if(verbose>2){
    printf("Reaccess Path: %s\n", fullPath);
  }

  FILE* fp =fopen(fullPath,"rb+");
  if(!fp){
    fprintf(stderr,"Error reaccessing heapfile. The file may not exist!\n");
    return NULL;
  }
    hf->fullPath=(char*)calloc(strlen(fullPath), sizeof(char));
  strcpy(hf->fullPath, fullPath);
  unsigned char* buf=(unsigned char*)calloc(512, sizeof(char));
  hf->fName=(char*)calloc(32, sizeof(char));
  strcpy(hf->fName, name);
  hf->fp=fp;
  hf->numPages=1;
  hf->pageSize=pagesize;
  fseek(hf->fp, 0, SEEK_SET);
  if(!fread(buf, 512, 1, hf->fp)){
    fprintf(stderr,"Error initializing heapfile contents!\n");
    return NULL;
  }
  hf->desc=TD::createTupleDesc(buf);
  if(verbose>0){
    printf("Reaccessed Tuple Desc\n");
    TD::toString(hf->desc);
  }


  //get number of pages:
  fseek(hf->fp, 0L, SEEK_END);
  size_t size = ftell(hf->fp);
  hf->numPages=(size-TD::getSizeTD(hf->desc))/hf->pageSize;

  hf->numSlots=HP::getNumSlots(hf);
  hf->headOffset=(((hf->numSlots+31)>>5)<<5)>>3;
  if(verbose>2){
    printf("HeapPage Slots: %d -> %d\n", hf->numSlots, hf->headOffset);
  }
  BP::addNewTable(log->treg, hf, 0);
  return hf;
}

thrHeapFile* HF::initThrHF(registrar* treg, heapFile* hf){
  thrHeapFile* newTHF = (thrHeapFile*)calloc(1, sizeof(thrHeapFile));
  newTHF->fp = fopen(hf->fullPath,"rb+");
  if(!newTHF->fp){
    fprintf(stderr,"Error opening heapfile!\n");
    fprintf(stderr,"Heapfile: %s\n", hf->fullPath);
    return NULL;
  }
  newTHF->myTable = BP::getTable(treg, hf->fName);
  newTHF->curPageNum = 0;
}

int HF::extendHeapFile(heapFile* hf, int n){
  fseek(hf->fp, 0 , SEEK_END);
  char byte=0x0;
  for(int i =0;i<n;i++){
    if(!fwrite(&byte, 1, 1, hf->fp)){
      fprintf(stderr,"Error extending heapfile!\n");
      return -1;
    }
  }
  fflush(hf->fp);
  return 0;
}

FILE* HF::getFile(heapFile* hf){
  return hf->fp;
}

tupleDesc* HF::getTupleDesc(heapFile* hf){
  return hf->desc;
}

unsigned long HF::getPageOffset(heapFile* hf, int pageNum){
  return TD::getSizeTD(getTupleDesc(hf))+hf->pageSize*pageNum;
}




page* HF::readPage(thrHeapFile* lhf, int pageNum){

  //reads page and stores in hf->curPage / hf->curPageNum
  //if pagenumber is out of range we extend file size
  heapFile* hf = lhf->myTable->tableDef;
  volatile unsigned long localNP = hf->numPages;
  if(verbose>2){
    printf("Comparing: %d vs %lu\n", pageNum, localNP);
  }
  if(pageNum>=(localNP)){
    LK::lock_wr(&lhf->myTable->flushLock, 0);
    if(verbose>2){
      printf("test: %lu == %lu\n", hf->numPages, localNP);
    }
    if(hf->numPages==localNP){
      if(verbose > 1){
      printf("Extending %lu->%lu pages\n", hf->numPages, localNP << 1);
      }
      if(-1==extendHeapFile(hf, hf->pageSize*hf->numPages)){
	return NULL;
      }
      hf->numPages=localNP<<1;
    }
    LK::unlock(&lhf->myTable->flushLock);
  }
  fseek(lhf->fp, 0, SEEK_SET);
  fseek(lhf->fp, getPageOffset(hf, pageNum), SEEK_CUR);
  if(verbose>2){
    printf("Reading new page: %d\n", pageNum);
  }
  page* myPage = (page*)calloc(hf->pageSize, sizeof(char));
  if(!fread(myPage, hf->pageSize, 1, lhf->fp)){
    fprintf(stderr,"Error reading page %d!\n", pageNum);
    return NULL;
  }
  if(verbose > 2){
    fprintf(stderr,"Read Page Successfully!\n");
  }
  return myPage;
}

int HF::getID(catalog_t* log, heapFile* hf){
  return LOG::getTableId(log, hf->fName);
}

int HF::writePage(heapFile* hf, FILE* lfp, page* pg, int pageNum){
  //just write to appropriate file location
  fseek(lfp, 0, SEEK_SET);
  fseek(lfp, getPageOffset(hf, pageNum), SEEK_CUR);
    if(verbose> 2){
      printf("Writing Page: %d\n", pageNum);
    }
  if(!fwrite(pg, hf->pageSize, 1, lfp)){
    fprintf(stderr,"Error writing back page %d!\n", pageNum);
    fprintf(stderr,"Error: %s\n", strerror(errno));
    return -1;
  }
  fflush(lfp);
  return 0;
}



int HF::hasSpace(page* pg, int mask){
  int bitVec=*((int*)pg);
  return !!(bitVec^mask);
}

int HF::getBitMask(page* pg){
  return *((int*)pg);
}

int HF::findFreePage(thrHeapFile* lhf){
  //go through pages till one doesnt have full bitmask
  heapFile* hf = lhf->myTable->tableDef;
  int mask=((1<<hf->numSlots)-1);

  while(1){
    int initBound=hf->numPages;
    if(verbose>2){
      printf("Num page: %d/%lu==%d\n", lhf->curPageNum,hf->numPages, initBound);
    }
    for(int i =0;i<=initBound;i++){
      lhf->curItem = BP::getPage(lhf->myTable, lhf, wrMode, i);
      lhf->curPageNum = i;
      assert(lhf->curItem->p);
      if(verbose>2){
	printf("%d: %x\n", i, getBitMask(lhf->curItem->p));
      }
      if(hasSpace(lhf->curItem->p, mask)){
	return i;
      }
    if(verbose > 2){
      printf("Dropping from here\n");
    }
    BP::dropItem(lhf->myTable->tcache, lhf, doWB);
      if(verbose>2){
	printf("%p & %p\n", lhf->curItem, lhf->curItem->ait->it);
      }
      lhf->curItem=NULL;
    }
    
  }
  return lhf->curPageNum;
}

int HF::addTuple(thrHeapFile* lhf, tuple* t){
  
  if(-1==findFreePage(lhf)){
    fprintf(stderr,"Error adding tuple!\n");
    return -1;
  }
  if(verbose>2){
    printf("Adding Tuple[ %d ] to Page: %d at %p\n",*((int*)t),lhf->curPageNum, lhf->curItem->p);
  }
  //once found free page pass off to heappage function cuz why not
  if(verbose>3){
    printf("Add Before[%d]", lhf->curPageNum);
  }
  HP::addTuple(lhf->myTable->tableDef->numSlots, lhf->curItem->p, t, lhf->myTable->tableDef);
  lhf->curItem->dirty=1;
  BP::dropItem(lhf->myTable->tcache, lhf, doWB);
  return 0;
}

int HF::deleteTuple(thrHeapFile* lhf, tuple* t){
  heapFile* hf = lhf->myTable->tableDef;
  if(verbose==2){
    printf("Deleting:  ");
    TUP::toString(hf->desc, t, 0);
  }
  int ret=0;
  int numSlots=hf->numSlots;
  int mask=((1<<numSlots)-1);
  for(int i =0;i<hf->numPages;i++){
    lhf->curItem = BP::getPage(lhf->myTable, lhf, wrMode, i);
    lhf->curPageNum = i;
    page* pg=lhf->curItem->p;
    assert(pg);
    if(!pg){
      BP::dropItem(lhf->myTable->tcache, lhf,doWB);
      return -1;
    }
    if(!getBitMask(pg)){
      BP::dropItem(lhf->myTable->tcache, lhf,doWB);
      continue;
    }
    //this should really be replaced with logn test clz
    for(int j=0;j<numSlots;j++){
      if(HP::slotOccupied(pg, j)){
	tuple* temp=HP::readTuple(numSlots, pg, j, hf);
	if(!temp){
	  fprintf(stderr,"Error reading a tuple that should have been readable!\n");
	  BP::dropItem(lhf->myTable->tcache, lhf,doWB);
	  return -1;
	}

	if(!memcmp(t, temp, TD::getSizeTuple(hf->desc))){
	  if(verbose>2){
	    printf("Deleting Tuple[ %d == %d ] from Page[%d][%d]\n", *((int*)temp),*((int*)t), i, j);
	  }
	  //want to be able to delete multiple of duplicate tuple (design choice?)
	  //ret will return 0
	  if(verbose>3){
	    printf("Del Before[%d][%d]: %x\n", i,j,getBitMask(pg));
	  }
	  ret=HP::setSlotUnoccupied(pg, j);
	  if(verbose>3){
	    printf("Del After[%d][%d]: %x\n",i,j, getBitMask(pg));
	  }
	  if(!ret){
	    lhf->curItem->dirty=1;
	    BP::dropItem(lhf->myTable->tcache, lhf,doWB);
	    return ret;
	  }
	}
      }
     
    }
    BP::dropItem(lhf->myTable->tcache, lhf,doWB);
  }
  return ret;
}

int HF::getNumPages(heapFile* hf){
  return hf->numPages;
}

void HF::cleanupCache(table* t){
  pageCache* head = t->tcache;
 if(verbose>2){
    fprintf(stderr,"Cleaning table!\n");
  }
 LK::lock_wr(&t->flushLock, 0);
  item* temp=head->head;
  while(temp){
    if(temp->dirty){
      writePage(t->tableDef, t->tableDef->fp, temp->p, temp->pageNum);
      temp->dirty = 0;
    }
    temp=temp->next;
  }
  LK::unlock(&t->flushLock);
}
void HF::cleanup(thrHeapFile* lhf){
  if(verbose>2){
    fprintf(stderr,"Cleaning up cur page!\n");
  }
  if(lhf->curItem&&lhf->curItem->p){
    LK::lock_wr(&lhf->myTable->flushLock, 0);
    if(lhf->curItem->dirty){
      writePage(lhf->myTable->tableDef, lhf->fp, lhf->curItem->p, lhf->curPageNum);
      lhf->curItem->dirty = 0;
    }
    LK::unlock(&lhf->myTable->flushLock);
  }
}

