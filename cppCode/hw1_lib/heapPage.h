#pragma once
#include "heapFile.h"

namespace HP {
  typedef unsigned char page;
  int getID(thrHeapFile* lhf);

  //returns the amount of tuples a page can store given a heapfile
  //and the descrpition of its fields
  int getNumSlots(heapFile* hf);

  //returns the size of the header for a given heapPage
  int getHeaderSize(heapFile* hf);

  //returns 1 if a given slot in the heappage is occupied and 0 othersize
  int slotOccupied(page* pg, int s);

  //sets slot s in heappage page to 1 to indicate its occupied (returns 1 on success)
  int setSlotOccupied(page* pg, int s);

  //sets slot to unoccupied, returns 1 on success
  int setSlotUnoccupied(page* pg, int s);

  //returns location that a tuple starts for a given slot in the bit vector
  char* tupleLoc(page* pg, int slot, heapFile* hf);
  
  //adds a new tuple to a given pg
  void addTuple(int numSlots, page* pg, tuple* t, heapFile* hf);

  //searches for and if found, will delete a given tuple from a page
  //if the tuple is found and deleted returns 1, else returns 0
  int deleteTuple(page* pg, tupleDesc* td, tuple* t);

  //returns the next tuple in a page after slot, will return NULL if next tuple is
  //outside of the page
  tuple* readTuple(int numSlots, page* pg, int slot, heapFile* hf);

  //returns the next tuple in a page after a given tuple. will reutrn null if next tuple
  //is outside of the page
  tuple* readNextTuple(page* pg, tuple* t, tupleDesc* td);
  
};
