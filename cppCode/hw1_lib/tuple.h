#pragma once
#include "heapFile.h"

typedef unsigned char tuple;
typedef unsigned char page;


typedef struct retTuple{
  tuple* t;
  int len;
  int size;
  //description of tuple we create via parse
  int tupSize;
  tupleDesc* td;
}retTuple;

namespace TUP{

  //when doing things like project multiple writes to a given tuple are necessary
  //this adds to rt->len+offset from field and increments rt->len when fieldSize+offset
  //make up full tuple size
  void fillIndex(retTuple* rt, int offset, tuple* field, int fieldSize);
  
  //get index of a tuple in rt (selects field determine by offset)
  tuple* getIndex(retTuple* rt, int i, int offset);

  //add a new tuple to rt structs list
  void incrRetTup(retTuple* rt, tuple* t);

  //creates a new rt struct
  retTuple* createRT(tupleDesc* td, int allocT);

  //adds current page of heapfile to retTuple (assumed same tupleDesc)
  void addCurPage(thrHeapFile* lhf, retTuple* rt);

  //returns size of a given tuple
  int getTupleSize(tupleDesc* td);

  //gets the tuple descriptor for this tuple type
  tupleDesc* getTupleDesc(heapFile* hf);

  //returns the page this tuple is stored on (pid is a proper noun!)
  int getPageId(thrHeapFile* lhf);

  //gets the slot a given tuple is stored at (in a given page)
  int getPageSlot(page* pg, tupleDesc* td, tuple* t);

  //sets the slot that a given suple is to be stored at to val (val must be 0 or 1)
  //returns -1 on error
  int setPageSlot(page* pg, tupleDesc* td, tuple* t, int val);

  //sets a given field (provided by field index) in a tuple to what is stored at data
  void setFieldData(unsigned char* data, tupleDesc* td, tuple* t, int fieldIndex);

  //gets the data stored in tuple t at fieldIndex
  unsigned char* getField(tupleDesc* td, tuple* t, int fieldIndex);

  //string representation (WHY??)
  void toString(tupleDesc* td, tuple* t, int humanReadable);
};

