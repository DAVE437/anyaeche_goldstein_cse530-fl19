#include "relation.h"



retTuple* REL::relOpInit(retTuple* rt, int fieldsMask, int nfields){

  int* fTypes=(int*)calloc(nfields, sizeof(int));
  char** fNames=(char**)calloc(nfields, sizeof(char*));
  int writeIndex=0;
  for(int i =0;i<rt->td->numFields;i++){
    if(fieldsMask&(1<<i)){
      if(verbose>2){
	printf("Selecting field: %d -> %s[%d]\n", i, TD::indexToName(rt->td, i), (int)rt->td->field_types[i]);
      }
      fNames[writeIndex]=(char*)calloc(field_name_length, sizeof(char));
      memcpy(fNames[writeIndex], TD::indexToName(rt->td,i), field_name_length);
      //      memcpy(fTypes+writeIndex, &rt->td->field_types[i], sizeof(int));
      fTypes[writeIndex]=rt->td->field_types[i];
      writeIndex++;
    }
  }
  retTuple* initRT = TUP::createRT(TD::newTupleDesc(nfields, 0, fTypes, fNames),1);
  return initRT;
}
retTuple* REL::select(retTuple* rt, relOp op, void* cmpVal, int field, retTuple* selectRT){
  //it seems I have given up on efficient coding...
  int offset=TD::fieldOffset(rt->td, field);
  if(verbose>2){
  printf("Select Offset: %d\n", offset);
  }
  tuple* temp;
  types t = rt->td->field_types[field];
  for(int i =0;i<rt->len;i++){
    //same optimization for order could be made to cond branching
    temp=TUP::getIndex(rt, i, offset);
    if(doOp(op, temp, cmpVal, t)){
      TUP::incrRetTup(selectRT, temp);
    }
  }
  return selectRT;
}


retTuple* REL::rename(retTuple* rt, char* fName, int field){
  memcpy(TD::indexToName(rt->td, field), fName, field_name_length);
  return rt;
}
retTuple* REL::project(retTuple* rt, int fieldsMask, retTuple* projectRT){
  int sizeMap[projectRT->td->numFields]={0};
  int offsetMap[projectRT->td->numFields]={0};
  int indexNum=0;
  for(int i =0;i<rt->td->numFields;i++){
    if(fieldsMask&(1<<i)){
      sizeMap[indexNum]=TD::fieldSize(rt->td->field_types[i]);
      offsetMap[indexNum]=TD::fieldOffset(rt->td, i);
      indexNum++;
    }
  }
  for(int i =0;i<rt->len;i++){
    int pOffset=0;
    for(int j=0;j<indexNum;j++){
      //fieldsize thing is probably inefficient
      TUP::fillIndex(projectRT,pOffset, TUP::getIndex(rt, i, offsetMap[j]), sizeMap[j]);
      pOffset+=TD::fieldSize(projectRT->td->field_types[j]);
    }
  }

  return NULL;
}
retTuple* REL::join(retTuple* r1, retTuple* r2, int f1, int f2, retTuple* joinRT){
  //double for loop for now...
  int offset1=TD::fieldOffset(r1->td,f1);
  int offset2=TD::fieldOffset(r2->td,f2);
  relOp op = EQ;
  //hashtable later, for now this
  /*  char* rehit=(char*)calloc(r2->len, sizeof(char));
  types t = r1->td->field_types[f1];
  for(int i =0;i<r1->len;i++){
    for(int j=0;j<r2->len;j++){
      if(doOp(op, TUP::getIndex(r1, i, offset1), TUP::getIndex(r2, j, offset2), t)){
	if(!rehit[j]){
	TUP::incrRetTup(joinRT, TUP::getIndex(r1, i, offset1));
	rehit[j]=1;
	}
      }
    }
    }
    free(rehit);*/
  tupleHT* ht= HT::initTable(r1->td, r2->td, f1, f2);
  for(int i =0;i<r1->len;i++){
    HT::addNode(ht, TUP::getIndex(r1, i, 0));
  }
  ht->rel=2;
  ht->koffset=ht->goffset;
  ht->ksize=ht->gsize;
  for(int i =0;i<r2->len;i++){
        HT::addNode(ht, TUP::getIndex(r2, i, 0));
  }
  free(joinRT->t);
  joinRT->t=HT::getNodesRel(ht);
  joinRT->size=ht->items;
  joinRT->len=ht->items;
  return joinRT;
}

int REL::doOp(relOp op, void* a, void* b, types t){
  if(t){
    return doOpStr(op, (char*)a, (char*)b);
  }
  return doOpInt(op, (int*)a, (int*)b);
}

int REL::doOpInt(relOp op, int* a, int* b){
  //  printf("Doing op between: %d and %d\n", *a, *b);
  switch(op)
    {
    case GT:
      return *a>*b;
    case LT:
      return *a<*b;
    case EQ:
      return *a==*b;
    case GTE:
      return *a>=*b;
    case LTE:
      return *a<=*b;
    case NOTEQ:
      return *a!=*b;
    default:
      fprintf(stderr,"Bad relational op\nexiting...\n");
      exit(0);
    }

}
int REL::doOpStr(relOp op, char* a, char* b){
  switch(op)
    {
    case GT:
      return 0;
    case LT:
      return 0;
    case EQ:
      return !strncmp(a, b, sizes[1]);
    case GTE:
      return 0;
    case LTE:
      return 0;
    case NOTEQ:
      return 0;
    default:
      fprintf(stderr,"Bad relational op\nexiting...\n");
      exit(0);
    }
}


