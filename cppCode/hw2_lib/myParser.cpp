#include "myParser.h"
//db globals/constants



char* toLower(char* buf, int len){
  char* low=(char*)calloc(len+2, sizeof(char));
  for(int i =0;i<len;i++){
    if(buf[i]>=65&&buf[i]<=90){
      low[i]=buf[i]+32;
    }
    else{
      low[i]=buf[i];
    }
  }
  return low;
}
void MP::handleQuery(char* buf){
  hsql::SQLParserResult parseMyResult;
  if(verbose==-1){
    printf("Running: %s\n", buf);
  }
  std::string query = buf;
  hsql::SQLParser::parse(query, &parseMyResult);
  if(!parseMyResult.isValid()){
    fprintf(stderr, "Given string is not a valid SQL query.\n%s\n", buf);
    fprintf(stderr, "%s (L%d:%d)\n",
	    parseMyResult.errorMsg(),
	    parseMyResult.errorLine(),
	    parseMyResult.errorColumn());
    exit(0);
  }
  hsql::printStatementInfo(parseMyResult.getStatement(0));
  parseFile();
}

int MP::testAgr(char* line){
  if(!strncmp(line, "avg", 3)){
    return 3;
  }
  if(!strncmp(line, "count", 5)){
    return 4;
  }
  if(!strncmp(line, "min", 3)){
    return 2;
  }
  if(!strncmp(line, "max", 3)){
    return 1;
  }
  if(!strncmp(line, "sum", 3)){
    return 5;
  }
  return -1;

}


int MP::testRel(char* line){
  if(!strncmp(line, "<", 1)){
    return 2;
  }
  if(!strncmp(line, ">", 1)){
    return 1;
  }
  if(!strncmp(line, "=", 1)){
    return 3;
  }
  if(!strncmp(line, ">=", 2)){
    return 4;
  }
  if(!strncmp(line, "<=", 2)){
    return 5;
  }
  if(!strncmp(line, "!=", 2)){
    return 5;
  }
  return -1;

}

int MP::getType(char* line){
  if(!strncmp("Fields", line, 6)){
    return 0;
  }
  if(!strncmp("Sources", line, 7)){
    return 1;
  }
  if(!strncmp("GroupBy", line, 7)){
    return 2;
  }
  if(!strncmp("Search Conditions", line, 17)){
    return 3;
  }


}
void MP::parseFile(){
  FILE* fp = fopen("qp.txt","r");
  if(!fp){
    fprintf(stderr,"Error opening parse file!\n");
  }
  char* line = (char*)calloc(64, sizeof(char));
  if(!fgets(line, 64, fp)){
    fprintf(stderr,"Error reading from parse file!\n");
  }
  if(strncmp(line,"SelectStatement", 15)){
    fprintf(stderr,"Unknown file format!\n");
  }
  
  char tables[2][16]={"",""};
  int ntab=0;
  char cols[2][16]={"",""};
  char jCols[2][16]={"",""};
  char aCols[2][16]={"",""};
  char cmpCol[2][16]={"",""};
  char groupField[16]="";
  int ncol=0;
  int type=-1, hasAgr[2]={0}, alias=0;
  int l=0, r=0;
  int joinOp=0;
  int cmpOp=0;
  int first1=0;
  while(fgets(line, 64, fp)){
    int lineLen = strlen(line);
    line[lineLen-1]=0;
    lineLen--;
    if(lineLen>1){
      if(line[lineLen-1]==':'){
	type=getType(line);
	continue;
      }
    }
    switch(type)
      {
      case 0:
	if(!hasAgr[ncol]){
	  char* test=toLower(line, lineLen);
	  hasAgr[ncol]=testAgr(test);
	    if(hasAgr[ncol]>0){
	      break;
	    }
	}
	if(!strncmp(line,"Alias", 5)){
	  alias=1;
	  break;
	}
	if(alias){
	  strcpy(aCols[ncol-1], line);
	}
	else{
	  strcpy(cols[ncol], line);
	  ncol++;
	}
	alias=0;
	break;
      case 1:
	if(!first1){
	  first1=1;
	  if(strncmp("Join Table", line, 10)){
	    strcpy(tables[0], line);
	    ntab=1;
	  }
	}
	else{
	if(!strncmp(line,"Left", 4)){
	  l=1;
	  break;
	}
	if(!strncmp(line,"Right", 5)){
	  r=1;
	  break;
	}
	if(l){
	  strcpy(tables[0], line);
	}
	else if(r){
	  strcpy(tables[1], line);
	}
	else{
	  if(!strncmp("Join Condition", line, 14)){
	    fgets(line, 64, fp);
	    joinOp=testRel(line);
	    fgets(line, 64, fp);
	    strncpy(jCols[0], line, strlen(line)-1);
	    fgets(line, 64, fp);
	    fgets(line, 64, fp);
	    strncpy(tables[0], line, strlen(line)-1);
	    fgets(line, 64, fp);
	    strncpy(jCols[1], line, strlen(line)-1);
	    fgets(line, 64, fp);
	    fgets(line, 64, fp);
	    strncpy(tables[1], line, strlen(line)-1);
	    ntab=2;
	  }
	}
	  l=0;
	  r=0;
	}
	break;
      case 2:
	strcpy(groupField, line);
	break;
      case 3:
	cmpOp=testRel(line);
	fgets(line, 64, fp);
	strncpy(cmpCol[0], line, strlen(line)-1);
	fgets(line, 64, fp);
	strncpy(cmpCol[1], line, strlen(line)-1);
	break;
      default:
	fprintf(stderr,"Malformed request!\n");
	exit(0);

      }
  }
  fclose(fp);
  if(verbose>2){
    printf("PARSE RESULTS!\n");
    printf("Tables[%d]: %s and %s\n", ntab, tables[0], tables[1]);
    printf("cols[%d]: %s and %s\n", ncol, cols[0], cols[1]);
    printf("jCol: %s and %s\n", jCols[0], jCols[1]);
    printf("aCols: %s and %s\n", aCols[0], aCols[1]);
  printf("cmpCol: %s and %s\n", cmpCol[0], cmpCol[1]);
  printf("groupField: %s\n", groupField);
  printf("hasAgr: %d, %d\n", hasAgr[0], hasAgr[1]);
  printf("joinOp: %d\n", joinOp);
  printf("cmpOp: %d\n", cmpOp);
  }
  if(joinOp&&ntab!=2){
    fprintf(stderr,"Invalid Query!\nJoin specified without sufficient table number!");
    return;
  }
  if(ncol!=2&&groupField[0]){
    fprintf(stderr,"Invalid Query!\nGroup by specified without sufficient columsn");
    return;
  }
  heapFile* hfs[2]={NULL,NULL};
  hfs[0]=hf;
  if(joinOp){
    if(!strcmp(hfs[0]->fName, tables[0])){
      hfs[1]=HF::reaccessHeapFile(log, (char*)tables[1]);
      if(!hfs[1]){
	fprintf(stderr,"Error accessing join file[1]!\n");
	exit(0);
      }
    }
    else if(!strcmp(hfs[0]->fName, tables[1])){
      hfs[1]=hfs[0];
      hfs[0]=HF::reaccessHeapFile(log, tables[0]);
      if(!hfs[0]){
	fprintf(stderr,"Error accessing join file[0]!\n");
	exit(0);
      }
    }
    else{
      hfs[0]=HF::reaccessHeapFile(log, tables[0]);
      if(!hfs[0]){
	fprintf(stderr,"Error accessing join file[0]!\n");
	exit(0);
      }
      hfs[1]=HF::reaccessHeapFile(log, tables[1]);
      if(!hfs[1]){
	fprintf(stderr,"Error accessing join file[0]!\n");
	exit(0);
      }
    }
  }
  else{
    if(strcmp(hfs[0]->fName, tables[0])){
      hfs[0]=HF::reaccessHeapFile(log, tables[0]);
      if(!hfs[0]){
	fprintf(stderr,"Error accessing table!\n");
	exit(0);
      }
    }
  }
  if(verbose>2){
  for(int i =0;i<ntab;i++){
    fprintf(stderr,"HFS[%d]: %p->%s\n", i, hfs[i], hfs[i]->fName);
  }
  }
  retTuple* tRets[2]={NULL,NULL};
  thrHeapFile** lhfs[2] = {NULL,NULL};
  int ubound[2] = {0};
  for(int i=0;i<ntab;i++){
    ubound[i] = hfs[i]->numPages;
    lhfs[i] = (thrHeapFile**)calloc(ubound[i], sizeof(thrHeapFile*));
    tRets[i]=TUP::createRT(hfs[i]->desc, 1);
    for(int j=0;j<ubound[i];j++){
      lhfs[i][j] = HF::initThrHF(log->treg, hfs[i]);
      lhfs[i][j]->curItem = BP::getPage(lhfs[i][j]->myTable, lhfs[i][j], wrMode, j);
      lhfs[i][j]->curPageNum = j;
      TUP::addCurPage(lhfs[i][j], tRets[i]);
    }
  }
  for(int i=0;i<ntab;i++){
       for(int j=0;j<ubound[i];j++){
	 BP::dropItem(lhfs[i][j]->myTable->tcache, lhfs[i][j], doWB);
	 fclose(lhfs[i][j]->fp);
	 free(lhfs[i][j]);
       }
       free(lhfs[i]);
  }
  
  int fieldMask=0;
  int upShift=0;
  int selectField=-1;
  for(int i =0;i<ncol;i++){
    
    upShift=TD::nameToIndex(hfs[0]->desc, cols[i]);
    if(upShift==-1){
      fprintf(stderr,"Specified bad field %s\n", cols[i]);
      exit(0);
    }
    if(!strcmp(cmpCol[0], cols[i])){
      selectField=i;
    }
    fieldMask|=(1<<upShift);
  }
  retTuple* qRet = REL::relOpInit(tRets[0], fieldMask, ncol);
  void* cmpVal=NULL;

  if(selectField!=-1){
    if(qRet->td->field_types[selectField]){
      cmpVal=(void*)cmpCol[1];
    }
    else{
      int val=atoi(cmpCol[1]);
      cmpVal=(void*)(&val);
    }
    REL::select(tRets[0], (relOp)(cmpOp-1), cmpVal, selectField, qRet);
      if(tRets[0]->t){
    free(tRets[0]->t);
  }
  free(tRets[0]);
  tRets[0]=qRet;
  }

  qRet = REL::relOpInit(tRets[0], fieldMask, ncol);
  if(tRets[1]){
    REL::join(tRets[0], tRets[1], TD::nameToIndex(tRets[0]->td, jCols[0]),TD::nameToIndex(tRets[1]->td, jCols[1]), qRet);

  }
  
  if(REL::project(tRets[0], fieldMask, qRet)){
    fprintf(stderr,"Error projecting for query!\n");
    exit(0);
  }
  for(int i=0;i<ncol;i++){
    if(aCols[i][0]){
      if(groupField[0]){
	if(!strcmp(groupField, TD::indexToName(qRet->td,i))){
	  memset(groupField, 0, 16);
	  strcpy(groupField, aCols[i]);
	}
      }
      REL::rename(qRet, aCols[i], i);
    }
  }
  if(verbose>2){
  TD::toString(qRet->td);
  }
  int doAgr=-1;
  int agrIndex=-1;
  for(int i =0;i<ncol;i++){
    if(hasAgr[i]!=-1){
      doAgr=hasAgr[i];
      agrIndex=i;
    }
  }
  if(doAgr!=-1){

    agrDesc* ad= (agrDesc*)calloc(1, sizeof(agrDesc));
    if(groupField[0]){
      ad->groupBy=1;
      ad->fields[0]=TD::nameToIndex(qRet->td, groupField);
      ad->fields[1]=ncol-(ad->fields[0]+1);
      qRet->td->field_types[ad->fields[1]]=(types)0;

    }
    else{
      ad->fields[0]=agrIndex;
    qRet->td->field_types[ad->fields[0]]=(types)0;      
    }

    

    ad->op=(agrOp)(doAgr-1);
  retTuple* agrRT=AGR::agrOpInit(qRet, ad);
  tupleHT* ht=HT::initTable(ad, qRet->td);
  if(ad->groupBy){
    for(int i =0;i<qRet->len;i++){
      AGR::mergeGB(ht,TUP::getIndex(qRet, i, 0));
    }
  }
  else{
    retTuple* aRet=AGR::agrOpInit(qRet, ad);
    for(int i =0;i<qRet->len;i++){
      AGR::mergeOF(aRet,ad, TUP::getIndex(qRet, i, 0));
    }
    if(qRet->t){
      free(qRet->t);
      free(qRet);
      }
    qRet=aRet;
  }
  AGR::getResults(ht, ad,qRet, qRet);
  }
  if(!silent){
    for(int i =0;i<qRet->len;i++){
      TUP::toString(qRet->td, TUP::getIndex(qRet, i, 0), 1);
    }
  }

  
}


