#include "hashtable.h"
#include "aggregator.h"
#include "relation.h"

//just a generally good hash function, taken from wikipedia
//Link: https://en.wikipedia.org/wiki/MurmurHash
uint32_t 
murmur3_32(const uint8_t* key, size_t len, uint32_t seed)
{
  uint32_t h = seed;
  if (len > 3) {
    const uint32_t* key_x4 = (const uint32_t*) key;
    size_t i = len >> 2;
    do {

      uint32_t k = *key_x4++;
      k *= 0xcc9e2d51;
      k = (k << 15) | (k >> 17);
      k *= 0x1b873593;
      h ^= k;
      h = (h << 13) | (h >> 19);
      h = h * 5 + 0xe6546b64;
    } while (--i);
    key = (const uint8_t*) key_x4;
  }
  if (len & 3) {
    size_t i = len & 3;
    uint32_t k = 0;
    key = &key[i - 1];
    do {
      k <<= 8;
      k |= *key--;
    } while (--i);
    k *= 0xcc9e2d51;
    k = (k << 15) | (k >> 17);
    k *= 0x1b873593;
    h ^= k;
  }
  h ^= len;
  h ^= h >> 16;
  h *= 0x85ebca6b;
  h ^= h >> 13;
  h *= 0xc2b2ae35;
  h ^= h >> 16;
  return h;
}


int HT::hashFun(unsigned char* relField, int len, unsigned int seed){
  return murmur3_32((const uint8_t*)relField, (size_t)len, (uint32_t)seed);
}

tupleHT* HT::initTable(agrDesc* ad, tupleDesc* td){
  tupleHT* ht= (tupleHT*)calloc(1, sizeof(tupleHT));
  ht->table=(tupleEnt**)calloc(initSize, sizeof(tupleEnt*));
  ht->seed=rand();
  ht->tableSize=initSize;
  ht->items=0;
  ht->op=ad->op;


  int gField=ad->fields[ad->groupBy];
  int kField=ad->fields[0];
  ht->koffset=TD::fieldOffset(td, kField);
  ht->goffset=TD::fieldOffset(td, gField);
  ht->ksize=TD::fieldSize(td->field_types[kField]);
  ht->gsize=TD::fieldSize(td->field_types[gField]);
  ht->gtype=td->field_types[gField];
  ht->tupleSize=TD::getSizeTuple(td);

  return ht;
}


tupleHT* HT::initTable(tupleDesc* td1,tupleDesc* td2, int f1, int f2){
  tupleHT* ht= (tupleHT*)calloc(1, sizeof(tupleHT));
  ht->table=(tupleEnt**)calloc(initSize, sizeof(tupleEnt*));
  ht->seed=rand();
  ht->tableSize=initSize;
  ht->items=0;
  ht->koffset=TD::fieldOffset(td1, f1);
  ht->goffset=TD::fieldOffset(td2, f2);
  ht->ksize=TD::fieldSize(td1->field_types[f1]);
  ht->gsize=TD::fieldSize(td2->field_types[f2]);
  ht->gtype=td1->field_types[f1];
  ht->tupleSize=TD::getSizeTuple(td1);
  ht->rel=1;
  return ht;
}
void HT::resize(tupleHT* ht){
  tupleEnt** oldTable=ht->table;
  int oldSize=ht->tableSize;
  ht->table = (tupleEnt**)calloc(ht->tableSize<<1, sizeof(tupleEnt*));
  ht->tableSize+=oldSize;
  for(int i =0;i<oldSize;i++){
    tupleEnt* next=NULL,* temp=oldTable[i];
    while(temp){
      next=temp->next;
      addNodeResize(ht, temp);
      temp=next;
    }
  }
  free(oldTable);
}
  //add  node to table
int HT::findNode(tupleHT* ht, tuple* t, int slot){
  tupleEnt* temp=ht->table[slot];
  while(temp){
    if(!memcmp(temp->t+ht->koffset, t+ht->koffset, ht->ksize)){
      if(!ht->rel){
      temp->hits++;
      //i know this is disgusting, basic logic is that I'm putting
      //groupby field in 2nd index of fields in agrDesc struct if
      
      return AGR::doOp(ht->op, temp->t+ht->goffset, t+ht->goffset, ht->gtype);
      }
      else if(ht->rel==1){
	return 1;
      }
      else{
	temp->hits++;
	return 1;
      }
    }
    temp=temp->next;
  }
  return 0;
}
void HT::addNode(tupleHT* ht, tuple* t){
  int slot=hashFun(t+ht->koffset, ht->ksize, ht->seed);
  if(slot&(1<<31)){
    slot=-slot;
  }
  slot=slot%ht->tableSize;
    if(verbose>2){
      printf("Trying: %s with grouper: %d\n", (char*)t+ht->koffset, *(int*)(t+ht->goffset));
    }
  if(!findNode(ht, t, slot)){
    if(verbose>2){
    printf("Adding: %s with grouper: %d\n", (char*)t+ht->koffset, *(int*)(t+ht->goffset));
    }
    tupleEnt* newTE = (tupleEnt*)calloc(1, sizeof(tupleEnt));
    newTE->t=(tuple*)calloc(1, ht->tupleSize);
    memcpy(newTE->t, t, ht->tupleSize);
    if(ht->op==COUNT){
      memset(newTE->t+ht->goffset, 0 , ht->gsize);
      AGR::doOp(ht->op, newTE->t+ht->goffset, NULL, ht->gtype);
    }
    newTE->next=ht->table[slot];
    ht->table[slot]=newTE;
    ht->items++;
    if(ht->items>ht->tableSize){
      resize(ht);
    }
  }
}

void HT::addNodeResize(tupleHT* ht, tupleEnt* te){
    int slot=hashFun(te->t+ht->koffset, ht->ksize, ht->seed);
  if(slot&(1<<31)){
    slot=-slot;
  }
  slot=slot%ht->tableSize;
  te->next=ht->table[slot];
  ht->table[slot]=te;
}

  //get all nodes as array of tuples
tuple* HT::getNodesAgr(tupleHT* ht){

  tuple* tArr = (tuple*)calloc(ht->items, ht->tupleSize);
  int index=0;
  int tupleSize=ht->tupleSize;
  for(int i =0;i<ht->tableSize;i++){
    tupleEnt* next=NULL, *temp=ht->table[i];
    while(temp){
      next=temp->next;
      if(ht->op==AVG&&temp->hits){
	int* val=(int*)(temp->t+ht->goffset);
	*val=*val/temp->hits;
      }
      memcpy(tArr+tupleSize*index, temp->t, tupleSize);
      index++;
      free(temp->t);
      free(temp);
      temp=next;
    }
  }
  
  return tArr;
}


tuple* HT::getNodesRel(tupleHT* ht){
  tuple* tArr = (tuple*)calloc(ht->items, ht->tupleSize);
  int index=0;
  int tupleSize=ht->tupleSize;
  ht->items=0;
  for(int i =0;i<ht->tableSize;i++){
    tupleEnt* next=NULL, *temp=ht->table[i];
    while(temp){
      next=temp->next;
      if(temp->hits){
      memcpy(tArr+tupleSize*index, temp->t, tupleSize);
      ht->items++;
      index++;
      }
      free(temp->t);
      free(temp);
      temp=next;
    }
  }
  return tArr;
}
