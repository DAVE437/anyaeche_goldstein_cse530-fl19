#pragma once
#include "aggregateOp.h"
#include "../hw1_lib/tuple.h"


#define initSize 1024
struct agrDesc;

typedef struct tupleEnt{
  tuple* t;
  int hits;
  tupleEnt* next;
}tupleEnt;

typedef struct tupleHT{
  tupleEnt** table;
  unsigned int seed;
  int tableSize;
  int items;
  agrOp op;
  int tupleSize;
  int koffset;
  int goffset;
  int ksize;
  int gsize;
  types gtype;
  int rel;

}tupleHT;

//this is a really inefficient hash table, if we do serious parallization ill change it up
namespace HT{
  //initializes new hashtable (aggregator)
  tupleHT* initTable(agrDesc* ad, tupleDesc* td);

  //init new hashtable (relational join)
  tupleHT* initTable(tupleDesc* td1,tupleDesc* td2, int f1, int f2);
    
  //checks if node exists in table, if does performs agr op on node and returns 1, else returns 0
  int findNode(tupleHT* ht, tuple* t, int slot);

  //resizes table if goes above load factor
  void resize(tupleHT* ht);
  
  //hash function
  int hashFun(unsigned char* relField, int len, unsigned int seed);

  //add  node to table
  void addNode(tupleHT* ht, tuple* t);

  //get all nodes as array of tuples
  tuple* getNodesAgr(tupleHT* ht);

  //add node function for resizing process, no alloc, no check, no items incr
  void addNodeResize(tupleHT* ht, tupleEnt* te);

  //gets nodes for relationalJoin
  tuple* getNodesRel(tupleHT* ht);
}
