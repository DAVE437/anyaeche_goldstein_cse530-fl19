#pragma once
//sql parser
#include <hsql/SQLParser.h>
#include <hsql/SQLParserResult.h>
#include <hsql/util/sqlhelper.h>
#include <hsql/sql/SQLStatement.h>
#include <hsql/sql/statements.h>
#include <hsql/sql/Expr.h>
#include "relation.h"
#include "aggregator.h"
#include "../hw1_lib/heapFile.h"

extern heapFile* hf;
extern catalog_t* log;
extern int silent;
namespace MP{


  void handleQuery(char* buf);
  int getType(char* line);
  void parseFile();
  int testRel(char* line);
  int testAgr(char* line);




}
