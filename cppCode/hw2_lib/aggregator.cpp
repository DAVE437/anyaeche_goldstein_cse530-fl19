#include "aggregator.h"



//tuples to be merged will have been projected to proper colums first
void AGR::mergeGB(tupleHT* ht, tuple* t){
  HT::addNode(ht, t);
}
void AGR::mergeOF(retTuple* rt, agrDesc* ad, tuple* t){
  doOp(ad->op, rt->t, t,rt->td->field_types[ad->fields[0]]);
  rt->len++;
}

retTuple* AGR::agrOpInit(retTuple* rt, agrDesc* ad){
  retTuple* initRT = TUP::createRT(rt->td, !ad->groupBy);
  return initRT;
}

retTuple* AGR::getResults(tupleHT* ht, agrDesc* ad, retTuple* rt, retTuple* resultRT){
  if(ad->groupBy){
  resultRT->t=HT::getNodesAgr(ht);
  resultRT->len=ht->items;
  resultRT->size=ht->items;
  }
  else{
    if(ad->op==AVG&&resultRT->len){
	int* val=(int*)(resultRT->t);
	*val=*val/resultRT->len;

      }
    resultRT->len=1;
  }
  return resultRT;
}
int AGR::doOp(agrOp op, void* a, void* b, types t){
  if(t){
    return doOpStr(op, (char*)a, (char*)b);
  }
  return doOpInt(op, (int*)a, (int*)b);
}
int AGR::doOpInt(agrOp op, int* a, int* b){
  switch(op)
    {
    case MAX:
      *a=std::max(*a,*b);
      return 1;
    case MIN:
      *a=std::min(*a, *b);
      return 1;
    case AVG:
      *a= *a+*b;
      return 1;
    case COUNT:
      *a=*a+1;
      return 1;
    case SUM:
      *a=*a+*b;
      return 1;
    default:
      fprintf(stderr,"Bad aggregate op\nexiting...\n");
      exit(0);
    }
}

int AGR::doOpStr(agrOp op, char* a, char* b){
  switch(op)
    {
    case MAX:
      return 0;
    case MIN:
      return 0;
    case AVG:
      return 0;
    case COUNT:
      *a=*a+1;
      return 1;
    case SUM:
      return 0;
    default:
      fprintf(stderr,"Bad aggregate op\nexiting...\n");
      exit(0);
    }
}
