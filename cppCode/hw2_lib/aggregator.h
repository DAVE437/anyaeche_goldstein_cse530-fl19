#pragma once

#include <algorithm>
#include "../hw1_lib/tuple.h"
#include "hashtable.h"
#include "aggregateOp.h"

//#define min(X, Y)  ((X) < (Y) ? (X) : (Y))
//#define max(X, Y)  ((X) < (Y) ? (Y) : (X))
#define agrFields 2
/*int min(int a, int b){
  return ((a) < (b) ? (a) : (b))
}

int min(int a, int b){
  return ((a) < (b) ? (b) : (a))
}*/

typedef struct agrDesc{
  int groupBy;
  int fields[agrFields];
  agrOp op;
  
  //description of tuples that are being operated on
  //  tupleDesc* td;
}agrDesc;


namespace AGR{

  //merges in a new tuple (t) defined by tupleDesc (td) into table (ht)
  //as is defined by the arg (ad) if groupBy is true
  void mergeGB(tupleHT* ht, tuple* t);

  //merge if no group by
  void mergeOF(retTuple* rt, agrDesc* ad, tuple* t);
  
  //set up the ret tuple struct for aggregation
  retTuple* agrOpInit(retTuple* rt, agrDesc* ad);
  
  //gets results of mergin in all tuples of rt based on op ad
  retTuple* getResults(tupleHT* ht, agrDesc* ad, retTuple* rt, retTuple* resultRT);

  //generic do op, will select int/str depending on t
  int doOp(agrOp op, void* a, void* b, types t);
  
  //does integer operation specified by op
  int doOpInt(agrOp op, int* a, int* b);

  //dont need len because know len<=129
  //dont need len because know len<=129
  int doOpStr(agrOp op, char* a, char* b);

}

