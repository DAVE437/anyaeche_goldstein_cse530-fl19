#pragma once
#include "../hw1_lib/tuple.h"
#include "hashtable.h"
#include "relationalOp.h"

#ifndef numTypes
#define numTypes 2
#endif
extern const int sizes[numTypes];

namespace REL{
  //does select on a list of tuples and returns list of tuples who returned true
  //for operation defined by op on field defined by field
  retTuple* select(retTuple* rt, relOp op, void* cmpVal, int field, retTuple* selectRT);
  //renames the fields of a retTuple
  retTuple* rename(retTuple* rt, char* fName, int field);
  //returns list of tuples only including fields specified in fields array
  retTuple* project(retTuple* rt, int fieldsMask, retTuple* projectRT);
  //returns a list of tuples that are shared by list r1 and r2 on fields f1 and f2
  retTuple* join(retTuple* r1, retTuple* r2, int f1, int f2, retTuple* joinRT);

  //initialize a retTuple for new rel operation
  retTuple* relOpInit(retTuple* rt,int fieldsMask, int nfields);

    //does comparison, takes voids * and returns doOpInt or doOpStr (depending on type)
    int doOp(relOp op, void* a, void* b, types t);
  //does integer operation specified by op
  //this might be one of the few times that the JIT will outperform gcc
  int doOpInt(relOp op, int* a, int* b); 

  //does stirng operation specified by op
  //dont need len because know len<=129
  int doOpStr(relOp op, char* a, char* b);





}
