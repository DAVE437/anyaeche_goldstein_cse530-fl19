//standard includes 
#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <sched.h>
#include <stdarg.h>
#include <errno.h>
#include <unistd.h>
#include <stdbool.h>
#include <assert.h>
#include <fstream>

#define numTypes 2
const extern int sizes[numTypes]={4,128};
//helper includes
#include "../utils/arg.h"


//hw4_includes
#include "../hw4_lib/bufferPool.h"
#include "../hw4_lib/myLock.h"


//hw1 includes
#include "../hw1_lib/database.h"
#include "../hw1_lib/heapFile.h"
#include "../hw1_lib/tupleDesc.h"
#include <hsql/SQLParser.h>
#include <hsql/SQLParserResult.h>
#include <hsql/util/sqlhelper.h>
#include <hsql/sql/SQLStatement.h>
#include <hsql/sql/statements.h>
#include <hsql/sql/Expr.h>
hsql::SQLParserResult parseMyResult;
//hw2 includes
#include "../hw2_lib/relation.h"
#include "../hw2_lib/aggregator.h"
#include "../hw2_lib/myParser.h"


catalog_t* log=NULL;
heapFile* hf=NULL, *hf2=NULL;
pthread_barrier_t* startBarrier;
pthread_barrier_t* endBarrier;
pthread_t* threads;

//arguments
int verbose=0;
int ops = 0;
char* myQuery=NULL;
char* trace=NULL;
char* dbPath=NULL;
char* tName=NULL;
int echo=0;
int newt=0;
int silent=0;
int queryCutOff=0;
int nthreads=1;
int locks=0;
#define Version "0.1"


static ArgOption args[] = {
  // Kind, 	  Method,	name,	        reqd,  variable,	 help
  { KindOption,   Integer,	"-v", 	        0,     &verbose, 	 "verbose mode" },
  { KindOption,   Integer,	"-t", 	        0,     &nthreads, 	 "num threads" },
  { KindOption,   Integer,      "--locks",      0,     &locks,  	 "do lock test" },
  { KindOption,   Set,   	"--silent", 	0,     &silent, 	 "silent mode (overrides verbose)" },
  { KindOption,   Set, 		"--echo",       0,     &echo,            "echo args" },
  { KindOption,   Integer,      "--ops",        0,     &ops,      	 "How many ops (inserts/queryies to do (use this OR trace, NOT BOTH!)" },
  { KindOption,   String,	"--query",	0,     &myQuery,         "query to run" },
  { KindOption,   String,      "--trace",       0,     &trace,      	 "If using a tracefile specified here" },
  { KindOption,   String,      "--db",          0,     &dbPath,          "Dir that will actually server as the database" },
  { KindOption,   String,      "--table",       0,     &tName,           "Name of the table to use" },
  { KindOption,   Set,         "--new",         0,     &newt,            "Name of the table to use" },
  { KindHelp,     Help, 	"-h" },
  { KindEnd }
};
static ArgDefs argp = { args, "Args Processor", Version, NULL };



//for testing locks
volatile long incr = 0;
#define ITER 500000
#define tillTimeout 45
typedef struct lockTest{
  myLock* l;
  int tid;
  int nLocks;
}lockTest;

void smartFree(void** ptr, int iter){
  for(int i =0;i<iter;i++){
    free(ptr[i]);
  }
  free(ptr);
}


void startDB(){
  int result=DB::createDB(dbPath);
  if(result==-1){
    fprintf(stderr,"Error creating db at path: %s\n", dbPath);
    exit(0);
  }
  log=LOG::createCatalog(dbPath);
  if(!log){
    fprintf(stderr,"Error creating catalog!\n");
    exit(0);
  }


  if(newt){
    int* fTypes=(int*)calloc(numTypes, sizeof(int));
    char** fNames=(char**)calloc(numTypes, sizeof(char*));
    for(int i =0;i<numTypes;i++){
      fTypes[i]=i;
      fNames[i]=(char*)calloc(field_name_length,sizeof(char));
    }
    sprintf(fNames[0], "Field_INT");
    sprintf(fNames[1], "Field_STRING");
    hf=HF::createHeapFile(log, tName, TD::newTupleDesc(numTypes, 1, fTypes, fNames));
    if(!hf){
      fprintf(stderr,"Error creating new heapfile!\n");
      exit(0);
    }
    //  smartFree((void**)fTypes,0);
    //  smartFree((void**)fNames, numTypes);
  }
  else{
    hf=HF::reaccessHeapFile(log, tName);
    if(!hf){
      fprintf(stderr,"Error creating new heapfile!\n");
      exit(0);
    }
  }
}

tuple* genTuple(){
  int bound=TD::getSizeTuple(hf->desc);
  tuple* t=(tuple*)calloc(bound,sizeof(char));
  for(int i=sizeof(int);i<bound;i++){
    t[i]=(rand()%26)+65;
  }
  int f1=rand();
  memcpy(t, &f1, 4);
  return t;
}


void deleteNoThere(thrHeapFile* lhf,tuple* t){
  if(HF::deleteTuple(lhf, t)){
    fprintf(stderr,"Error in delete, either issue accessing the page or return true on impossible input\n");
    exit(0);
  }
}
void deleteIsThere(thrHeapFile* lhf,tuple* t){
  if(1!=HF::deleteTuple(lhf, t)&&nthreads<2){
    fprintf(stderr,"Depending on the arguments given to helpers, this might indicate an error in delete!\n");
    exit(0);
    }
}

void add(thrHeapFile* lhf, tuple* t){
  if(HF::addTuple(lhf, t)){
    fprintf(stderr,"Error adding tuple!\n");
    HF::cleanup(lhf);
    exit(0);
  }
  //  smartFree((void**)t, 0);
}



void* runTrace(void* argt){
  unsigned long tid = (unsigned long)argt;
  FILE* fp=fopen(trace,"r");
  if(!fp){
    fprintf(stderr,"Error opening trace file!\n");
    exit(0);
  }
  thrHeapFile* lhf = HF::initThrHF(log->treg, hf);
  if(verbose>2){
    fprintf(stderr,"treg: %p\nTables: %p\nNtables: %d\nSize: %d\ncsize: %d\n",
	    log->treg,
	    log->treg->tables,
	    log->treg->numTables,
	    log->treg->size,
	    log->treg->cacheSize);
    fprintf(stderr,"LHF: %p\nfp: %p\nmyTable: %p\nCurItem: %p\nCurPGN: %d\n",
	    lhf, lhf->fp, lhf->myTable, lhf->curItem, lhf->curPageNum);
    fprintf(stderr,"Table: %p == %p\nHF: %p->%s\naitem: %p\ntachce: %p\n",
	    lhf->myTable,
	    log->treg->tables[0],
	    lhf->myTable->tableDef,
	    lhf->myTable->tableDef->fName,
	    lhf->myTable->aItems,
	    lhf->myTable->tcache);
  }
  char* buf=(char*)calloc(256, sizeof(char));
  char op, *tup=(char*)calloc(TD::getSizeTuple(hf->desc), sizeof(short));
  int * intPort=(int*)calloc(1, 16);
  char * strPort=(char*)calloc(1, 256);
  pthread_barrier_wait(startBarrier);
  while(fgets(buf, 256, fp)){
    memset(tup, 0 ,TD::getSizeTuple(hf->desc)*sizeof(short));
    memset(intPort, 0 ,16);
    memset(strPort, 0 ,256);
    if(sscanf(buf,"%c %d %s\n", &op, intPort, strPort)<1){
      fprintf(stderr,"Bad Trace File Format Scan!\n%s\nExiting...", buf);
      //HF::cleanup(hf);
      exit(0);
    }
    memcpy(tup, intPort,4);
    memcpy(tup+4, strPort,128);
    switch(op){
    case 'I':
      add(lhf, (tuple*)tup);
      break;
    case 'D':
      deleteIsThere(lhf, (tuple*)tup);
      break;
    case 'Q':
      buf[strlen(buf)-2]=0;
      //fprintf(stderr,"Buf: %s\n", buf);
      MP::handleQuery(buf+3);
      break;
    default:
      fprintf(stderr,"Bad Trace File Format!\n");
      //HF::cleanup(hf);
      exit(0);
    }
  }
  pthread_barrier_wait(endBarrier);
  HF::cleanup(lhf);
  if(!tid){
    HF::cleanupCache(lhf->myTable);
  }
}


int runner(){
  startDB();
  if(trace[0]){
    for(unsigned long i=0;i<nthreads;i++){
      pthread_create(&threads[i], NULL, runTrace, (void*)i);
    }
    for(int i=0;i<nthreads;i++){
      pthread_join(threads[i], NULL);
    }
  }
  else if(myQuery[0]){
    if(nthreads > 1){
      fprintf(stderr,"Havent implemented multithreaded query support yet!\n");
      exit(0);
    }
      MP::handleQuery(myQuery);
  }

  //  HF::cleanup(hf);
}

void* readsOnOff(void* argt){
 lockTest *lt = (lockTest*)argt;
  myLock* larr=lt->l;
  int tid = lt->tid;
  int nLocks = lt->nLocks;
  pthread_barrier_wait(startBarrier);
  for(int i=0;i<nLocks;i++){
    LK::lock_rd(&larr[i], 0);
  }
  for(int i=0;i<nLocks;i++){
    LK::unlock(&larr[i]);
  }

  for(int i=0;i<nLocks;i++){
    LK::lock_rd(&larr[i], 0);
  }
  if(tid&1){
    for(int i=0;i<nLocks;i++){
      LK::unlock(&larr[i]);
    }
    for(int i=0;i<nLocks;i++){
      LK::lock_rd(&larr[i], 0);
    }
  }
  for(int i=0;i<nLocks;i++){
    LK::unlock(&larr[i]);
  }
  return NULL;
}
void* canSwapAndMultiread(void* argt){
  lockTest *lt = (lockTest*)argt;
  myLock* larr=lt->l;
  int tid = lt->tid;
  int nLocks = lt->nLocks;
  volatile int iStop =nLocks;
  pthread_barrier_wait(startBarrier);
  for(int i=0;i<nLocks;i++){
    if(RETRY==LK::lock_rd(&larr[i], 1<<20)){
      iStop = i;
      break;
    }
  }
  if(tid){
    for(int i=0;i<iStop;i++){
      LK::unlock(&larr[i]);
    }
  }
  else{
    for(int i=0;i<nLocks;i++){
      assert(LK::getMode(&larr[i]) >= 1);
      LK::swap_mode(&larr[i],0);
      assert(LK::getMode(&larr[i]) == wrlocked);
    }
  }
  return NULL;
}
void* resolveDeadlock(void* argt){
  lockTest *lt = (lockTest*)argt;
  myLock* larr=lt->l;
  int tid = lt->tid;
  int nLocks = lt->nLocks;
  pthread_barrier_wait(startBarrier);
  if(tid&1){
    for(int i = nLocks-1 ;i>=0;i--){
      LK::lock_wr(&larr[i], 1<<20);
    }
  }
  else{
    for(int i= 0;i<nLocks;i++){
      LK::lock_wr(&larr[i], 1<<20);
    }
  }

  return NULL;
}
void* incrTest(void* argt){
  myLock* l = (myLock*)argt;
  pthread_barrier_wait(startBarrier);
  for(int i=0;i<ITER;i++){
    LK::lock_wr(l, 0);
    incr++;
    LK::unlock(l);
  }
  return NULL;
}
void* doLockTest(void* argt){
  int nLocks = 1<<15;
  myLock* locks = (myLock*)calloc(nLocks+1, sizeof(myLock));
  for(int i=0;i<=nLocks;i++){
    LK::initExst(&locks[i]);
  }
  pthread_t* t= (pthread_t*)calloc(nthreads, sizeof(pthread_t));
  lockTest* ltarr = (lockTest*)calloc(nthreads, sizeof(lockTest));
  startBarrier=(pthread_barrier_t*)calloc(1, sizeof(pthread_barrier_t));
  pthread_barrier_init(startBarrier, NULL, nthreads);
  if(verbose>1){
  printf("1/4: Testing Deadlock Resolution\n");
  }
  for(int i=0;i<nthreads;i++){
    ltarr[i].tid = i;
    ltarr[i].l = locks;
    ltarr[i].nLocks = nLocks;
    pthread_create(&t[i], NULL, resolveDeadlock, (void*)(&ltarr[i]));
  }
  for(int i=0;i<nthreads;i++){
    pthread_join(t[i], NULL);
  }

    if(verbose>1){
      printf("1/4: Deadlock Resolution Test Passed\n\n");
  }
    
  pthread_barrier_init(startBarrier, NULL, nthreads);
    for(int i=0;i<nLocks;i++){
      assert(LK::getMode(&locks[i]) == wrlocked);
      LK::unlock(&locks[i]);
      assert(LK::getMode(&locks[i]) == unlocked);
    }
    if(verbose>1){
      printf("2/4: Testing Race Free Incrementing\n");
  }
    
    for(int i=0;i<nthreads;i++){
    pthread_create(&t[i], NULL, incrTest, (void*)(locks));
  }
  for(int i=0;i<nthreads;i++){
    pthread_join(t[i], NULL);
  }
  assert(incr == ITER*nthreads);
      if(verbose>1){
      printf("2/4: Testing Race Free Incrementing Passed\n\n");
  }
  pthread_barrier_init(startBarrier, NULL, nthreads);
      if(verbose>1){
	printf("3/4: Testing Multiple Readers Locking/Unlocking\n");
  }
      
  for(int i=0;i<nthreads;i++){
    pthread_create(&t[i], NULL, readsOnOff, (void*)(&ltarr[i]));
  }
  for(int i=0;i<nthreads;i++){
    pthread_join(t[i], NULL);
  }
  for(int i=0;i<nLocks;i++){
    assert(LK::getMode(&locks[i]) == unlocked);
  }
        if(verbose>1){
	printf("3/4: Testing Multiple Readers Locking/Unlocking Passed\n\n");
  }

	pthread_barrier_init(startBarrier, NULL, nthreads);
  if(verbose>1){
    printf("4/4: Testing Swapping Lock Mode\n");
  }
  
  for(int i=0;i<nthreads;i++){
    pthread_create(&t[i], NULL, canSwapAndMultiread, (void*)(&ltarr[i]));
  }
  for(int i=0;i<nthreads;i++){
    pthread_join(t[i], NULL);
  }
  for(int i=0;i<nLocks;i++){
    assert(LK::getMode(&locks[i]) == wrlocked);
    LK::swap_mode(&locks[i], 0);
    assert(LK::getMode(&locks[i]) == 1);
    LK::unlock(&locks[i]);
    assert(LK::getMode(&locks[i]) == unlocked);
  }
  if(verbose>1){
    printf("4/4: Testing Swapping Lock Mode Passed\n\n");
  }
  free(locks);
  free(t);
  free(ltarr);

}
int main(int argc, char** argv){
  srand(time(NULL));
  trace=(char*)calloc(64,sizeof(char));
  dbPath=(char*)calloc(64,sizeof(char));
  tName=(char*)calloc(64, sizeof(char));
  myQuery=(char*)calloc(256, sizeof(char));
  ArgParser* ap = createArgumentParser(&argp);
  int ok = parseArguments(ap, argc, argv);
  if (ok){
    fprintf(stderr,"Error parsing args\n");
    return -1;
  }
  if(locks){
    if(verbose>0){
      fprintf(stderr,"Doing Lock Tests\n");
    }
    int counter = locks/100;
    int comp=0;

    for(int i=0;i<locks;i++){
      incr = 0;
      if(i>counter){
	comp++;
	if(!counter){
	  comp=i*(100/locks);
	}

	if(verbose>0){
	  fprintf(stderr, "\r%d%% Complete", comp);
	}
	
	counter+=locks/100;
      }
    pthread_t lt;
    struct timespec tout;
    if (clock_gettime(CLOCK_REALTIME, &tout) == -1){
      fprintf(stderr,"Your system is fucked mate\n");
      exit(0);
    }
    tout.tv_sec+=tillTimeout;
    pthread_create(&lt, NULL, doLockTest, NULL);
    if(pthread_timedjoin_np(lt, NULL, &tout)){
      fprintf(stderr,"Lock test timed out. Deadlock likely!\n");
      exit(0);
    }
    }
    if(verbose>0){
      printf("\r%d%% Complete\nLock Tests Passed\n", 100);
    }
    return 0;
  }
  //  sqlFileRead.open("qp.txt");
  verbose=verbose*(!silent);
  if(echo){
    for(int i =0;i<argc;i++){
      printf("%s ", argv[i]);
    }
    printf("\n");
  }

  if(trace[0]&&ops){
    fprintf(stderr,"Please either specify a trace or ops, not both!\n");
    return -1;
  }
  if(!dbPath[0]){
    fprintf(stderr,"Please specify a drir to use the database\n");
    return -1;
  }
  if(!tName[0]){
    fprintf(stderr,"Please specify a file to use as the database!\n");
    return -1;
  }
  threads = (pthread_t*)calloc(nthreads, sizeof(pthread_t));
  startBarrier=(pthread_barrier_t*)calloc(1, sizeof(pthread_barrier_t));
  endBarrier=(pthread_barrier_t*)calloc(1, sizeof(pthread_barrier_t));
  pthread_barrier_init(startBarrier, NULL, nthreads);
  pthread_barrier_init(endBarrier, NULL, nthreads);
  runner();
  
  return 0;
}
