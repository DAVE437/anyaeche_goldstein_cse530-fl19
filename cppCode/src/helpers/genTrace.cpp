#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <time.h>
#include "../../utils/arg.h"


#define Version "0.1"
int intMax=RAND_MAX;
int verbose=0;
int len=0;
int insertp=0;
int deletep=0;
int groups=0;
int addQueries=0;
char* fName=NULL;
static ArgOption args[] = {
  // Kind, 	  Method,	name,	        reqd,  variable,	 help
  { KindOption,   Set, 		"-v", 	        0,     &verbose, 	 "verbose mode" },
  { KindOption,   Set, 		"--aq",         0,     &addQueries, 	 "add queries to file" },
  { KindOption,   Integer,      "--len",        0,     &len,             "Amount of items" },
  { KindOption,   Integer,      "--im",         0,     &intMax,          "Max int for intfield" },
  { KindOption,   Integer,      "--groups",     0,     &groups,          "Amount of groups" },
  { KindOption,   Integer,      "--insert",     0,     &insertp,         "Insert percentage" },
  { KindOption,   Integer,      "--delete",     0,     &deletep,         "Delete percentage" },
  { KindOption,   String,       "--file",       0,     &fName,           "File Name" },
  { KindHelp,     Help, 	"-h" },
  { KindEnd }
};
static ArgDefs argp = { args, "Args Processor", Version, NULL };


#define slen 128


void genStr(char** prev,char* ptr, int ind, char** gNames){
  if(!gNames){
    for(int i =0;i<slen;i++){
      ptr[i]=rand()%26+65;
    }
  }
  else{
    strcpy(ptr,gNames[rand()%groups]);
  }
  prev[ind]=(char*)calloc(256, sizeof(char));
  memcpy(prev[ind], ptr, slen);
}
int main(int argc, char** argv){
  srand(time(NULL));
  fName=(char*)calloc(128, sizeof(char));
  ArgParser* ap = createArgumentParser(&argp);
  int ok = parseArguments(ap, argc, argv);
  if (ok){
    fprintf(stderr,"Error parsing args\n");
    return -1;
  }
  if(!fName[0]){
    printf("specify a file!\n");
    return -1;
  }
  char** gNames=NULL;
  if(groups){
    gNames=(char**)calloc(groups, sizeof(char*));
    for(int i =0;i<groups;i++){
      gNames[i]=(char*)calloc(16, sizeof(char));
      sprintf(gNames[i],"Group_%d", i);
    }
  }
  FILE* fp=fopen(fName,"w");
  char* mystr=(char*)calloc(256, sizeof(char));
  char** prevStr=(char**)calloc(len, sizeof(char**));
  int* prevInt = (int*)calloc(len, sizeof(int));
  int ti=0;
  int myint=0;
  for(int i =0;i<len;i++){
    int todo=(!!i)*rand()%100;
    char op;
    genStr(prevStr,mystr, ti, gNames);

    myint=rand()%intMax;
    prevInt[ti]=myint;
    ti++;
    op='I';
    fprintf(fp,"%c %d %s\n", op, myint, mystr);
    if(todo<deletep){
      int index=0;
      int attempt=0;
      while(1){
	attempt++;
	if(attempt>10){
	  break;
	}
	index=rand()%ti;
	if(prevStr[index][0]){
	  break;
	}
      }
      if(attempt>10){
	continue;
      }
      memcpy(mystr, prevStr[index], slen);
      prevStr[index][0]=0;
      myint=prevInt[index];
      op='D';
      fprintf(fp,"%c %d %s\n", op, myint, mystr);
    }
  }
  //note this is not a complete test, this is a reasonable list but emacs macros are only so strong and it felt almost morally wrong to write a helper script to format the helper script...
  if(addQueries){
    fprintf(fp,"Q \"SELECT count(Field_STRING) as MyAliasTest FROM t1;\"\n");
    fprintf(fp,"Q \"SELECT count(Field_STRING) FROM t1;\"\n");
    fprintf(fp,"Q \"SELECT count(Field_INT) as world FROM t1;\"\n");
    fprintf(fp,"Q \"SELECT min(Field_INT) as world FROM t1;\"\n");
    fprintf(fp,"Q \"SELECT max(Field_INT) as world FROM t1;\"\n");
    fprintf(fp,"Q \"SELECT avg(Field_INT) as world FROM t1;\"\n");
    fprintf(fp,"Q \"SELECT sum(Field_INT) as world FROM t1;\"\n");
    fprintf(fp,"Q \"SELECT count(Field_INT), Field_STRING as MyAliasTest FROM t1 group by Field_STRING;\"\n");
    fprintf(fp,"Q \"SELECT min(Field_INT), Field_STRING as MyAliasTest FROM t1 group by Field_STRING;\"\n");
    fprintf(fp,"Q \"SELECT max(Field_INT), Field_STRING as MyAliasTest FROM t1 group by Field_STRING;\"\n");
    fprintf(fp,"Q \"SELECT avg(Field_INT), Field_STRING as MyAliasTest FROM t1 group by Field_STRING;\"\n");
    fprintf(fp,"Q \"SELECT sum(Field_INT), Field_STRING as MyAliasTest FROM t1 group by Field_STRING;\"\n");
    fprintf(fp,"Q \"SELECT count(Field_INT), Field_STRING as MyAliasTest FROM t1;\"\n");
    fprintf(fp,"Q \"SELECT min(Field_INT), Field_STRING as MyAliasTest FROM t1;\"\n");
    fprintf(fp,"Q \"SELECT max(Field_INT), Field_STRING as MyAliasTest FROM t1;\"\n");
    fprintf(fp,"Q \"SELECT avg(Field_INT), Field_STRING as MyAliasTest FROM t1;\"\n");
    fprintf(fp,"Q \"SELECT sum(Field_INT), Field_STRING as MyAliasTest FROM t1;\"\n");
    fprintf(fp,"Q \"SELECT Field_INT, Field_STRING FROM t1;\"\n");
    fprintf(fp,"Q \"SELECT count(Field_INT) as world FROM t1 where Field_INT < %d;\"\n", intMax/2);
    fprintf(fp,"Q \"SELECT count(Field_INT) as world FROM t1 where Field_INT = %d;\"\n", intMax/2);
    fprintf(fp,"Q \"SELECT count(Field_INT) as world FROM t1 where Field_INT > %d;\"\n", intMax/2);
    fprintf(fp,"Q \"SELECT count(Field_INT) as world FROM t1 where Field_INT <= %d;\"\n", intMax/2);
    fprintf(fp,"Q \"SELECT count(Field_INT) as world FROM t1 where Field_INT >= %d;\"\n", intMax/2);
    fprintf(fp,"Q \"SELECT min(Field_INT) as world FROM t1 where Field_INT < %d;\"\n", intMax/2);
    fprintf(fp,"Q \"SELECT min(Field_INT) as world FROM t1 where Field_INT = %d;\"\n", intMax/2);
    fprintf(fp,"Q \"SELECT min(Field_INT) as world FROM t1 where Field_INT > %d;\"\n", intMax/2);
    fprintf(fp,"Q \"SELECT min(Field_INT) as world FROM t1 where Field_INT <= %d;\"\n", intMax/2);
    fprintf(fp,"Q \"SELECT min(Field_INT) as world FROM t1 where Field_INT >= %d;\"\n", intMax/2);
    fprintf(fp,"Q \"SELECT max(Field_INT) as world FROM t1 where Field_INT < %d;\"\n", intMax/2);
    fprintf(fp,"Q \"SELECT max(Field_INT) as world FROM t1 where Field_INT = %d;\"\n", intMax/2);
    fprintf(fp,"Q \"SELECT max(Field_INT) as world FROM t1 where Field_INT > %d;\"\n", intMax/2);
    fprintf(fp,"Q \"SELECT max(Field_INT) as world FROM t1 where Field_INT <= %d;\"\n", intMax/2);
    fprintf(fp,"Q \"SELECT max(Field_INT) as world FROM t1 where Field_INT >= %d;\"\n", intMax/2);
    fprintf(fp,"Q \"SELECT avg(Field_INT) as world FROM t1 where Field_INT < %d;\"\n", intMax/2);
    fprintf(fp,"Q \"SELECT avg(Field_INT) as world FROM t1 where Field_INT = %d;\"\n", intMax/2);
    fprintf(fp,"Q \"SELECT avg(Field_INT) as world FROM t1 where Field_INT > %d;\"\n", intMax/2);
    fprintf(fp,"Q \"SELECT avg(Field_INT) as world FROM t1 where Field_INT <= %d;\"\n", intMax/2);
    fprintf(fp,"Q \"SELECT avg(Field_INT) as world FROM t1 where Field_INT >= %d;\"\n", intMax/2);
    fprintf(fp,"Q \"SELECT sum(Field_INT) as world FROM t1 where Field_INT < %d;\"\n", intMax/2);
    fprintf(fp,"Q \"SELECT sum(Field_INT) as world FROM t1 where Field_INT = %d;\"\n", intMax/2);
    fprintf(fp,"Q \"SELECT sum(Field_INT) as world FROM t1 where Field_INT > %d;\"\n", intMax/2);
    fprintf(fp,"Q \"SELECT sum(Field_INT) as world FROM t1 where Field_INT <= %d;\"\n", intMax/2);
    fprintf(fp,"Q \"SELECT sum(Field_INT) as world FROM t1 where Field_INT >= %d;\"\n", intMax/2);
    fprintf(fp,"Q \"SELECT count(Field_INT), Field_STRING as MyAliasTest FROM t1 where Field_INT < %d group by Field_STRING;\"\n", intMax/2);
    fprintf(fp,"Q \"SELECT count(Field_INT), Field_STRING as MyAliasTest FROM t1 where Field_INT = %d group by Field_STRING;\"\n", intMax/2);
    fprintf(fp,"Q \"SELECT count(Field_INT), Field_STRING as MyAliasTest FROM t1 where Field_INT > %d group by Field_STRING;\"\n", intMax/2);
    fprintf(fp,"Q \"SELECT count(Field_INT), Field_STRING as MyAliasTest FROM t1 where Field_INT <= %d group by Field_STRING;\"\n", intMax/2);
    fprintf(fp,"Q \"SELECT count(Field_INT), Field_STRING as MyAliasTest FROM t1 where Field_INT >= %d group by Field_STRING;\"\n", intMax/2);
    fprintf(fp,"Q \"SELECT min(Field_INT), Field_STRING as MyAliasTest FROM t1 where Field_INT < %d group by Field_STRING;\"\n", intMax/2);
    fprintf(fp,"Q \"SELECT min(Field_INT), Field_STRING as MyAliasTest FROM t1 where Field_INT = %d group by Field_STRING;\"\n", intMax/2);
    fprintf(fp,"Q \"SELECT min(Field_INT), Field_STRING as MyAliasTest FROM t1 where Field_INT > %d group by Field_STRING;\"\n", intMax/2);
    fprintf(fp,"Q \"SELECT min(Field_INT), Field_STRING as MyAliasTest FROM t1 where Field_INT <= %d group by Field_STRING;\"\n", intMax/2);
    fprintf(fp,"Q \"SELECT min(Field_INT), Field_STRING as MyAliasTest FROM t1 where Field_INT >= %d group by Field_STRING;\"\n", intMax/2);
    fprintf(fp,"Q \"SELECT max(Field_INT), Field_STRING as MyAliasTest FROM t1 where Field_INT < %d group by Field_STRING;\"\n", intMax/2);
    fprintf(fp,"Q \"SELECT max(Field_INT), Field_STRING as MyAliasTest FROM t1 where Field_INT = %d group by Field_STRING;\"\n", intMax/2);
    fprintf(fp,"Q \"SELECT max(Field_INT), Field_STRING as MyAliasTest FROM t1 where Field_INT > %d group by Field_STRING;\"\n", intMax/2);
    fprintf(fp,"Q \"SELECT max(Field_INT), Field_STRING as MyAliasTest FROM t1 where Field_INT <= %d group by Field_STRING;\"\n", intMax/2);
    fprintf(fp,"Q \"SELECT max(Field_INT), Field_STRING as MyAliasTest FROM t1 where Field_INT >= %d group by Field_STRING;\"\n", intMax/2);
    fprintf(fp,"Q \"SELECT avg(Field_INT), Field_STRING as MyAliasTest FROM t1 where Field_INT < %d group by Field_STRING;\"\n", intMax/2);
    fprintf(fp,"Q \"SELECT avg(Field_INT), Field_STRING as MyAliasTest FROM t1 where Field_INT = %d group by Field_STRING;\"\n", intMax/2);
    fprintf(fp,"Q \"SELECT avg(Field_INT), Field_STRING as MyAliasTest FROM t1 where Field_INT > %d group by Field_STRING;\"\n", intMax/2);
    fprintf(fp,"Q \"SELECT avg(Field_INT), Field_STRING as MyAliasTest FROM t1 where Field_INT <= %d group by Field_STRING;\"\n", intMax/2);
    fprintf(fp,"Q \"SELECT avg(Field_INT), Field_STRING as MyAliasTest FROM t1 where Field_INT >= %d group by Field_STRING;\"\n", intMax/2);
    fprintf(fp,"Q \"SELECT sum(Field_INT), Field_STRING as MyAliasTest FROM t1 where Field_INT < %d group by Field_STRING;\"\n", intMax/2);
    fprintf(fp,"Q \"SELECT sum(Field_INT), Field_STRING as MyAliasTest FROM t1 where Field_INT = %d group by Field_STRING;\"\n", intMax/2);
    fprintf(fp,"Q \"SELECT sum(Field_INT), Field_STRING as MyAliasTest FROM t1 where Field_INT > %d group by Field_STRING;\"\n", intMax/2);
    fprintf(fp,"Q \"SELECT sum(Field_INT), Field_STRING as MyAliasTest FROM t1 where Field_INT <= %d group by Field_STRING;\"\n", intMax/2);
    fprintf(fp,"Q \"SELECT sum(Field_INT), Field_STRING as MyAliasTest FROM t1 where Field_INT >= %d group by Field_STRING;\"\n", intMax/2);
    fprintf(fp,"Q \"SELECT count(Field_INT), Field_STRING as MyAliasTest FROM t1 where Field_INT < %d;\"\n", intMax/2);
    fprintf(fp,"Q \"SELECT count(Field_INT), Field_STRING as MyAliasTest FROM t1 where Field_INT = %d;\"\n", intMax/2);
    fprintf(fp,"Q \"SELECT count(Field_INT), Field_STRING as MyAliasTest FROM t1 where Field_INT > %d;\"\n", intMax/2);
    fprintf(fp,"Q \"SELECT count(Field_INT), Field_STRING as MyAliasTest FROM t1 where Field_INT <= %d;\"\n", intMax/2);
    fprintf(fp,"Q \"SELECT count(Field_INT), Field_STRING as MyAliasTest FROM t1 where Field_INT >= %d;\"\n", intMax/2);
    fprintf(fp,"Q \"SELECT min(Field_INT), Field_STRING as MyAliasTest FROM t1 where Field_INT < %d;\"\n", intMax/2);
    fprintf(fp,"Q \"SELECT min(Field_INT), Field_STRING as MyAliasTest FROM t1 where Field_INT = %d;\"\n", intMax/2);
    fprintf(fp,"Q \"SELECT min(Field_INT), Field_STRING as MyAliasTest FROM t1 where Field_INT > %d;\"\n", intMax/2);
    fprintf(fp,"Q \"SELECT min(Field_INT), Field_STRING as MyAliasTest FROM t1 where Field_INT <= %d;\"\n", intMax/2);
    fprintf(fp,"Q \"SELECT min(Field_INT), Field_STRING as MyAliasTest FROM t1 where Field_INT >= %d;\"\n", intMax/2);
    fprintf(fp,"Q \"SELECT max(Field_INT), Field_STRING as MyAliasTest FROM t1 where Field_INT < %d;\"\n", intMax/2);
    fprintf(fp,"Q \"SELECT max(Field_INT), Field_STRING as MyAliasTest FROM t1 where Field_INT = %d;\"\n", intMax/2);
    fprintf(fp,"Q \"SELECT max(Field_INT), Field_STRING as MyAliasTest FROM t1 where Field_INT > %d;\"\n", intMax/2);
    fprintf(fp,"Q \"SELECT max(Field_INT), Field_STRING as MyAliasTest FROM t1 where Field_INT <= %d;\"\n", intMax/2);
    fprintf(fp,"Q \"SELECT max(Field_INT), Field_STRING as MyAliasTest FROM t1 where Field_INT >= %d;\"\n", intMax/2);
    fprintf(fp,"Q \"SELECT avg(Field_INT), Field_STRING as MyAliasTest FROM t1 where Field_INT < %d;\"\n", intMax/2);
    fprintf(fp,"Q \"SELECT avg(Field_INT), Field_STRING as MyAliasTest FROM t1 where Field_INT = %d;\"\n", intMax/2);
    fprintf(fp,"Q \"SELECT avg(Field_INT), Field_STRING as MyAliasTest FROM t1 where Field_INT > %d;\"\n", intMax/2);
    fprintf(fp,"Q \"SELECT avg(Field_INT), Field_STRING as MyAliasTest FROM t1 where Field_INT <= %d;\"\n", intMax/2);
    fprintf(fp,"Q \"SELECT avg(Field_INT), Field_STRING as MyAliasTest FROM t1 where Field_INT >= %d;\"\n", intMax/2);
    fprintf(fp,"Q \"SELECT sum(Field_INT), Field_STRING as MyAliasTest FROM t1 where Field_INT < %d;\"\n", intMax/2);
    fprintf(fp,"Q \"SELECT sum(Field_INT), Field_STRING as MyAliasTest FROM t1 where Field_INT = %d;\"\n", intMax/2);
    fprintf(fp,"Q \"SELECT sum(Field_INT), Field_STRING as MyAliasTest FROM t1 where Field_INT > %d;\"\n", intMax/2);
    fprintf(fp,"Q \"SELECT sum(Field_INT), Field_STRING as MyAliasTest FROM t1 where Field_INT <= %d;\"\n", intMax/2);
    fprintf(fp,"Q \"SELECT sum(Field_INT), Field_STRING as MyAliasTest FROM t1 where Field_INT >= %d;\"\n", intMax/2);
    fprintf(fp,"Q \"SELECT Field_INT, Field_STRING FROM t1 where Field_INT < %d;\"\n", intMax/2);
    fprintf(fp,"Q \"SELECT Field_INT, Field_STRING FROM t1 where Field_INT = %d;\"\n", intMax/2);
    fprintf(fp,"Q \"SELECT Field_INT, Field_STRING FROM t1 where Field_INT > %d;\"\n", intMax/2);
    fprintf(fp,"Q \"SELECT Field_INT, Field_STRING FROM t1 where Field_INT <= %d;\"\n", intMax/2);
    fprintf(fp,"Q \"SELECT Field_INT, Field_STRING FROM t1 where Field_INT >= %d;\"\n", intMax/2);
    fprintf(fp,"Q \"SELECT AVG(Field_INT), Field_STRING as world FROM t1 inner join t2 on t1.Field_INT=t2.Field_INT where Field_INT < 100 group by Field_STRING;\"\n");
    fprintf(fp,"Q \"SELECT AVG(Field_INT), Field_STRING as world FROM t1 inner join t2 on t1.Field_INT=t2.Field_INT where Field_INT < 700 group by Field_STRING;\"\n");
    fprintf(fp,"Q \"SELECT count(Field_STRING) as MyAliasTest FROM t1;\"\n");
    fprintf(fp,"Q \"SELECT count(Field_STRING) FROM t1;\"\n");
    fprintf(fp,"Q \"SELECT count(Field_INT) as world FROM t1 inner join t2 on t1.Field_INT=t2.Field_INT;\"\n");
    fprintf(fp,"Q \"SELECT min(Field_INT) as world FROM t1 inner join t2 on t1.Field_INT=t2.Field_INT;\"\n");
    fprintf(fp,"Q \"SELECT max(Field_INT) as world FROM t1 inner join t2 on t1.Field_INT=t2.Field_INT;\"\n");
    fprintf(fp,"Q \"SELECT avg(Field_INT) as world FROM t1 inner join t2 on t1.Field_INT=t2.Field_INT;\"\n");
    fprintf(fp,"Q \"SELECT sum(Field_INT) as world FROM t1 inner join t2 on t1.Field_INT=t2.Field_INT;\"\n");
    fprintf(fp,"Q \"SELECT count(Field_INT), Field_STRING as MyAliasTest FROM t1 inner join t2 on t1.Field_INT=t2.Field_INT group by Field_STRING;\"\n");
    fprintf(fp,"Q \"SELECT min(Field_INT), Field_STRING as MyAliasTest FROM t1 inner join t2 on t1.Field_INT=t2.Field_INT group by Field_STRING;\"\n");
    fprintf(fp,"Q \"SELECT max(Field_INT), Field_STRING as MyAliasTest FROM t1 inner join t2 on t1.Field_INT=t2.Field_INT group by Field_STRING;\"\n");
    fprintf(fp,"Q \"SELECT avg(Field_INT), Field_STRING as MyAliasTest FROM t1 inner join t2 on t1.Field_INT=t2.Field_INT group by Field_STRING;\"\n");
    fprintf(fp,"Q \"SELECT sum(Field_INT), Field_STRING as MyAliasTest FROM t1 inner join t2 on t1.Field_INT=t2.Field_INT group by Field_STRING;\"\n");
    fprintf(fp,"Q \"SELECT count(Field_INT), Field_STRING as MyAliasTest FROM t1 inner join t2 on t1.Field_INT=t2.Field_INT;\"\n");
    fprintf(fp,"Q \"SELECT min(Field_INT), Field_STRING as MyAliasTest FROM t1 inner join t2 on t1.Field_INT=t2.Field_INT;\"\n");
    fprintf(fp,"Q \"SELECT max(Field_INT), Field_STRING as MyAliasTest FROM t1 inner join t2 on t1.Field_INT=t2.Field_INT;\"\n");
    fprintf(fp,"Q \"SELECT avg(Field_INT), Field_STRING as MyAliasTest FROM t1 inner join t2 on t1.Field_INT=t2.Field_INT;\"\n");
    fprintf(fp,"Q \"SELECT sum(Field_INT), Field_STRING as MyAliasTest FROM t1 inner join t2 on t1.Field_INT=t2.Field_INT;\"\n");
    fprintf(fp,"Q \"SELECT Field_INT, Field_STRING FROM t1 inner join t2 on t1.Field_INT=t2.Field_INT;\"\n");
    fprintf(fp,"Q \"SELECT count(Field_INT) as world FROM t1 inner join t2 on t1.Field_INT=t2.Field_INT where Field_INT < %d;\"\n", intMax/2);
    fprintf(fp,"Q \"SELECT count(Field_INT) as world FROM t1 inner join t2 on t1.Field_INT=t2.Field_INT where Field_INT = %d;\"\n", intMax/2);
    fprintf(fp,"Q \"SELECT count(Field_INT) as world FROM t1 inner join t2 on t1.Field_INT=t2.Field_INT where Field_INT > %d;\"\n", intMax/2);
    fprintf(fp,"Q \"SELECT count(Field_INT) as world FROM t1 inner join t2 on t1.Field_INT=t2.Field_INT where Field_INT <= %d;\"\n", intMax/2);
    fprintf(fp,"Q \"SELECT count(Field_INT) as world FROM t1 inner join t2 on t1.Field_INT=t2.Field_INT where Field_INT >= %d;\"\n", intMax/2);
    fprintf(fp,"Q \"SELECT min(Field_INT) as world FROM t1 inner join t2 on t1.Field_INT=t2.Field_INT where Field_INT < %d;\"\n", intMax/2);
    fprintf(fp,"Q \"SELECT min(Field_INT) as world FROM t1 inner join t2 on t1.Field_INT=t2.Field_INT where Field_INT = %d;\"\n", intMax/2);
    fprintf(fp,"Q \"SELECT min(Field_INT) as world FROM t1 inner join t2 on t1.Field_INT=t2.Field_INT where Field_INT > %d;\"\n", intMax/2);
    fprintf(fp,"Q \"SELECT min(Field_INT) as world FROM t1 inner join t2 on t1.Field_INT=t2.Field_INT where Field_INT <= %d;\"\n", intMax/2);
    fprintf(fp,"Q \"SELECT min(Field_INT) as world FROM t1 inner join t2 on t1.Field_INT=t2.Field_INT where Field_INT >= %d;\"\n", intMax/2);
    fprintf(fp,"Q \"SELECT sum(Field_INT) as world FROM t1 inner join t2 on t1.Field_INT=t2.Field_INT where Field_INT >= %d;\"\n", intMax/2);
    fprintf(fp,"Q \"SELECT count(Field_INT), Field_STRING as MyAliasTest FROM t1 inner join t2 on t1.Field_INT=t2.Field_INT where Field_INT < %d group by Field_STRING;\"\n", intMax/2);
    fprintf(fp,"Q \"SELECT count(Field_INT), Field_STRING as MyAliasTest FROM t1 inner join t2 on t1.Field_INT=t2.Field_INT where Field_INT = %d group by Field_STRING;\"\n", intMax/2);
    fprintf(fp,"Q \"SELECT count(Field_INT), Field_STRING as MyAliasTest FROM t1 inner join t2 on t1.Field_INT=t2.Field_INT where Field_INT >= %d;\"\n", intMax/2);
    fprintf(fp,"Q \"SELECT min(Field_INT), Field_STRING as MyAliasTest FROM t1 inner join t2 on t1.Field_INT=t2.Field_INT where Field_INT < %d;\"\n", intMax/2);
    fprintf(fp,"Q \"SELECT Field_INT, Field_STRING FROM t1 inner join t2 on t1.Field_INT=t2.Field_INT where Field_INT >= %d;\"\n", intMax/2);  
  }
}
      
