#include "myLock.h"

int LK::getMode(myLock* l){
#ifndef myLocks
  fprintf(stderr,"Not included functionality!\n");
  exit(0);
#else
  return l->mode;
#endif
}
void LK:: initExst(myLock* l){
#ifndef myLocks
  if(pthread_rwlock_init(&l->l, NULL)){
    fprintf(stderr,"Fatal error initializing lock\nExiting...\n");
    exit(0);
  }
#else
  l->mode = unlocked;
#endif
}
myLock LK:: initNew(){
  myLock l;
  initExst(&l);
  return l;
}

int LK::lock_rd(myLock* l, const int prio){
#ifndef myLocks
  return pthread_rwlock_rdlock(&l->l);
#else
  volatile long setVal, expec;
  expec = l->mode;
  setVal = expec + 1;
  volatile int deadlocked = 0;
  while(expec == -1){
    deadlocked+=prio;
    if(deadlocked < 0){
      return RETRY;
    }
    expec = l->mode;
    setVal = expec + 1;
  }
  while(!__atomic_compare_exchange(&l->mode,
				   &expec,
				   &setVal,
				   1, __ATOMIC_RELAXED, __ATOMIC_RELAXED)){
    deadlocked+=prio;
    if(deadlocked < 0){
      return RETRY;
    }
    expec = l->mode;
    setVal = expec + 1;
    while(expec == -1){
      deadlocked+=prio;
      if(deadlocked < 0){
	return RETRY;
      }
      expec = l->mode;
      setVal = expec + 1;
    }
  }
  return SUCCESS;
#endif
}

//prio is inverted (i.e higher number means higher chance to kill self)
//prio of 0 means this will try forever...
int LK::lock_wr(myLock* l, const int prio){
#ifndef myLocks
  return pthread_rwlock_wrlock(&l->l);
#else
  volatile long setVal = wrlocked, expec = unlocked;
  volatile int deadlocked = 0;
  while(!__atomic_compare_exchange(&l->mode,
				   &expec,
				   &setVal,
				   1, __ATOMIC_RELAXED, __ATOMIC_RELAXED)){
    deadlocked+=prio;
    if(deadlocked < 0){
      return RETRY;
    }
    //this is goddam annoying...
    expec = unlocked;
  }
  return SUCCESS;
#endif
}



int LK::swap_mode(myLock* l, const int prio){
#ifndef myLocks
  fprintf(stderr,"This is not expected functionality!\n");
  exit(0);
#else
  volatile long setVal, expec;
  volatile int deadlocked = 0;
  if(l->mode > unlocked){
    setVal = wrlocked;
    expec = 1;
    while(!__atomic_compare_exchange(&l->mode,
				     &expec,
				     &setVal,
				     1, __ATOMIC_RELAXED, __ATOMIC_RELAXED)){
      deadlocked+=prio;
      if(deadlocked < 0){
	return RETRY;
      }
      //1 refers to a single readlock
      expec = 1;
    }
  }
  else if(l->mode == wrlocked){
    //getting first rdlock
    setVal = 1;
    expec = wrlocked;
    assert(__atomic_compare_exchange(&l->mode,&expec,&setVal,1, __ATOMIC_RELAXED, __ATOMIC_RELAXED));
  }
  else{
    fprintf(stderr,"Fatal error swapping: %ld\nExiting...\n", l->mode);
    exit(0);
  }
#endif
  return SUCCESS;
}
int LK::trylock_wr(myLock* l){
#ifndef myLocks
  return pthread_rwlock_trywrlock(&l->l);
#else
  volatile long setVal = wrlocked, expec = unlocked;
  return !__atomic_compare_exchange(&l->mode,
				    &expec,
				    &setVal,
				    1, __ATOMIC_RELAXED, __ATOMIC_RELAXED);
#endif
}
void LK::unlock(myLock* l){
#ifndef myLocks
  if(pthread_rwlock_unlock(&l->l)){
    fprintf(stderr,"Fatal error unlocking\nExiting...\n");
    exit(0);
  }
#else
  volatile long expec, setVal = unlocked;
  if(l->mode > unlocked){
    assert(__atomic_sub_fetch(&l->mode, 1, __ATOMIC_RELAXED)>=0);
  }
  else if(l->mode == wrlocked){
    expec=wrlocked;
    assert(__atomic_compare_exchange(&l->mode, &expec, &setVal,1, __ATOMIC_RELAXED, __ATOMIC_RELAXED));
  }
  else{
    fprintf(stderr,"Fatal error unlocking: %ld\nExiting...\n", l->mode);
    exit(0);
  }
#endif
}
