#pragma once
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "myLock.h"


#define locked 1
#define unlocked 0

#define wrMode 1
#define rdMode 0
#define maxList 32
struct heapFile;
struct thrHeapFile;
struct accessItem;
typedef unsigned char page;
typedef struct item {
  page* p;
  item* next;
  item* prev;
  //atomic incr when using/atomic dec when done using
  volatile unsigned long refCount;
  accessItem* ait;
  int dirty;
  int pageNum;
}item;

typedef struct pageCache{
  item* head;
  item* tail;
  volatile unsigned long numItems; //it is important to keep atomics 8byte aligned in struct
  unsigned long maxItems;
  myLock cacheLock;
  myLock* flushLock;
}pageCache;

typedef struct accessItem{
  item * it;
  myLock pgLock;
}accessItem;
  
  
typedef struct table{
  heapFile* tableDef;
  accessItem** aItems;
  pageCache* tcache;
  myLock flushLock;
}table;
  
typedef struct registrar{
  table** tables;
  int numTables;
  int size;
  int cacheSize;
  myLock addLock;
}registrar;

namespace BP {

  pageCache* initPC(int size, myLock* flushLock);
  void addItem(table* t, item* it, int alreadyLocked);
  void removeLRU(table* t, int alreadyLocked);
  void updateLRU(table* t, item* it, int alreadyLocked);


  item* newIT(accessItem* ait, page* page, int pageNum);
  //will free/NULL
  void freeIT(item* it);

  registrar* initReg(int size, int cacheSize);

  unsigned long getCords(unsigned int pageNum);
  item* getPage(table* t, thrHeapFile* lhf, int mode, unsigned int pageNum);
  void dropItem(pageCache* head, thrHeapFile* lhf, int wb);
  table* getTable(registrar* treg, char* name);
  void addNewTable(registrar* treg, heapFile* hf, int isNew);
  
  table* initTable(heapFile* hf, int csize);
  accessItem* newAccessArr(int size);
  
}
