#pragma once
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

//to turn of pthread_rwlock_t and my locks (this is mostly for debuggin)
#define myLocks

#define SUCCESS 0
//this means needs to restart transaction
#define RETRY 1 

//#define locked states (rdlocked is > 0)
#define wrlocked -1
#define unlocked 0
typedef struct myLock{
#ifndef myLocks
  pthread_rwlock_t l;
#else
  volatile long mode;
#endif
}myLock;

namespace LK{
  int getMode(myLock* l);
  int swap_mode(myLock* l,const int prio);
  myLock initNew();
  void initExst(myLock* l);
  int lock_rd(myLock* l, const int prio);
  int lock_wr(myLock* l, const int prio);
  void unlock(myLock* l);
  int trylock_wr(myLock* l);
}
