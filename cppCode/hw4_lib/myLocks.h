#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>



typedef struct myLock{
  pthread_rwlock l;
}myLock;

namespace LK{
  int lock_rd(myLock* l);
  int lock_wr(myLock* l);
  i unlock(myLock* l);
}
