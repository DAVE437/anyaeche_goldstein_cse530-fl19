#include "bufferPool.h"
#include "../hw1_lib/heapFile.h"


pageCache* BP::initPC(int size, myLock* flushLock){
  if(size < 2){
    fprintf(stderr,"Invalid size. Must be >= 2\n");
    return NULL;
  }
  pageCache* newCache = (pageCache*)calloc(1, sizeof(pageCache));
  assert(newCache);

  newCache->maxItems=size;
  LK::initExst(&newCache->cacheLock);
  newCache->flushLock=flushLock;
  return newCache;
}

//assumed it is calloced ahead of time (logic for that in registrar)
void BP::addItem(table* t, item* it, int alreadyLocked){
  pageCache* head = t->tcache;
  if(!alreadyLocked){
    LK::lock_wr(&head->cacheLock, 0);
  }
  if(verbose > 3){
    fprintf(stderr,"BEFORE:: Add\n");
    item* temp=head->head;
    fprintf(stderr,"[%lu]->", head->numItems);
    while(temp){
      fprintf(stderr,"%p[%lu][%p:%p]->", temp, temp->refCount, temp->prev, temp->next);
      temp=temp->next;
    }
    fprintf(stderr,"%p", head->tail);
    fprintf(stderr,"\n");
  }
    if(verbose > 2){
  fprintf(stderr,"add[%p]: %lu\n",it, it->refCount);
  }

  __atomic_add_fetch(&(it)->refCount, 1, __ATOMIC_RELAXED);
      if(verbose > 2){
  fprintf(stderr,"addsecond[%p]: %lu\n",it, it->refCount);
  }
  if(!head->head){
    head->head=it;
    head->tail=it;
    head->numItems++;
  }
  else{
    it->next=head->head;
    head->head->prev=it;
    head->head=it;
    head->numItems++;

    if(head->numItems>head->maxItems){
      assert(head->tail!=it); //this would be fatal error
      if(verbose > 2){
      fprintf(stderr,"remove LRU!\n");
      }
      removeLRU(t, locked);
    }
  }

    if(verbose > 3){
    fprintf(stderr,"AFTER:: add\n");
    item* temp=head->head;
    fprintf(stderr,"[%lu]->", head->numItems);
    while(temp){
      fprintf(stderr,"%p[%lu][%p:%p]->", temp, temp->refCount, temp->prev, temp->next);
      temp=temp->next;
    }
    fprintf(stderr,"%p", head->tail);
    fprintf(stderr,"\n");
  }
    if(!alreadyLocked){
      LK::unlock(&head->cacheLock);
    }
}

void BP::removeLRU(table* t, int alreadyLocked){
  pageCache* head = t->tcache;
  if(!alreadyLocked){
    LK::lock_wr(&head->cacheLock, 0);
  }
  else{
    if(verbose > 2){
    fprintf(stderr,"Is already locked!\n");
    }
  }

    if(verbose > 3){
    fprintf(stderr,"BEFORE:: rm\n");
    item* temp=head->head;
    fprintf(stderr,"[%lu]->", head->numItems);
    while(temp){
      fprintf(stderr,"%p[%lu][%p:%p]->", temp, temp->refCount, temp->prev, temp->next);
      temp=temp->next;
    }
    fprintf(stderr,"%p", head->tail);
    fprintf(stderr,"\n");
  }
  item* temp = head->tail;
  head->tail=(head->tail)->prev;
  (head->tail)->next = NULL;
  if(verbose>2){
    printf("Decr Before: %lu\n", temp->refCount);
  }
  int ret = __atomic_sub_fetch(&(temp)->refCount, 1, __ATOMIC_RELAXED);
    if(verbose>2){
    printf("Decr After: %lu\n", temp->refCount);
  }
  
  head->numItems--;
  if(verbose > 2){
  fprintf(stderr,"Remove[%p]: %lu==%d\n",temp, temp->refCount, ret);
  }
  //high probability no else is using now
  if(!ret){
    if(verbose > 2){
    fprintf(stderr,"Freeing the last page!\n");
    }
    //grab lock to avoid race condition
    //this handles case where after atomic sub another thread
    //quick runs transaction and thus will also get 0 case (i.e need to both
    //get 0 ref count AND acquire write lock)
    if(!(LK::trylock_wr(&temp->ait->pgLock))){
      if(verbose > 2){
      fprintf(stderr,"Won the lock!\n");
      }
      //if win lock can now safely null temp/flush
      if((temp)->dirty){
	//this is non negitiable
	LK::lock_wr(head->flushLock, 0);
	HF::writePage(t->tableDef, t->tableDef->fp, temp->p, temp->pageNum);
	//do flush here (need to think about how to do flush lock)
	LK::unlock(head->flushLock);
      }
      accessItem* tait = temp->ait;
      BP::freeIT(temp); //alot of thinking has gone into this...
      LK::unlock(&tait->pgLock);
    }
  }
    if(verbose > 3){
    fprintf(stderr,"After:: remove\n");
    item* temp=head->head;
    fprintf(stderr,"[%lu]->", head->numItems);
    while(temp){
      if(verbose > 2){
      fprintf(stderr,"%p[%lu][%p:%p]->", temp, temp->refCount, temp->prev, temp->next);
      }
      temp=temp->next;
    }
    fprintf(stderr,"%p", head->tail);
    fprintf(stderr,"\n");
  }

  if(!alreadyLocked){
    LK::unlock(&head->cacheLock);
  }
}

void BP::updateLRU(table* t, item* it, int alreadyLocked){

  pageCache* head=t->tcache;
  if(!alreadyLocked){
    LK::lock_wr(&head->cacheLock, 0);
  }
  //this means only reference is the it we have
  if(it->refCount == 1){
    addItem(t, it, locked);
    if(!alreadyLocked){
      LK::unlock(&head->cacheLock);
    }
    return;
  }
  if(verbose > 3){
    fprintf(stderr,"BEFORE:: UPDATE[%p]\n", it);
    item* temp=head->head;
    fprintf(stderr,"[%lu]->", head->numItems);
    while(temp){
      fprintf(stderr,"%p[%lu][%p:%p]->", temp, temp->refCount, temp->prev, temp->next);
      temp=temp->next;
    }
    fprintf(stderr,"%p", head->tail);
    fprintf(stderr,"\n");
  }
  //done...
  if(it==head->head){
    if(!alreadyLocked){
      LK::unlock(&head->cacheLock);
    }
    return;
  }
  //special case of tail
  else if(it==head->tail){
    head->tail=it->prev;
    head->tail->next=NULL;
  }
  //set surrounding pointers
  else{
    if(verbose > 3){
      fprintf(stderr,"Middle:: UPDATE[%p:%lu]\n", it, it->refCount);
    item* temp=head->head;
    fprintf(stderr,"[%lu]->", head->numItems);
    while(temp){
      fprintf(stderr,"%p[%lu][%p:%p]->", temp, temp->refCount, temp->prev, temp->next);
      temp=temp->next;
    }
    fprintf(stderr,"%p", head->tail);
    fprintf(stderr,"\n");
    }
    
    it->next->prev=it->prev;
    it->prev->next=it->next;
  }
  //set it as new head
  it->next=head->head;
  head->head->prev=it;
  head->head=it;

    if(verbose > 3){
    fprintf(stderr,"After update\n");
    item* temp=head->head;
    fprintf(stderr,"[%lu]->", head->numItems);
    while(temp){
      fprintf(stderr,"%p[%lu][%p:%p]->", temp, temp->refCount, temp->prev, temp->next);
      temp=temp->next;
    }
    fprintf(stderr,"%p", head->tail);
    fprintf(stderr,"\n");
    }
    if(!alreadyLocked){
      LK::unlock(&head->cacheLock);
    }
}


registrar* BP::initReg(int size, int cacheSize){
  registrar* newReg = (registrar*)calloc(1, sizeof(registrar));
  assert(newReg);
  newReg->size=size;
  newReg->cacheSize=cacheSize;
  newReg->tables = (table**)calloc(size, sizeof(table*));
  newReg->addLock = LK::initNew();
  assert(newReg->tables);
  return newReg;
}


table* BP::getTable(registrar* treg, char* name){
  for(int i=0;i<treg->numTables;i++){
    if(!strcmp(treg->tables[i]->tableDef->fName, name)){
      return treg->tables[i];
    }
  }
  return NULL;
}

accessItem* BP::newAccessArr(int size){
  accessItem* newAIT=(accessItem*)calloc(size, sizeof(accessItem));
  assert(newAIT);
  for(int i=0;i<size;i++){
    LK::initExst(&newAIT[i].pgLock);
  }
  return newAIT;

}
table* BP::initTable(heapFile* hf, int csize){
  table* newTable = (table*)calloc(1, sizeof(table));
  assert(newTable);
  newTable->tableDef = hf;
  newTable->aItems=(accessItem**)calloc(maxList, sizeof(accessItem*));
  assert(newTable->aItems);
  newTable->aItems[0]=BP::newAccessArr(2);
  newTable->flushLock=LK::initNew();
  newTable->tcache=BP::initPC(csize, &newTable->flushLock);
  return newTable;
}


void BP::addNewTable(registrar* treg, heapFile* hf, int isNew){
  LK::lock_wr(&treg->addLock, 0);
  table* ret=getTable(treg, hf->fName);
  if(!ret){
    ret=BP::initTable(hf, treg->cacheSize);
    treg->tables[treg->numTables]=ret;
    treg->numTables++;
  }else{
    if(isNew){
      ret->tableDef=hf;
      }
  }
  LK::unlock(&treg->addLock);
}

//basically find leading one with __atomic_clz
//and that will be used to store which index in 2d array
//access item for pagenum is. Then use lower bits to get
//slot in the approperiate 1d array. This is basically
//a way of doing O(1) array access without having to move items
//on resize (which is annoying to do in parallel programming).
//top 32 bits of return is the 2d array slot, bottum 32 1d array slot;
unsigned long BP::getCords(unsigned int pageNum){
  unsigned long ret;
  int ffo = 32 - ( __builtin_clz(pageNum)+1);
  ret=ffo;
  ret=ret<<32;
  ret|=(((1<<ffo)-1)&pageNum)+(pageNum == 1);
  return ret;
}

item* BP::newIT(accessItem* ait, page* page, int pageNum){
  item* it = (item*)calloc(1, sizeof(item));
  it->p=page;
  assert(it);
  it->ait=ait;
  it->pageNum=pageNum;
  it->refCount = 0;
  
  return it;
}
void BP::freeIT(item* it){
  if(verbose > 2){
  fprintf(stderr,"Freeing IT!\n");
  }
  it->ait->it=NULL;
  free(it->p);
  free(it);

}


item* BP::getPage(table* t, thrHeapFile* lhf, int mode, unsigned int pageNum){
  heapFile* hf = t->tableDef;
  unsigned long indexes=getCords(pageNum);
  if(verbose > 2){
  fprintf(stderr,"cords: %d->%lx\n", pageNum, indexes);
  }
  if(!t->aItems[indexes>>32]){
    accessItem* expec=NULL;
    accessItem* newAIT=newAccessArr((1<<((indexes>>32)))<<1);
    if(verbose > 2){
    fprintf(stderr,"New AIT NEEDED!\n");
    }
    if(!__atomic_compare_exchange(&t->aItems[indexes>>32], &expec, &newAIT, 1, __ATOMIC_RELAXED,__ATOMIC_RELAXED)){
      free(newAIT);
    }
  }
  accessItem* ait = &t->aItems[indexes>>32][(int)indexes];
  if(mode==wrMode){
    if(verbose > 2){
      fprintf(stderr,"Getting write page lock for [%lu][%d]\n", indexes>>32, (int)indexes);
    }
    LK::lock_wr(&ait->pgLock, 1);
  }
  else if(mode == rdMode){
    if(verbose > 2){
      fprintf(stderr,"Getting read page lock for [%lu][%d]\n", indexes>>32, (int)indexes);
    }
    LK::lock_rd(&ait->pgLock, 1);
  }
  else{
    fprintf(stderr,"Invalid mode given\n");
    exit(0);
  }
  if(ait->it){
    if(verbose>2){
      fprintf(stderr,"update fetch: before %lu!\n", ait->it->refCount);
    }
    __atomic_add_fetch(&ait->it->refCount, 1, __ATOMIC_RELAXED);
        if(verbose>2){
      fprintf(stderr,"update fetch: After %lu!\n", ait->it->refCount);
    }
    updateLRU(t, ait->it, unlocked);
  }
  else{
    //maybe acquire as write initially to avoid this
    if(verbose>2){
      fprintf(stderr,"reading: %d\n", (int)pageNum);
    }
    item* it=newIT(ait,HF::readPage(lhf, (int)pageNum), pageNum);
    item* expec=NULL;
    //basically only want one transaction to add it to cache (and dont want others to have
    //to wait so all will try, one will succeeed, this only matter for rd mode).
    if(!__atomic_compare_exchange(&ait->it, &expec, &it, 1, __ATOMIC_RELAXED,__ATOMIC_RELAXED)){
      freeIT(it);
      assert(mode!=wrMode);
      fprintf(stderr,"Here!---------------\n");
      __atomic_add_fetch(&ait->it->refCount, 1, __ATOMIC_RELAXED);
    }
    else{
      if(verbose > 2){
	printf("add after: %lu\n", ait->it->refCount);
      }
      __atomic_add_fetch(&ait->it->refCount, 1, __ATOMIC_RELAXED);
        if(verbose>2){
    printf("add after: %lu\n", ait->it->refCount);
  }
      addItem(t, ait->it, unlocked);
    }
   

  }


  return ait->it;
}

void BP::dropItem(pageCache* head, thrHeapFile* lhf, int wb){
  item* it = lhf->curItem;
  if(wb){
    LK::lock_wr(head->flushLock, 0);
    HF::writePage(lhf->myTable->tableDef, lhf->fp, lhf->curItem->p, lhf->curPageNum);
    LK::unlock(head->flushLock);
  }
  accessItem* tait = it->ait;
  if(verbose>2){
    printf("dropbefore: %lu\n", tait->it->refCount);
  }
  if(LK::getMode(&it->ait->pgLock) > unlocked){
    LK::swap_mode(&it->ait->pgLock,0);
  }
  int res = __atomic_sub_fetch(&it->ait->it->refCount,1, __ATOMIC_RELAXED);
  if(verbose > 2){
    fprintf(stderr,"Drop[%p]:  %lu == %d\n",it, tait->it->refCount, res);
    }
							 
  if(!res){

    if(verbose>2){
      fprintf(stderr,"trying writeback!\n");
    }
      if((it)->dirty){
	if(verbose>2){
	  fprintf(stderr,"doing writeback!\n");
	}
	//this is non negitiable
	LK::lock_wr(head->flushLock, 0);
	HF::writePage(lhf->myTable->tableDef, lhf->fp, lhf->curItem->p, lhf->curPageNum);
	LK::unlock(head->flushLock);
	it->dirty=0;
      }
 
      BP::freeIT(it); //alot of thinking has gone into this...
  }
  LK::unlock(&tait->pgLock);
}
