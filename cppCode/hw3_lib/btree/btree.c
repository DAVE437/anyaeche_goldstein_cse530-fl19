#include "btree.h"


#define min(X, Y)  ((X) < (Y) ? (X) : (Y))
#define max(X, Y)  ((X) > (Y) ? (X) : (Y))

typedef struct node {
  void * ptrs[N+1];
  field keys[N];
  char numKeys;
  struct node * parent;
}node;


typedef struct ret{
  node* n;
  int loc;
  int isIn;
}ret;


void* tptrs[N+1];
field tkeys[N];
const int splitLineOuter =(((N-1)>>1)+((N-1)&0x1));
const int splitLineInner =((N>>1)+(N&0x1));
#define highBits 16
#define ptrBits 48
#define lowBitsMask 0x7

const unsigned long highBitMask=((1UL)<<48)-1;
const unsigned long lowBitPtrMask=~lowBitsMask;



//actual btree functions
ret searchInternal(node * root, field key);
int getMyIndex(node * parent, node * l);
node * insertOuter(ret get, field key, page * pointer);
node * splitOuterIns(node * root, field key, page * pointer, ret get);
node * insertNodeInner(node * root, node * parent, int lIndex, field key, node * r);
node * splitNodeInnerIns(node * root, node * parent, int lIndex, field key, node * r);
node * insertParent(node * root, node * l, field key, node * r);
node * insertRoot(field key,node * l, node * r);
void combineNodes(node * root, node * n, node * pPtr, int pkey);
node * spreadNodeVals(node * root, node * n, node * pPtr,const int prev, const int pkey);
node * deleteRecursive(node * root, node * n, field key, void * pointer);


//possible future todos, use memory blocking and use top char to store a nodes index in its
//parent so it doesnt need to search. Reorganize code so that all calls to change num keys
//are through parent (basically i should have approached this differently!).
unsigned char highBitsGet(node* n){
  return getPtr(n)->numKeys;
  //    return (unsigned char)(((unsigned long)n)>>ptrBits);
}

//sets highBits of a ptr
void highBitsSet(node** n, unsigned char bits){
  /*   unsigned long newPtr=bits;
       newPtr=newPtr<<ptrBits;
       newPtr|=(unsigned long)(highBitsGetPtr(*n));
       *n=(node*)newPtr;*/
  getPtr(*n)->numKeys=bits;
}

void highBitsSetAdd(node**n, unsigned long bits){
  /*unsigned long newPtr=(unsigned long)(*n);
    newPtr+=(bits<<ptrBits);
    *n=(node*)newPtr;*/
  getPtr(*n)->numKeys+=bits;
}

//increments highBits, this is unsafe
void highBitsIncr(node** n){
  /*   unsigned long newPtr=(unsigned long)(*n);
       newPtr+=(1UL<<ptrBits);
       *n=(node*)newPtr;*/
  getPtr(*n)->numKeys++;
}

//decrements highBits, this is unsafe
void highBitsDecr(node** n){
  /*   unsigned long newPtr=(unsigned long)(*n);
       newPtr-=(1UL<<ptrBits);
       *n=(node*)newPtr;*/
  getPtr(*n)->numKeys--;
}

//drops high bits info returns ptr
node* highBitsGetPtr(node* n){
  return (node*)((((unsigned long)n)&highBitMask));
}


node* lowBitsGetPtr(node* n){
  return (node*)(((unsigned long)(n))&lowBitPtrMask);
}


int lowBitsGet(node* n){
  return ((unsigned long)n)&lowBitsMask;
}




//sets low bits of n to b, this is unsafe
void lowBitsSet(node** n, int b){
  *n=(node*)(((unsigned long)(lowBitsGetPtr(*n)))|b);
}

//gets valid ptr, use if both low and high bits in use
node* getPtr(node* n){
  return (node*)(((((unsigned long)n)&highBitMask)&lowBitPtrMask));
}


int binarySearch(field* arr, int n, int val){
  int l = 0xffffffff;
  int m;
  while (l+1 < n) {
    m = ((n+l)>>1);
    if (arr[m] < val)
      l = m;
    else 
      n = m;
  }

  return n;
}

ret searchInternal(node * root, field key) {
  ret r;
  r.isIn=0;
  int slot;
  node * searcher = root;
  while (!lowBitsGet(searcher)) {
    slot=binarySearch(getPtr(searcher)->keys, highBitsGet(searcher), key);
    slot+=(getPtr(searcher)->keys[slot]==key&&(slot!=highBitsGet(searcher)));
    searcher = (node *)getPtr(searcher)->ptrs[slot];
  }
  slot=binarySearch(getPtr(searcher)->keys, highBitsGet(searcher), key);
  r.n=searcher;
  r.loc=slot;
  r.isIn=getPtr(searcher)->keys[slot]==key;
  return r;
}


int getMyIndex(node * parent, node * me) {
  const int uBound=highBitsGet(parent);
  for(int i=0;i<=uBound;i++){
    if(getPtr(getPtr(parent)->ptrs[i])==getPtr(me)){
      return i;
    }
  }
  return uBound;
}

node * insertOuter(ret get,field key, page * pointer) {
  const int uBound=highBitsGet(get.n);
  for (int i = uBound; i > get.loc; i--) {
    getPtr(get.n)->keys[i] = getPtr(get.n)->keys[i - 1];
    getPtr(get.n)->ptrs[i] = getPtr(get.n)->ptrs[i - 1];
  }
  getPtr(get.n)->keys[get.loc] = key;
  getPtr(get.n)->ptrs[get.loc] = pointer;
  highBitsIncr(&get.n);
  return get.n;
}

node * splitOuterIns(node * root, field key, page * pointer, ret get) {
  node * nOuter;
  int nkey, i;
  const int uBound=highBitsGet(get.n);
  nOuter = calloc(1,sizeof(node));
  lowBitsSet(&nOuter, 1);
  for(i=0;i<uBound;i++){
    tkeys[i+(i>=get.loc)]=getPtr(get.n)->keys[i];
    tptrs[i+(i>=get.loc)]=getPtr(get.n)->ptrs[i];
  }
  tkeys[get.loc]=key;
  tptrs[get.loc]=pointer;
  for(i=0;i<splitLineOuter;i++){
    getPtr(get.n)->keys[i]=tkeys[i];
    getPtr(get.n)->ptrs[i]=tptrs[i];
  }
  for(i=splitLineOuter;i<N;i++){
    getPtr(nOuter)->ptrs[i-splitLineOuter] = tptrs[i];
    getPtr(nOuter)->keys[i-splitLineOuter] = tkeys[i];
  }
  highBitsSet(&get.n, splitLineOuter);
  highBitsSet(&nOuter, N-splitLineOuter);
  getPtr(nOuter)->ptrs[N - 1] = getPtr(get.n)->ptrs[N - 1];
  getPtr(get.n)->ptrs[N - 1] = nOuter;
  for (i = splitLineOuter; i < N - 1; i++){
    getPtr(get.n)->ptrs[i] = NULL;
  }
  for (i =N-splitLineOuter; i < N - 1; i++){
    getPtr(nOuter)->ptrs[i] = NULL;
  }
  getPtr(nOuter)->parent = getPtr(get.n)->parent;
  nkey = getPtr(nOuter)->keys[0];
  return insertParent(root, get.n, nkey, nOuter);
}


node * insertNodeInner(node * root, node * n, int lIndex, field key, node * r) {
  const int uBound=highBitsGet(n);
  for (int i = uBound; i > lIndex; i--) {
    getPtr(n)->ptrs[i + 1] = getPtr(n)->ptrs[i];
    getPtr(n)->keys[i] = getPtr(n)->keys[i - 1];
  }
  getPtr(n)->ptrs[lIndex + 1] = r;
  getPtr(n)->keys[lIndex] = key;
  highBitsIncr(&n);
  return root;
}

node * splitNodeInnerIns(node * root, node * pnode, int lIndex, field key, node * r) {
  int pkey;
  node * nnode, * child;
  const int uBound=highBitsGet(pnode);
  for (int i = 0; i < uBound; i++) {
    tptrs[i+(i>=lIndex+1)] = getPtr(pnode)->ptrs[i];
    tkeys[i+(i>=lIndex)] = getPtr(pnode)->keys[i];
  }
  tptrs[uBound+(uBound>=lIndex+1)] = getPtr(pnode)->ptrs[uBound];
  tptrs[lIndex + 1] = r;
  tkeys[lIndex] = key;

  nnode = calloc(1, sizeof(node));
  highBitsSet(&pnode, splitLineInner-1);
  for (int i = N-splitLineInner; i >=0; i--) {
    getPtr(pnode)->keys[i]=tkeys[i];
  }
  
  getPtr(pnode)->ptrs[splitLineInner-1] = tptrs[splitLineInner-1];
  pkey = tkeys[splitLineInner - 1];
  

  for (int i=splitLineInner;i < N; i++) {
    getPtr(nnode)->ptrs[i-splitLineInner] = tptrs[i];
    getPtr(nnode)->keys[i-splitLineInner] = tkeys[i];
  }
  highBitsSet(&nnode, N-splitLineInner);
  getPtr(nnode)->ptrs[N-splitLineInner] = tptrs[N];
  getPtr(nnode)->parent = getPtr(pnode)->parent;
  for (int i =0; i <=(N-splitLineInner) ; i++) {
    child = getPtr(nnode)->ptrs[i];
    getPtr(child)->parent = nnode;
  }


  return insertParent(root, pnode, pkey, nnode);
}

node * insertParent(node * root, node * l, field key, node * r) {
  node * parent;
  parent = getPtr(l)->parent;
  if (!parent){
    return insertRoot(key,l,r);
  }
  int lIndex = getMyIndex(parent, l);
  if(highBitsGet(parent)<N-1){
    return insertNodeInner(root, parent, lIndex, key, r);
  }
  return splitNodeInnerIns(root, parent, lIndex, key, r);
}


node * insertRoot(field key, node * l, node * r) {
  node * root = calloc(1, sizeof(node));
  getPtr(root)->keys[0] = key;
  getPtr(root)->ptrs[0] = l;
  getPtr(root)->ptrs[1] = r;
  highBitsSet(&root, 1);
  getPtr(l)->parent = root;
  getPtr(r)->parent = root;
  return root;
}

node * init() {
  node * root = calloc(1, sizeof(node));
  lowBitsSet(&root, 1);
  return root;
}

node * insert(node * root, field key, page* p) {
  ret get = searchInternal(root, key);
  if (get.isIn) {
    getPtr(get.n)->ptrs[get.loc]=p;
    return root;
  }
  if(highBitsGet(get.n)<N-1){
    get.n = insertOuter(get, key, p);
    return root;
  }
  return splitOuterIns(root, key, p, get);
}


node * rmEntryFirst(node * n, int slot) {
  const int uBound=highBitsGet(n);
  const int outerBool=lowBitsGet(n);
  
  for (; slot < uBound; slot++){
    getPtr(n)->keys[slot - 1] = getPtr(n)->keys[slot];
    getPtr(n)->ptrs[slot - 1] = getPtr(n)->ptrs[slot];
  }
  highBitsDecr(&n);
  if (outerBool){
    slot--;
    for (; slot < N - 1; slot++){
      getPtr(n)->ptrs[slot] = NULL;
    }
    return n;
  }
  //never hit case where is out of bounds
  getPtr(n)->ptrs[uBound] = getPtr(n)->ptrs[slot];
  for (; slot < N; slot++){
    getPtr(n)->ptrs[slot] = NULL;
  }
  return n;
}


node * rmEntry(node * n, field key) {
  int slot=binarySearch(getPtr(n)->keys, highBitsGet(n), key)+1;
  const int outerBool=lowBitsGet(n);
  const int uBound=highBitsGet(n);
  for (; slot < uBound; slot++){
    getPtr(n)->keys[slot - 1] = getPtr(n)->keys[slot];
    getPtr(n)->ptrs[slot + (!outerBool) - 1] = getPtr(n)->ptrs[slot+!outerBool];
  }
  highBitsDecr(&n);
  if (outerBool){
    for (; slot < N - 1; slot++){
      getPtr(n)->ptrs[slot] = NULL;
    }
    return n;
  }
  for (slot = uBound; slot < N; slot++){
    getPtr(n)->ptrs[slot] = NULL;
  }
  return n;
}



void combineNodes(node * root, node * n, node * pPtr, int pkey) {
  const int outerBool=lowBitsGet(n);
  const int uBound=highBitsGet(n);
  const int pBound =highBitsGet(pPtr)+!outerBool;
  for (int i = 0; i < uBound; i++) {
    getPtr(pPtr)->keys[i+pBound] = getPtr(n)->keys[i];
    getPtr(pPtr)->ptrs[i+pBound] = getPtr(n)->ptrs[i];
  }
  getPtr(pPtr)->ptrs[N - 1] = getPtr(n)->ptrs[N - 1];
  if (!outerBool) {
    getPtr(pPtr)->keys[pBound-1] = pkey;
    highBitsSet(&n,0);
    getPtr(pPtr)->ptrs[uBound+pBound] = getPtr(n)->ptrs[uBound];
    for (int i = 0; i < pBound+uBound+1; i++) {
      getPtr(((node *)getPtr(pPtr)->ptrs[i]))->parent=pPtr;
    }
  }
}


node * spreadNodeVals(node * root, node * n, node * pPtr, const int prevIndex, const int pkey) {  
  const int outerBool=lowBitsGet(n);
  const int uBound=highBitsGet(n);
  const int pBound=highBitsGet(pPtr);
  if (prevIndex) {
    if (!outerBool) {
      getPtr(n)->ptrs[0] = getPtr(pPtr)->ptrs[pBound];
      getPtr(n)->keys[0] = pkey;
      getPtr(getPtr(n)->parent)->keys[prevIndex-1] = getPtr(pPtr)->keys[pBound - 1];

      getPtr((getPtr((node *)n)->ptrs[0]))->parent=n;
      getPtr(pPtr)->ptrs[pBound] = NULL;
    }
    else {
      getPtr(n)->ptrs[0] = getPtr(pPtr)->ptrs[pBound - 1];
      getPtr(n)->keys[0] = getPtr(pPtr)->keys[pBound - 1];
      getPtr(getPtr(n)->parent)->keys[(prevIndex-1)] = getPtr(n)->keys[0];

      getPtr(pPtr)->ptrs[pBound - 1] = NULL;
    }
  }
  else {  
    if (outerBool) {
      getPtr(n)->keys[uBound] = getPtr(pPtr)->keys[0];
      getPtr(n)->ptrs[uBound] = getPtr(pPtr)->ptrs[0];
      getPtr(getPtr(n)->parent)->keys[prevIndex] = getPtr(pPtr)->keys[1];
    }
    else {
      getPtr(n)->keys[uBound] = pkey;
      getPtr(n)->ptrs[uBound + 1] = getPtr(pPtr)->ptrs[0];
      getPtr(getPtr(n)->parent)->keys[prevIndex] = getPtr(pPtr)->keys[0];
      
      getPtr((getPtr((node *)n)->ptrs[uBound + 1]))->parent=n;
    }
    
  }
  highBitsIncr(&n);
  highBitsDecr(&pPtr);
  return root;
}

node* deleteInternal(node* root, node* n, field key, void* pointer){
  const int outerBool=lowBitsGet(n);
  const int uBound=highBitsGet(n);
  if (getPtr(n)==getPtr(root)){
    if(!outerBool&&!uBound){
      root = getPtr(root)->ptrs[0];
      getPtr(root)->parent = NULL;
      free(getPtr(n));
    }
    return root;
  }

  if (uBound >=((((N-outerBool)>>1)+((N-outerBool)&0x1))-!outerBool)){
    return root;
  }
  const int prevIndex= getMyIndex(getPtr(n)->parent, n);
  int pkey = getPtr(getPtr(n)->parent)->keys[prevIndex-(prevIndex!=0)];
  node* pPtr = getPtr(getPtr(n)->parent)->ptrs[prevIndex-(prevIndex!=0)+(prevIndex==0)];
  const int pBound=highBitsGet(pPtr);
  if(pBound+uBound<N-!outerBool){
    if(prevIndex){
      combineNodes(root, n, pPtr,  pkey);
      highBitsSetAdd(&pPtr, uBound+!outerBool);
      root = deleteRecursive(root, getPtr(n)->parent, pkey, n);
      free(getPtr(n));
      return root;
    }
    else{
      combineNodes(root,  pPtr,n,  pkey);
      highBitsSetAdd(&n, pBound+!outerBool);
      root = deleteRecursive(root, getPtr(pPtr)->parent, pkey, pPtr);
      free(getPtr(pPtr));
      return root;
    }
  }
  if(prevIndex){
    for (int i = uBound+(!outerBool); i > 0; i--) {
      getPtr(n)->keys[i] = getPtr(n)->keys[i - 1];
      getPtr(n)->ptrs[i] = getPtr(n)->ptrs[i - 1];
    }
  }
  
  root=spreadNodeVals(root, n, pPtr, prevIndex, pkey);
  if(!prevIndex){
    for (int i = 0; i < pBound - outerBool; i++) {
      getPtr(pPtr)->keys[i] = getPtr(pPtr)->keys[i + 1];
      getPtr(pPtr)->ptrs[i] = getPtr(pPtr)->ptrs[i + 1];
    }
  }
  return root;
}
node * deleteRecursive(node * root, node * n, field key, void * pointer) {
  n=rmEntry(n, key);
  return deleteInternal(root, n, key,pointer);

}

page* search(node* root, field key){
  ret get=searchInternal(root, key);
  return getPtr(get.n)->ptrs[get.loc];
}

node * delete(node * root, field key) {
  ret get= searchInternal(root, key);
  if (get.isIn && get.n) {
    page* toFree=getPtr(get.n)->ptrs[get.loc];
    get.n=rmEntryFirst(get.n, get.loc+1);
    root = deleteInternal(root, get.n, key,toFree);
    free(toFree);
  }
  return root;
}


int testTree(node* root, field* samples, int sampleLen){
  node* tracker;
  int depthPrev,depth, slot;
  for(int i=0;i<sampleLen;i++){
    depth=0;
    tracker=root;
    while(!lowBitsGet(tracker)){
      depth++;
      assert(highBitsGet(tracker)>=(N>>1));
      slot=binarySearch(getPtr(tracker)->keys, highBitsGet(tracker), samples[i]);
      slot+=(getPtr(tracker)->keys[slot]==samples[i]&&(slot!=highBitsGet(tracker)));
      tracker = (node *)getPtr(tracker)->ptrs[slot];
    }
    if(depth){
      assert(highBitsGet(tracker)>=(N>>1));
    }
    if(i){
      assert(depthPrev==depth);
    }
    depthPrev=depth;
  }
  return depth;
}


