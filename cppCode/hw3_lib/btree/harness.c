#include "btree.h"
#include "utils/arg.h"

#define fullSample 1024
#define smallSample 16
int nitems=20;
int corr=0;
static ArgOption args[] = {
  // Kind, 	  Method,		name,	    reqd,  variable,		help
  { KindHelp,     Help, 	"-h" },
  { KindOption,   Set,          "--extra",          0,     &corr,               "set to include additional tests on tree structure. Dont set this for excessively large item values" },
  { KindOption,   Integer,      "--items",          0,     &nitems,             "amount of items to test with (give power of 2)" },
  { KindEnd }
};

#define Version "0.1"
static ArgDefs argp = { args, "prepsort example", Version, NULL };



void fullTest(node* root, int ts){
  field* ins=calloc(ts<<1, sizeof(field));
  page** pages=calloc(ts<<1, sizeof(page*));
  pages[0]=calloc(1, 16);
  ins[0]=rand()%(RAND_MAX/ts);
  printf("Testing with %d Unique K,V\n\n", ts<<1);
  printf("Setup\n");
  
  field* samples=malloc(fullSample*sizeof(field));
  int comp=0;
  int marker=((ts<<2)+(ts<<1))/100;
  for(int i =1;i<ts<<1;i++){
    if(i>comp*marker){
      fprintf(stderr, "\r%d%% Complete", comp);
      comp++;
    }
    pages[i]=calloc(1, 16);
    ins[i]=(rand()%((RAND_MAX/ts)-1))+1+ins[i-1];
  }
  int temp, r1, r2, pow2, depth;
  for(int i=0;i<(ts<<2);i++){
    if((i+(ts<<1))>comp*marker){
      fprintf(stderr, "\r%d%% Complete", comp);
      comp++;
    }
    r1=rand()%ts;
    r2=rand()%ts;
    temp=ins[r1];
    ins[r1]=ins[r2];
    ins[r2]=temp;
  }
  printf("\r%d%% Complete\n\n", 100);
  printf("Testing Insert\n");
  page* get;
  comp=0;
  marker=ts/100;
  pow2=1;
  for(int i =0;i<ts;i++){
    if(i>(1<<pow2)){
      pow2++;
    }
    if(i>comp*marker){
      fprintf(stderr, "\r%d%% Complete", comp);
      comp++;
    }
    root=insert(root, ins[i],pages[i]);
    get=search(root, ins[i]);
    assert(get==pages[i]);
    if(corr){
    for(int i =0;i<smallSample;i++){
      samples[i]=ins[rand()%ts];
    }
    depth=testTree(root, samples, smallSample);
    assert(depth<(pow2+10)&&depth>(pow2-10));
    }	 

  }
  if(corr){
  for(int i =0;i<fullSample;i++){
    samples[i]=ins[rand()%ts];
  }
  depth=testTree(root, samples, fullSample);
  assert(depth<(pow2+10)&&depth>(pow2-10));
  }
  printf("\r%d%% Complete\n\n",100);


  printf("Testing Insert and Delete\n");
  comp=0;
  for(int i =ts;i<ts<<1;i++){
    if((i-ts)>comp*marker){
      fprintf(stderr, "\r%d%% Complete", comp);
      comp++;
    }
    root=insert(root, ins[i], pages[i]);
    get=search(root, ins[i]);
    assert(get==pages[i]);
    root=delete(root, ins[i]);
    get=search(root, ins[i]);

    if(corr){
    for(int i =0;i<smallSample;i++){
      samples[i]=ins[rand()%ts];
    }
    depth=testTree(root, samples, smallSample);
    assert(depth<(pow2+10)&&depth>(pow2-10));
    }
  }
  printf("\r%d%% Complete\n\n",100);
  printf("Testing Remaining Nodes Correctness\n");
  comp=0;
  for(int i=0;i<ts;i++){
    if(i>comp*marker){
      fprintf(stderr, "\r%d%% Complete", comp);
      comp++;
    }
    get=search(root, ins[i]);
    assert(get==pages[i]);
    if(corr){
    for(int i =0;i<smallSample;i++){
      samples[i]=ins[rand()%ts];
    }
    depth=testTree(root, samples, smallSample);
    assert(depth<(pow2+10)&&depth>(pow2-10));
    }
  }
  printf("\r%d%% Complete\n\n",100);
  printf("Testing Modify Insert\n");
  comp=0;
  for(int i =0;i<ts;i++){
    if(i>comp*marker){
      fprintf(stderr, "\r%d%% Complete", comp);
      comp++;
    }
    root=insert(root, ins[i], pages[(i+1)%ts]);
    get=search(root, ins[i]);
    assert(get==pages[(i+1)%ts]);
    if(corr){
    for(int i =0;i<smallSample;i++){
      samples[i]=ins[rand()%ts];
    }
    depth=testTree(root, samples, smallSample);
    assert(depth<(pow2+10)&&depth>(pow2-10));
    }
  }
  if(corr){
  for(int i =0;i<fullSample;i++){
    samples[i]=ins[rand()%ts];
  }
  depth=testTree(root, samples, fullSample);
  assert(depth<(pow2+10)&&depth>(pow2-10));
  }
  
  printf("\r%d%% Complete\n\n",100);
  printf("Testing Delete\n");
  comp=0;
  for(int i =0;i<ts;i++){
    if((ts-i)<(1<<pow2)){
      pow2--;
    }
    if(i>comp*marker){
      fprintf(stderr, "\r%d%% Complete", comp);
      comp++;
    }
    root=delete(root, ins[i]);
    get=search(root, ins[i]);
    if(corr){
    for(int i =0;i<smallSample;i++){
      samples[i]=ins[rand()%ts];
    }
    depth=testTree(root, samples, smallSample);
    assert(depth<(pow2+10)&&depth>(pow2-10));
  }
  }

  printf("\r%d%% Complete\n\n",100);
  assert(!testTree(root, samples, 1));
  free(samples);
  free(getPtr(root));
  free(pages);
  free(ins);
  printf("All Test Done!\n");
}


int main(int argc, char** argv){
  ArgParser* ap = createArgumentParser(&argp);
  if(parseArguments(ap, argc, argv)){
    printf("Error parsing arguments!\n");
    return -1;
  }
  nitems=1<<(nitems-1);
  node* root = init();
  srand(time(NULL));
  fullTest(root, nitems);
  return 0;
}
