#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <assert.h>

#define N 3


typedef int field;
typedef char page;
typedef struct node node;




node* getPtr(node* n);
node* init();
page* search(node * root, field key);
node * insert(node * root, field key, page* p);
node * delete(node * root, field key);
int testTree(node* root, field* samples, int sampleLen);
