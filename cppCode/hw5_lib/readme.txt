This is the lab 5 solution to CSE 530 submitted by Noah and David

The program has 2 primary features: First all memory is managed by the
exstore malloc. This is to enable efficient memory reusable of deleted
blocks.

The second is an array of hashtables which are how a key/value is
found.

The approach to finding a key/value pair is to hash the key/value and
search the subtables (starting from subtable 0 and going to subtable
n) for a slot whose key/value match. Since each object can have
multiple keys/values and a query to each key/value must return the
object, each key/value is given a slot.

the description of objects can be found in db_object.h/.cpp
the description of the file can found in db_file.h/.cpp

Notes:
db_object's have no defined size, they have an undefined sized array
for both The data for each object is stored as an unsigned
char. getKeyType() can be used to extract what type was intended by
the inserter. This is C/C++ so that type is at most a suggestion...


This implementation is NOT optimized. There is room for many
improvements including reducing the meta data necessary to store and
object, storing the location in the subtable of each key/value to make
delete faster, aggressive prefetching, better usage of mmap,
implementing the freeblock list as a splay tree, batching the slot
acquisition of multiple keys, on demand hashing, and much much more.


In the main directory there are 3 folders: src, lib, utils

lib: contains all the logic for lab 5
src: contains the driver/Makefile
utils: contains the args processor

For running the program:
from the main directory run:
cd src
make clean; make
./harness -h

#the "--extra" argument here will ensure that each object can be found
#from each key/value it contains as well as from 10 random
#permutations of its keys. Naturally this slows down the program a
#great deal so refrain from using higher ntests with the --extra
#argument.  keep in mind that this implementation is only meant to
#handle file sizes < 2^32.  The test program will output the total
#amount of bytes that will be stored in the db at a given
#time. Keeping this under 10^9 is advisable to avoid overflow. Also
#keep in mind the DRAM available on the test computer as all the data
#for generating the test objects is stored in DRAM. Note that within a
#given run, between each test case, the file is closed and reaccessed.

#here are some tests that you might want to run:
#keep in mind maxkey/maxval < 16, nkeys < 31 this program, while a
#good start is ultimately incomplete. We are sure there are bugs. We
#also believe this is an interesting concept and method for
#approaching key-value store type databases. This method is designed
#for easy parallization. all operations can be done WITHOUT locking
#and exclusively with CAS. The runtime for all operation is
#O(nkeys*n*log(n))

./harness --ntests 19 --reset --extra --minkey 2 --maxkey 2 --minval 1 --maxval 4 --nkeys 3 --db ../db --table t1
./harness --ntests 20 --reset --minkey 2 --maxkey 2 --minval 1 --maxval 4 --nkeys 3 --db ../db --table t1
./harness --ntests 21 --reset --minkey 2 --maxkey 2 --minval 1 --maxval 4 --nkeys 3 --db ../db --table t1
./harness --ntests 22 --reset --minkey 2 --maxkey 2 --minval 1 --maxval 4 --nkeys 3 --db ../db --table t1
./harness --ntests 20 --reset --minkey 1 --maxkey 8 --minval 1 --maxval 8 --nkeys 3 --db ../db --table t1
./harness --ntests 20 --reset --minkey 5 --maxkey 9 --minval 5 --maxval 9 --nkeys 5 --db ../db --table t1
./harness --ntests 15 --reset --extra --minkey 10 --maxkey 10 --minval 10 --maxval 10 --nkeys 3 --db ../db --table t1
./harness --ntests 13 --reset --extra --minkey 10 --maxkey 14 --minval 10 --maxval 15 --nkeys 10 --db ../db --table t1
