#include "db.h"

typedef unsigned int uint;


void DB::initDB(char* table_name){
  if(mkdir(table_name, 0777)){
    if(errno!=EEXIST){
      fprintf(stderr,"Error creating dir: %s\n", table_name);
      fprintf(stderr,"%s\n", strerror(errno));
      exit(0);
    }
  }
}
