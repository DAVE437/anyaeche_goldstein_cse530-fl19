#include "db_file.h"


//returning char from seed, this is just space efficient way
//to get variation in hashing
char DBF::getSeedN(db_file* dbf, int seed_num){
  uint mask = 0xff;
  mask = mask<<((seed_num<<3));
  return (char)((dbf->seeds&mask)>>(seed_num<<3));
}

//returns where slot is taken or not
uint DBF::isSlotFree(slot s){
  return s&1;
}

//gets the tag from a given slot
uint DBF::getSlotTag(slot s){
  return (s>>1)&TAG_MASK;
}

//gets the obj location from a slot
uint DBF::getObjLoc(slot s){
  return (s>>32)&INT_MASK;
}

//turns slotnum in a given table into a slot* for add/find/del to use
slot* DBF::getSlot(db_file* dbf, uint slot_num, uint tnum){
  slot* s;
  return (slot*)(dbf->region+DBF::getSlotLoc(dbf, tnum, slot_num));
}

//gets the byte location of a slot
uint DBF::getSlotLoc(db_file* dbf, uint tnum, uint slot_num){
  return dbf->subtable_starts[tnum] + slot_num*sizeof(slot);
}

//modes hashval for given table num (power of 2 table sizes)
uint DBF::getSlotIndex(uint hash_val, uint init_size, uint tnum){
  return hash_val&((init_size<<tnum)-1);
}

void DBF::setRemainingSlots(db_file* dbf, db_object* obj, uint obj_loc, int num_keys){
  if(num_keys==1){
    return;
  }
  int retries = dbf->retries;
  int num_subtables = dbf->num_subtables;
  int init_size = dbf->init_size;

  uint hashvals[num_keys][dbf->retries];
  uint tag[num_keys];
  uint key_hash, val_hash;
    
  for(int i=1;i<num_keys;i++){
    for(int j=0;j<retries;j++){
      hashvals[i][j] = DBF::hashObject(dbf, obj, j, i);
    }
    tag[i] = DBF::hashObject(dbf, obj, tag_seed, i)&TAG_MASK;
  }

  int hits=0;
  for(int j=1;j<num_keys;j++){
    int start=0;
    int not_set=1;
    while(not_set){

      for(int i=start;i<num_subtables;i++){

	for(int k=0;k<retries;k++){
	  int slot_num = DBF::getSlotIndex(hashvals[j][k], init_size, i);
	  slot* s = DBF::getSlot(dbf, slot_num, i);
	  dbg_printf("[seting] Trying slot [%d][%d][%d] -> %d -> Locs| %u\n", i,j,  k, slot_num, obj_loc);
	  if(DBF::isSlotFree(*s) == FREE){
	    dbg_printf("[setting] doing slot [%d][%d][%d] -> %d -> LocAdd| %u\n", i,j,  k, slot_num, obj_loc);
	    *s = obj_loc;
	    *s = (*s)<<32;
	    *s |= (SLOT_TAKEN) | (tag[j]<<1);
	    /*DBO::setSubtable(obj, i, j);
	    dbg_assert(DBO::getSubtable(obj, j) == i);
	    DBO::setKeyLoc(obj,DBO::getKeyLoc(obj,j), j);
	    dbg_assert(DBO::getSubtable(obj, j) == i);*/
	    dbg_assert(DBO::getDel(obj) == ACTIVE);
	    dbg_assert(DBF::getObjLoc(*s) == obj_loc);
	    dbg_assert(DBF::isSlotFree(*s)==ACTIVE);
	    dbg_assert(DBF::getSlotTag(*s) ==tag[j]);
	    k=retries;
	    i=num_subtables;
	    not_set=0;
	    hits++;
	    continue;
	  }
	}
      }
  
      if(not_set){
	start=dbf->num_subtables;
	DBF::addSubTable(dbf, dbf->num_subtables);
	num_subtables = dbf->num_subtables;
	obj=(db_object*)(dbf->region+obj_loc);
      }
    }
  }
  dbg_assert(hits==num_keys-1);
}
void DBF::unsetRemainingSlots(db_file* dbf, db_object* obj, uint obj_loc, int num_keys, int ignore_key){
  if(num_keys==1){
    return;
  }
  int retries = dbf->retries;
  int num_subtables = dbf->num_subtables;
  int init_size = dbf->init_size;

  uint hashvals[num_keys][retries];
  uint tag[num_keys];
  uint key_hash, val_hash;
  int was_ignored=0;
  for(int i=0;i<num_keys;i++){
    if(i==ignore_key){
      was_ignored++;
      continue;
    }
    for(int j=0;j<retries;j++){
      hashvals[i][j] = DBF::hashObject(dbf, obj, j, i);
    }
    tag[i]= DBF::hashObject(dbf, obj, tag_seed, i)&TAG_MASK;
  }
  dbg_assert(was_ignored==1);
  was_ignored=0;
  int hits=0;
  for(int subtable_num=0;subtable_num<num_subtables;subtable_num++){      
    for(int j=0;j<num_keys;j++){
      if(j==ignore_key){
	was_ignored++;
	continue;
      }

      for(int k=0;k<retries;k++){
	dbg_assert(j!=ignore_key);
	//	int subtable_num = DBO::getSubtable(obj, j);
	int slot_num = DBF::getSlotIndex(hashvals[j][k], init_size, subtable_num);
	slot* s = DBF::getSlot(dbf, slot_num, subtable_num);
	dbg_printf("[Unseting] Trying slot [%d][%d][%d] -> %d -> Locs| %u vs %u\n", subtable_num,j,  k, slot_num, obj_loc, DBF::getObjLoc(*s));
	if(DBF::isSlotFree(*s) == ACTIVE){
	  if(DBF::getObjLoc(*s) == obj_loc && DBF::getSlotTag(*s) == tag[j]){
	    hits++;
	    db_object* test_obj = DBF::getObject(dbf, DBF::getObjLoc(*s));
	    dbg_assert(DBO::getDel(test_obj) == ACTIVE);
	    for(int m=0;m<num_keys;m++){
	      dbg_printf("[Unset] Comparing[%d]: Key[%u]: %s and Val[%u]: %s vs Key[%u]: %s and Val[%u]: %s\n",
			 m,
			 DBO::getKeyLen(obj, m), DBO::getKey(obj, m),
			 DBO::getValLen(obj, m), DBO::getVal(obj, m),
			 DBO::getKeyLen(test_obj, m), DBO::getKey(test_obj, m),
			 DBO::getValLen(test_obj, m), DBO::getVal(test_obj, m));
	    }

			    
	    dbg_assert(DBO::compareObjects(obj, test_obj));
	    dbg_printf("[Unseting] doing slot [%d][%d][%d] -> %d\n", subtable_num,j,  k, slot_num);
	    *s ^= ACTIVE;
	    *s = 2;
	    dbg_assert(DBF::isSlotFree(*s) == FREE);
	    dbg_assert(*s != 0UL);

	    k=retries;
	    continue;
	  }
	}
      }
    }
    if(was_ignored!=1){
      fprintf(stderr,"Was ignored: %d\n", was_ignored);
      fprintf(stderr,"hits vs num_keys: %d vs %d\n", hits, num_keys-1);
      DBO::toString(obj);
    }
    dbg_assert(was_ignored==1);
    was_ignored=0;
  }


  if(hits!=num_keys-1){
  fprintf(stderr,"hits vs num_keys: %d vs %d\n", hits, num_keys-1);
  DBO::toString(obj);
  }
  dbg_assert(hits == num_keys-1);

}

//adds a sub table to array of subtables
/*void DBF::addSubTable(db_file* dbf, uint tnum){
  dbg_assert((!(dbf->file_end&0x7)) | (!tnum) );

  //this will be start of the object the subtable is stored in
  uint subtable_loc=dbf->file_end;
  dbf->file_end = HLP::round(dbf->file_end, sizeof(slot))+meta_data_size;

  //get space for subtable
  dbf->num_subtables=tnum+1;
  dbf->subtable_starts[tnum] = dbf->file_end;
  dbf->file_end+=(dbf->init_size<<(tnum))*sizeof(slot);
  while(dbf->file_end > dbf->file_size){
  DBF::extendFile(dbf);
  }

  //set meta data for subtable (this makes keeping the rest of
  //the object blocks organized much easier)
  db_object* subtable_obj = (db_object*)(dbf->region+subtable_loc);
  DBO::setTotalLen(subtable_obj, meta_data_size+(dbf->init_size<<(tnum))*sizeof(slot));
  DBO::setIsPrevFree(subtable_obj, dbf->is_last_free);
  DBO::setDel(subtable_obj, ACTIVE);
  dbf->is_last_free = ACTIVE;
  }

*/
//adds a sub table to array of subtables
int DBF::addSubTable(db_file* dbf, uint tnum){
  dbg_assert((!(dbf->file_end&0x7)) | (!tnum) );

  //this will be start of the object the subtable is stored in
  uint new_st_size = (dbf->init_size<<(tnum))*sizeof(slot);
  
  uint subtable_loc=DBF::findFreeFit(dbf,new_st_size + meta_data_size);
  if(subtable_loc == NIL){
    subtable_loc=dbf->file_end;
    dbf->file_end = HLP::round(dbf->file_end, sizeof(slot))+meta_data_size;

    //get space for subtable
    dbf->num_subtables=tnum+1;
    dbf->subtable_starts[tnum] = dbf->file_end;
    dbf->file_end+=new_st_size;
    while(dbf->file_end > dbf->file_size){
      DBF::extendFile(dbf);
    }
    dbg_printf("Subtable LOC[0]::::: %u\n", subtable_loc);
    //set meta data for subtable (this makes keeping the rest of
    //the object blocks organized much easier)
    db_object* subtable_obj = (db_object*)(dbf->region+subtable_loc);
    DBO::setNKeys(subtable_obj, 1);
    DBO::setKeyLoc(subtable_obj, 32, 0);
    DBO::setValLoc(subtable_obj, 32, 0);
    
    DBO::setTotalLen(subtable_obj, meta_data_size+new_st_size);
    DBO::setIsPrevFree(subtable_obj, dbf->is_last_free);
    DBO::setDel(subtable_obj, ACTIVE);
    dbf->is_last_free = ACTIVE;
    return 1;
  }
  else{
    dbf->num_subtables=tnum+1;
    dbf->subtable_starts[tnum] = subtable_loc+meta_data_size;
    db_object* temp = (db_object*)calloc(meta_data_size, 1);
    DBO::setNKeys(temp, 1);
    DBO::setKeyLoc(temp, 32, 0);
    DBO::setValLoc(temp, 32, 0);
    DBO::setTotalLen(temp, meta_data_size+new_st_size);
    db_object* spare = (db_object*)(dbf->region+subtable_loc);
    DBF::place(dbf, spare, subtable_loc, temp);
    writeWrapper((unsigned char*)temp, meta_data_size, dbf, subtable_loc);
    memset(dbf->region+subtable_loc+meta_data_size, 0, new_st_size);
    free(temp);
    return 0;
  }
}

//if db file already exists this will get the data out of it
//and start a new db_file* struct 
db_file* DBF::recoverFile(int fd){
  db_file* new_dbf = (db_file*)calloc(1, sizeof(db_file));
  if(!new_dbf){
    fprintf(stderr,"Fatal error in calloc!\n");
    exit(0);
  }
  struct stat f_info;
  fstat(fd, &f_info);
  new_dbf->fd= fd;
  new_dbf->file_size = HLP::nextPow2(f_info.st_size);
  if(!new_dbf->file_size && f_info.st_size){
    new_dbf->file_size = ~0;
  }
  new_dbf->region = (unsigned char*)mmap(NULL,
					 new_dbf->file_size,
					 PROT_WRITE|PROT_READ,
					 MAP_SHARED,
					 fd,
					 0);

  if(new_dbf->region == MAP_FAILED){
    fprintf(stderr,"Error mmaping file!\n");
    fprintf(stderr,"%s\n", strerror(errno));
    exit(0);
  }
  HLP::byteCopy((unsigned char*)new_dbf, new_dbf->region, recover_fields);
  return new_dbf;
}

db_file* DBF::initDBF(uint isize, uint retries, char* file_name, char* db_name){
  char full_path[64]="";
  sprintf(full_path, "%s/%s", db_name, file_name);
  if(verbose > 0){
    fprintf(stderr,"file_path: %s\n", full_path);
  }
  if(access(full_path, F_OK ) != -1){
    int fd = open(full_path, O_RDWR);
    if(fd<=0){
      fprintf(stderr,"Error opening file!\n");
      fprintf(stderr,"%s\n", strerror(errno));
      exit(0);
    }
    return DBF::recoverFile(fd);
  }
  int fd = open(full_path, O_CREAT | O_RDWR, 0666);
  if(fd<=0){
    fprintf(stderr,"Error opening file!\n");
    fprintf(stderr,"%s\n", strerror(errno));
    exit(0);
  }
  db_file* new_dbf = (db_file*)calloc(1, sizeof(db_file));
  if(!new_dbf){
    fprintf(stderr,"Fatal error in calloc!\n");
    exit(0);
  }
  new_dbf->fd = fd;
  new_dbf->file_size = init_file_size;
  if(posix_fallocate(new_dbf->fd, 0, new_dbf->file_size)!=0){
    fprintf(stderr,"Error extending file!\n");
    fprintf(stderr,"%s\n", strerror(errno));
    exit(0);
  }
  new_dbf->region = (unsigned char*)mmap(NULL,
					 new_dbf->file_size,
					 PROT_WRITE|PROT_READ,
					 MAP_SHARED,
					 fd,
					 0);
  if(new_dbf->region == MAP_FAILED){
    fprintf(stderr,"Error mmaping file!\n");
    fprintf(stderr,"%s\n", strerror(errno));
    exit(0);
  }
  //  memset(new_dbf->region, 0, new_dbf->file_size);
  new_dbf->is_last_free = ACTIVE;
  new_dbf->init_size = isize;
  new_dbf->retries = retries;
  for(int i=0;i<seg_list_size;i++){
    new_dbf->first_free[i] = NIL;
    new_dbf->last_free[i] = NIL;
  }
  new_dbf->seeds = rand();
  new_dbf->file_end = 0;

  
  dbg_printf("Recovery fields: %u\ndbf->file_size: %u\n", recover_fields, (uint)new_dbf->file_size);
  DBF::writeWrapper((unsigned char*)new_dbf, recover_fields, new_dbf, 0);
  DBF::addSubTable(new_dbf, 0);
  return new_dbf;
}
void DBF::cleanupDBF(db_file* dbf){
  DBF::writeWrapper((unsigned char*)dbf, recover_fields, dbf, 0);
  msync(dbf->region, dbf->file_end, MS_SYNC);
  if(munmap(dbf->region, dbf->file_size)){
    fprintf(stderr,"Error unmapping memory!\n");
    fprintf(stderr,"%s\n", strerror(errno));
    exit(0);
  }
}



void DBF::extendFile(db_file* dbf){
  msync(dbf->region, dbf->file_size, MS_SYNC);
  if(posix_fallocate(dbf->fd, dbf->file_size, dbf->file_size<<1)!=0){
    fprintf(stderr,"Error extending file(2)!\n");
    fprintf(stderr,"%s\n", strerror(errno));
    exit(0);
  }
  unsigned char* temp = (unsigned char*)mremap(dbf->region,
					       dbf->file_size,
					       dbf->file_size<<1,
					       MREMAP_MAYMOVE);
  if(temp == MAP_FAILED){
    fprintf(stderr,"Error remaping file!\n");
    fprintf(stderr,"%s\n", strerror(errno));
    exit(0);
  }
  dbf->region = temp;
  //  memset(dbf->region+dbf->file_size, 0, dbf->file_size);
  dbf->file_size = dbf->file_size<<1;

}


void DBF::writeWrapper(unsigned char* src, uint nbytes, db_file* dbf, uint pos){

  while(nbytes+pos > dbf->file_size){
    DBF::extendFile(dbf);
  }
  if(nbytes+pos > dbf->file_end){
    dbf->file_end = nbytes+pos;
  }

  return HLP::byteCopy((unsigned char*)dbf->region+pos, src, nbytes);
}



void DBF::freeBlock(db_file* dbf, db_object* obj, uint obj_loc){
#ifdef dbg_l
  dbg_assert(!DBF::dbg_is_in_list(dbf, obj_loc));
#endif
  DBO::setDel(obj, FREE);
  return DBF::coalesce(dbf, obj, obj_loc);
}

unsigned long DBF::getFooter(db_file* dbf, uint loc){
  return *((unsigned long*)(dbf->region+loc));
}
db_object* DBF::getObject(db_file* dbf, uint obj_loc){
  return (db_object*)(dbf->region+obj_loc);
}
db_object* DBF::getNextObject(db_file* dbf, db_object* obj, uint obj_loc){
  /*  if(DBO::getTotalLen(obj) + obj_loc >= dbf->file_size){
      return NULL;
      }*/
  if(DBO::getTotalLen(obj) + obj_loc >= dbf->file_end){
    return NULL;
  }

  dbg_printf("[Get Next Object] getting header at %u\n", DBO::getTotalLen(obj)+obj_loc);
  return (db_object*)(dbf->region+DBO::getTotalLen(obj)+obj_loc);
}

db_object* DBF::getPrevObject(db_file* dbf, db_object* obj, uint obj_loc){
  unsigned long pfooter = DBF::getFooter(dbf, obj_loc-sizeof(unsigned long));
  dbg_assert(!(pfooter&0x7));
  return DBF::getObject(dbf, obj_loc-pfooter);
}

int DBF::dbg_is_in_list(db_file* dbf, int loc){
#ifdef dbg_l
  uint cur = dbf->first_free;
  int to_ret = 0;
  while(cur!=NIL){
    db_object* check = DBF::getObject(dbf, cur);
    dbg_assert(PREV_ACTIVE==DBO::getIsPrevFree(check));
    dbg_assert(FREE==DBO::getDel(check));
    dbg_assert(DBO::getTotalLen(check) == DBO::getFooter(check));
    db_object* next_obj = DBF::getNextObject(dbf, check, cur);
    if(next_obj){
      dbg_assert(check==DBF::getPrevObject(dbf, next_obj, cur+DBO::getTotalLen(check)));
      dbg_assert(DBO::getIsPrevFree(next_obj)==PREV_FREE);
      dbg_assert(DBO::getDel(next_obj)==ACTIVE);
    }else{
      dbg_assert(cur+DBO::getTotalLen(check)==dbf->file_end);
      dbg_assert(dbf->is_last_free==FREE);
    }

    //debug start
    dbg_assert(DBO::getTotalLen(check) == DBO::getFooter(check));
    if(verbose>1){
      DBO::toString(check);
    }
    //debug end
    
    if(cur==loc){
      to_ret = 1;
    }
    cur = DBO::getNextFree(check);
  }
  return to_ret;
#else
  return 1;
#endif
}
void DBF::coalesce(db_file* dbf, db_object* obj, uint obj_loc){
  dbg_printf("[Coalesce] starting!\n");
#ifdef first_hit
  dbg_assert(DBO::getCounter(obj) == 0);
#endif
  db_object* next_obj = DBF::getNextObject(dbf, obj, obj_loc);
  db_object* prev_obj = NULL;
  uint next_free = ACTIVE, prev_free = ACTIVE;
  if(next_obj){
    dbg_printf("[Coalesce] next_obj exists!\n");
    db_object* next_next_obj = DBF::getNextObject(dbf,
						  next_obj,
						  obj_loc+DBO::getTotalLen(next_obj));
    int my_guess = dbf->is_last_free;
    if(next_next_obj){
      my_guess = DBO::getIsPrevFree(next_next_obj);
    }
    next_free = DBO::getDel(next_obj);
    DBO::setIsPrevFree(next_obj, PREV_FREE);
  }
  else{
    dbg_assert(obj_loc+DBO::getTotalLen(obj)==dbf->file_end);
    dbf->is_last_free = FREE;
  }
  if(PREV_FREE == DBO::getIsPrevFree(obj)){
    prev_obj = DBF::getPrevObject(dbf, obj, obj_loc);
    prev_free = FREE;
  }
  if(prev_free!=FREE && next_free!=FREE){
    dbg_printf("[Coalesce] neither adjacent blocks free!\n");
    DBO::setIsPrevFree(obj, ACTIVE);
    DBO::setFooter(obj, DBO::getTotalLen(obj));
    dbg_assert(DBO::getFooter(obj) == DBO::getTotalLen(obj));
    dbg_printf("[Coalesce] obj [%u, %u]\n", DBO::getTotalLen(obj), (uint)DBO::getFooter(obj));
    DBF::addToList(dbf, obj, obj_loc);
    dbg_assert(DBF::dbg_is_in_list(dbf, obj_loc));
    return;
  }
  if(prev_free==FREE && next_free!=FREE){
    dbg_printf("[Coalesce] previous block free!\n");
    DBF::removeFromList(dbf, prev_obj);
    uint prev_loc = obj_loc - DBO::getTotalLen(prev_obj);
    DBO::setTotalLen(prev_obj, DBO::getTotalLen(prev_obj) + DBO::getTotalLen(obj));
    DBO::setFooter(prev_obj, DBO::getTotalLen(prev_obj));
    DBF::addToList(dbf, prev_obj, prev_loc);
    dbg_assert(DBO::getFooter(prev_obj) == DBO::getTotalLen(prev_obj));
    dbg_assert(DBO::getTotalLen(prev_obj) >= min_block_size);
    dbg_assert(DBF::dbg_is_in_list(dbf, prev_loc));
    return;
  }
  if(prev_free!=FREE && next_free==FREE){
    dbg_printf("[Coalesce] next block free[%u + %u == %u]!\n", DBO::getTotalLen(obj), DBO::getTotalLen(next_obj), DBO::getTotalLen(obj) + DBO::getTotalLen(next_obj));
    DBF::removeFromList(dbf, next_obj);
    DBO::setTotalLen(obj, DBO::getTotalLen(obj) + DBO::getTotalLen(next_obj));
    DBO::setFooter(obj, DBO::getTotalLen(obj));
    dbg_assert(obj == ((db_object*)(dbf->region+obj_loc)));
    dbg_assert(DBO::getTotalLen(obj) >= min_block_size);
    dbg_assert(DBO::getFooter(obj) == DBO::getTotalLen(obj));
    
    DBF::addToList(dbf, obj, obj_loc);

    //fixme
    
    dbg_assert(DBF::dbg_is_in_list(dbf, obj_loc));
    return;
  }
  dbg_printf("[Coalesce] both adjacent blocks free!\n");
  uint prev_loc = obj_loc - DBO::getTotalLen(prev_obj);
  DBF::removeFromList(dbf, next_obj);
  DBF::removeFromList(dbf, prev_obj);
  DBO::setTotalLen(prev_obj,
		   DBO::getTotalLen(obj) +
		   DBO::getTotalLen(next_obj) +
		   DBO::getTotalLen(prev_obj));
  dbg_printf("[Coalesce]: Total Len [%u]\n", DBO::getTotalLen(prev_obj));
  DBO::setFooter(prev_obj, DBO::getTotalLen(prev_obj));
  DBF::addToList(dbf, prev_obj, prev_loc);
  dbg_assert(DBO::getTotalLen(prev_obj) >= min_block_size);
  dbg_assert(DBO::getFooter(prev_obj) == DBO::getTotalLen(prev_obj));
  dbg_assert(DBF::dbg_is_in_list(dbf, prev_loc));
  return;
}

void DBF::addToList(db_file* dbf, db_object* obj, uint obj_loc){
  int list_num = calculateI(DBO::getTotalLen(obj));

  DBO::setNextFree(obj, dbf->first_free[list_num]);
  DBO::setPrevFree(obj, NIL);
  if(dbf->first_free[list_num]!=NIL){
    
    dbg_printf("[Add to List] next free from [%u]!\n", dbf->first_free[list_num]);
    
    db_object* next_free = getObject(dbf, dbf->first_free[list_num]);
    DBO::setPrevFree(next_free, obj_loc);
  }else{
    dbf->last_free[list_num]=obj_loc;
  }
  dbf->first_free[list_num]=obj_loc;
  dbg_printf("[Add To List] obj [prev, next], dbf[first == obj_loc[%u]]: [%u, %u], [%u == %u]\n",
	     DBO::getTotalLen(obj),
	     DBO::getPrevFree(obj),
	     DBO::getNextFree(obj),
	     dbf->first_free[list_num], obj_loc);
}

void DBF::removeFromList(db_file* dbf, db_object* obj){
  int list_num = calculateI(DBO::getTotalLen(obj));
  uint loc_prev_free = DBO::getPrevFree(obj);
  uint loc_next_free = DBO::getNextFree(obj);
  
  dbg_printf("[Remove From List[%u]] removing [%u: %u,%u]\n", list_num, dbf->first_free[list_num], loc_prev_free, loc_next_free);
  
  if(loc_prev_free == NIL && loc_next_free == NIL){
    
    dbg_printf("[Remove From List Case 1] [%u, %u] -> [NIL, NIL]\n", loc_prev_free, loc_next_free);
    
    dbf->first_free[list_num] = NIL;
    dbf->last_free[list_num] = NIL;
  }
  else if(loc_prev_free != NIL && loc_next_free == NIL){
    db_object* prev_free = getObject(dbf, loc_prev_free);
    
    dbg_printf("[Remove From List Case 2] [%u, %u] -> [%p, NIL]\n", loc_prev_free, loc_next_free, prev_free);
    
    DBO::setNextFree(prev_free, NIL);
    dbf->last_free[list_num] = loc_prev_free;
  }
  else if(loc_prev_free == NIL && loc_next_free != NIL){
    db_object* next_free = getObject(dbf, loc_next_free);
    
    dbg_printf("[Remove From List Case 3] [%u, %u] -> [NIL, %p]\n", loc_prev_free, loc_next_free, next_free);
    
    DBO::setPrevFree(next_free, NIL);
    dbf->first_free[list_num] = loc_next_free;
  }
  else{
    db_object* next_free = getObject(dbf, loc_next_free);
    db_object* prev_free = getObject(dbf, loc_prev_free);
    
    dbg_printf("[Remove From List Case 4] [%u, %u] -> [%p, %p]\n", loc_prev_free, loc_next_free, prev_free, next_free);
    
    DBO::setPrevFree(next_free, loc_prev_free);
    DBO::setNextFree(prev_free, loc_next_free);
  }
}

void DBF::place(db_file* dbf, db_object* old_obj, uint old_loc, db_object* new_obj){
  uint old_size= DBO::getTotalLen(old_obj);
  uint new_size= DBO::getTotalLen(new_obj);
  char is_prev_free = DBO::getIsPrevFree(old_obj);
  
  //debug start
  dbg_printf("[Place] old_size[at %u]  vs new_size[%d]: %u == %u vs %u\n", old_loc, (int)is_prev_free,old_size,(uint)DBO::getFooter(old_obj),new_size);
  dbg_assert(old_size == DBO::getFooter(old_obj));
  //debug end
  db_object* temp_old_obj=NULL;
  if(old_size-new_size > min_block_size){
    //need to remove because next/prev pouinter will be wrong otherwise.
    DBF::removeFromList(dbf, old_obj);
    temp_old_obj = (db_object*)(((unsigned char*)old_obj)+new_size);
    DBO::setDel(temp_old_obj, FREE);
    DBO::setTotalLen(temp_old_obj, old_size-new_size);
    DBO::setFooter(temp_old_obj, old_size-new_size);
    DBO::setIsPrevFree(temp_old_obj, PREV_ACTIVE);
    DBF::addToList(dbf, temp_old_obj, old_loc+new_size);
    
    //debug start
    dbg_assert(DBO::getTotalLen(temp_old_obj) >= min_block_size);
    dbg_assert(DBO::getTotalLen(temp_old_obj) == DBO::getFooter(temp_old_obj));
    dbg_printf("[Place] setting old_obj at %u==%u [%u]\n",
	       DBO::getTotalLen(temp_old_obj),
	       (uint)DBO::getFooter(temp_old_obj),
	       old_loc+new_size);
    dbg_printf("[Place] writing back old_obj[%u == %lu] to %u == %x\n", DBO::getTotalLen(temp_old_obj), DBO::getFooter(temp_old_obj), old_loc+new_size,old_loc+new_size);
    if(verbose>1){
      DBO::toString(temp_old_obj);
    }
    //debug end
  }
  else{
    dbg_assert(DBF::dbg_is_in_list(dbf, old_loc));
    DBF::removeFromList(dbf, old_obj);
    DBO::setTotalLen(new_obj, old_size);
    if(old_loc+DBO::getTotalLen(new_obj) == dbf->file_end){
      dbf->is_last_free = ACTIVE;
    }else{
      db_object* next_obj = DBF::getNextObject(dbf, new_obj, old_loc);
      DBO::setIsPrevFree(next_obj, ACTIVE);
      dbg_assert(DBO::getDel(next_obj)==ACTIVE);
    }
    //debug start
    dbg_assert(DBO::getTotalLen(new_obj) >= min_block_size);
    //debug end
    
  }
  
  DBO::setIsPrevFree(new_obj, is_prev_free);

  //debug start
  dbg_printf("[Place] writing back new_obj[%u] to %u == %x\n", DBO::getTotalLen(new_obj), old_loc, old_loc);
  if(verbose>1){
    DBO::toString(new_obj);
  }
  //debug end
  DBO::setDel(new_obj, ACTIVE);


  DBF::dbg_is_in_list(dbf, old_loc);
      
}

uint DBF::findFreeFit(db_file* dbf, uint obj_size){
  uint start_loop = DBF::calculateI(obj_size);
  for(uint i =start_loop;i<seg_list_size;i++){
    //  uint cur = dbf->first_free[i];
    uint cur = dbf->last_free[i];
    dbg_printf("[Find Fit] First loc: %u\n", cur);
    while(cur!=NIL){
      db_object* check = DBF::getObject(dbf, cur);
      dbg_printf("[Find Fit] Got obj[%u] -> %p\n", cur, check);

      //debug start
      dbg_assert(PREV_ACTIVE==DBO::getIsPrevFree(check));
      dbg_assert(FREE==DBO::getDel(check));
      dbg_assert(DBO::getTotalLen(check) == DBO::getFooter(check));
      dbg_printf("[Find Fit] Checking obj[%u]: %u >= %u -> %u\n", cur, DBO::getTotalLen(check), obj_size, DBO::getTotalLen(check) >= obj_size);
      if(verbose>1){
	DBO::toString(check);
      }
      //debug end
    
      if(DBO::getTotalLen(check) >= obj_size){
	dbg_printf("[Find Fit] Success!\n");
	return cur;
      }
      //      cur = DBO::getNextFree(check);
      cur = DBO::getPrevFree(check);
    }
  }
  return NIL;
}

uint DBF::hashBytes(uint seed, uint b_len, uint8_t* bytes){
  //  dbg_printf("[Hash Bytes] -> [seed, blen]: [%u, %u]\n", seed, b_len);
  b_len = min(max_hash_size, b_len);
  return HASH::murmur3_32(bytes,(size_t)b_len,seed);
}

uint DBF::hashObject(db_file* dbf, db_object* obj, uint seed_num, int key_num){
  char seed = DBF::getSeedN(dbf, seed_num);
  /*printf("[HashObject] seed: %c\nkey_loc[%d]: %p -> %s\nval_loc[%d]: %p -> %s\n",
    seed,
    min(max_hash_size, DBO::getKeyLen(obj)),
    DBO::getKey(obj),
    DBO::getKey(obj),
    min(max_hash_size, DBO::getValLen(obj)),
    DBO::getVal(obj),
    DBO::getVal(obj));*/
  
  uint key_hash = DBF::hashBytes((uint)seed,
				 DBO::getKeyLen(obj, key_num),
				 (uint8_t*)DBO::getKey(obj, key_num));
  
  uint val_hash = DBF::hashBytes((uint)seed,
				 DBO::getValLen(obj,key_num),
				 (uint8_t*)DBO::getVal(obj, key_num));
  dbg_printf("[HashObject] key: %s and val: %s -> %u\n",DBO::getKey(obj, key_num),DBO::getVal(obj, key_num), combine_hash(key_hash, val_hash));

  return combine_hash(key_hash, val_hash);
}


obj_node* DBF::findObject(db_file* dbf,
			  uint num_keys,
			  int* key_slots,
			  uint* key_len, uint* val_len,
			  unsigned char** key, unsigned char** val){

  obj_node* ret = NULL;
  int retries = dbf->retries;
  int num_subtables = dbf->num_subtables;
  int init_size = dbf->init_size;
  uint hashvals[num_keys][dbf->retries];
  uint tag[num_keys];
  
  uint key_hash, val_hash;
  

  //  dbg_printf("[Find Obj] tag: %x\n", tag);
  for(int i=0;i<num_keys;i++){
    for(int j=0;j<retries;j++){
      
      key_hash = hashBytes((uint)DBF::getSeedN(dbf, j),
			   key_len[key_slots[i]],
			   (uint8_t*)key[key_slots[i]]);
      val_hash = hashBytes((uint)DBF::getSeedN(dbf, j),
			   val_len[key_slots[i]],
			   (uint8_t*)val[key_slots[i]]);
      hashvals[i][j] = combine_hash(key_hash, val_hash);


      dbg_printf("[Find Obj] hashvals[%d]: %x\n", i, hashvals[i][j]);
    }
    key_hash = hashBytes((uint)DBF::getSeedN(dbf, tag_seed),
			 key_len[key_slots[i]],
			 (uint8_t*)key[key_slots[i]]);
    val_hash = hashBytes((uint)DBF::getSeedN(dbf, tag_seed),
			 val_len[key_slots[i]],
			 (uint8_t*)val[key_slots[i]]);
    tag[i]= (combine_hash(key_hash, val_hash))&TAG_MASK;
    dbg_printf("[Find] tag: %x\n", tag[i]);
  }
  for(int i=0;i<num_subtables;i++){
    for(int j=0;j<num_keys;j++){
      for(int k=0;k<retries;k++){
	int slot_num = DBF::getSlotIndex(hashvals[j][k], init_size, i);
      
	dbg_assert(slot_num < (init_size<<i));
      
	slot* s = DBF::getSlot(dbf, slot_num, i);
      
	dbg_printf("[Find Obj] Trying slot [%d][%d][%d] -> %d\n", i,j,  k, slot_num);
      
	if(DBF::isSlotFree(*s) == ACTIVE){
	  dbg_printf("[Find Obj] Slot Active [%d][%d][%d][%d] comparing tags %x vs %x\n", i,j,  k, slot_num, tag[j], DBF::getSlotTag(*s));
	
	  if(tag[j] == DBF::getSlotTag(*s)){
	  
	    dbg_printf("[Find Obj] Tags Equal [%d][%d][%d][%d][%x] Comparing obj\n", i, j, k, slot_num, tag[j]);

	    db_object* test_obj = DBF::getObject(dbf, DBF::getObjLoc(*s));
	    dbg_assert(DBO::getDel(test_obj) == ACTIVE);
	    for(int m=0;m<num_keys;m++){
	      dbg_printf("Key: %d\n", m);
	      dbg_printf("[Find] objs: \nKey: %s Val: %s\nvs\n Key: %s Val: %s\n",
			 key[key_slots[m]], val[key_slots[m]],
			 DBO::getKey(test_obj, key_slots[m]), DBO::getVal(test_obj, key_slots[m]));
	    }

	    dbg_printf("About to compare!\n");
	    if(DBO::compareAll(num_keys, key_slots, key_len, val_len, key, val, test_obj)){	    
	      dbg_printf("[Find Obj] Objects Equal [%d][%d][%d][%x] returning\n", i, k, slot_num, tag[j]);

	      ret = DBO::addNode(ret, DBO::newObjNode(test_obj));
	    }
	    else{
	      for(int m=0;m<num_keys;m++){
		dbg_printf("[Find] Miscompare[%d]: Key[%u]: %s and Val[%u]: %s vs Key[%u]: %s and Val[%u]: %s\n",
			   key_slots[m],
			   key_len[key_slots[m]], key[key_slots[m]], val_len[key_slots[m]], val[key_slots[m]],
			   DBO::getKeyLen(test_obj, key_slots[key_slots[m]]), DBO::getKey(test_obj, key_slots[m]),
			   DBO::getValLen(test_obj, key_slots[m]), DBO::getVal(test_obj, key_slots[m]));
	      }
	    
	    }
	    dbg_assert(DBO::getDel(test_obj)==ACTIVE);

	  }
	}
	else if(*s == (0UL)){
	  return ret;
	}
      }
    }
  }
  return ret;
}
uint DBF::setObjInSlot(db_file* dbf, db_object* new_obj, int new_obj_size, int subtable_num, int slot_num, uint tag){
  slot* s =  DBF::getSlot(dbf, slot_num, subtable_num);
  //debug start
  dbg_printf("[Add] Slot is Free\n");
  //debug end
	  
  uint obj_loc = DBF::findFreeFit(dbf, new_obj_size);

  //debug start
  dbg_printf("[Add] Free Loc: %u\n", obj_loc);
  //debug end
	  
  db_object* old_obj = NULL;
  if(obj_loc == NIL){
    obj_loc = dbf->file_end;
    uint obj_len =0;
    *s = obj_loc;
    *s = (*s)<<32;
    *s |= (SLOT_TAKEN) | (tag<<1);
    dbg_assert(subtable_num==subtable_num&0xf);
    DBO::setSubtable(new_obj, subtable_num, 0);
	    
    if(dbf->file_size < dbf->file_end + (new_obj_size<<1)){
      while(dbf->file_end + (new_obj_size<<1) > dbf->file_size){
	DBF::extendFile(dbf);
      }
      old_obj = (db_object*)(dbf->region+dbf->file_end);
      dbf->file_end+=new_obj_size<<1;
      obj_len = new_obj_size<<1;
      dbg_printf("[Add] case 0 size -> %u\n", obj_len);
    }
    else{
      old_obj = (db_object*)(dbf->region+obj_loc);
      obj_len = dbf->file_size-dbf->file_end;
      dbf->file_end = dbf->file_size;
      dbg_printf("[Add] case 1 size -> %u\n", obj_len);

    }

	    
    //debug stuff start
    dbg_printf("[Add] is_last_free before -> %u\n", dbf->is_last_free);
    //debug stuff end
	    
    DBO::setIsPrevFree(old_obj, dbf->is_last_free);


    //debug start
    dbg_printf("[Add] Excess space!\n");
    //debug end
	      
    dbf->is_last_free = FREE;	      
    DBO::setTotalLen(old_obj, obj_len);
    DBO::setFooter(old_obj, obj_len);
    DBO::setDel(old_obj, FREE);
    //	    DBF::freeBlock(dbf, old_obj, obj_loc);
    DBF::addToList(dbf, old_obj, obj_loc);

    //debug start
    dbg_printf("[Add] new block[%u] [%u == %u]\n",
	       obj_loc, DBO::getTotalLen(old_obj),
	       (uint)DBO::getFooter(old_obj));
    dbg_printf("[Add] is_last_free after -> %u\n", dbf->is_last_free);
      
    //debug end
	      
  }
  else{
	    
    //debug start
    dbg_printf("[Add] Getting object at %u\n", obj_loc);
    //debug end
	    
    old_obj = DBF::getObject(dbf, obj_loc);
    *s = obj_loc;
    *s = (*s)<<32;
    *s |= (SLOT_TAKEN) | (tag<<1);
    dbg_assert(subtable_num==subtable_num&0xf);
    DBO::setSubtable(new_obj, subtable_num, 0);

  }

  //debug start
  dbg_printf("[Add] placing loc[%u]!\n", obj_loc);
  //debug end

  DBF::place(dbf, old_obj, obj_loc, new_obj);
  writeWrapper((unsigned char*)new_obj, new_obj_size, dbf, obj_loc);
  s =  DBF::getSlot(dbf, slot_num, subtable_num);
  dbg_assert(isSlotFree(*s)==ACTIVE);	  
  DBF::setRemainingSlots(dbf, (db_object*)(dbf->region+obj_loc), obj_loc, DBO::getNKeys(new_obj));
  //  DBF::unsetRemainingSlots(dbf, (db_object*)(dbf->region+obj_loc), obj_loc, DBO::getNKeys(new_obj), 0);
  //  DBF::setRemainingSlots(dbf, (db_object*)(dbf->region+obj_loc), obj_loc, DBO::getNKeys(new_obj));
  s =  DBF::getSlot(dbf, slot_num, subtable_num);
  dbg_assert(isSlotFree(*s)==ACTIVE);
  dbg_assert(DBO::getCounter(new_obj) >= 0);
  free(new_obj);


  return obj_loc;
}





uint DBF::addObject(db_file* dbf, db_object* new_obj){
  int retries = dbf->retries;
  int num_subtables = dbf->num_subtables;
  int init_size = dbf->init_size;
  uint hashvals[dbf->retries];



  
  for(int i=0;i<retries;i++){
    hashvals[i] = DBF::hashObject(dbf, new_obj, i, 0);
    dbg_printf("[Add] hashvals[%d]: %x\n", i, hashvals[i]);
  }
  uint tag = DBF::hashObject(dbf, new_obj, tag_seed, 0)&TAG_MASK;
  dbg_printf("[Add] tag: %x\n", tag);
  int start = 0;
  int did_add=0;
  int new_obj_size = DBO::getTotalLen(new_obj);
  int find_slot = 0;
  int slot_cords[2] = {0,0};
  while(1){

    //debug start
    dbg_printf("[Add] start loop [ret][nsub] -> [%u][%u] start/num_subtable: %u/%u\n",
	       retries,
	       num_subtables,
	       start,
	       dbf->num_subtables);
    //debug end
    
    for(int i=start;i<num_subtables;i++){
      for(int j=0;j<retries;j++){
	int slot_num = DBF::getSlotIndex(hashvals[j], init_size, i);

	//debug start
	dbg_assert(slot_num < (init_size<<i));
	dbg_printf("[Add] slot_num: %d\n", slot_num);
	//debug end
	
	slot* s = DBF::getSlot(dbf, slot_num, i);

	//debug start
	dbg_printf("[Add] Got Slot: %lx\n", *s);
	//debug end
	
	if(DBF::isSlotFree(*s) == FREE){
	  dbg_printf("[Add] slot is free, adding!\n");
#ifdef first_hit
	  dbg_printf("Add Method 0\n");
	  uint test_loc = DBF::setObjInSlot(dbf, new_obj, new_obj_size, i, slot_num, tag);
	  s = DBF::getSlot(dbf, slot_num, i);
	  dbg_assert(DBF::getObjLoc(*s) == test_loc);
	  db_object* tester = (db_object*)(dbf->region + DBF::getObjLoc(*s));
	  dbg_assert(DBO::getDel(tester)==ACTIVE);
	  dbg_assert(DBF::isSlotFree(*s) ==ACTIVE);
	  dbg_assert(DBF::getSlotTag(*s) ==tag);
	  return 0;
#endif
	  if(*s == 0UL){
	    dbg_printf("Add Method 1\n");
	    uint test_loc = DBF::setObjInSlot(dbf, new_obj, new_obj_size, i, slot_num, tag);
	    s = DBF::getSlot(dbf, slot_num, i);
	    dbg_assert(DBF::getObjLoc(*s) == test_loc);
	    db_object* tester = (db_object*)(dbf->region + DBF::getObjLoc(*s));
	    dbg_assert(DBO::getDel(tester)==ACTIVE);
	    dbg_assert(DBF::isSlotFree(*s) ==ACTIVE);
	    dbg_assert(DBF::getSlotTag(*s) ==tag);
	    return 0;

	  }
	  find_slot = 1;
	  slot_cords[0] = i;
	  slot_cords[1] = slot_num;
	}
	else if(DBF::getSlotTag(*s) == tag){
	  db_object* test_obj = DBF::getObject(dbf, DBF::getObjLoc(*s));
	  dbg_assert(DBO::getDel(test_obj) ==ACTIVE);
	  dbg_printf("[add] testing counter incremented!\nKey: %s Val: %s\nvs\n Key: %s Val: %s\n",
		     DBO::getKey(new_obj, 0), DBO::getVal(new_obj, 0),
		     DBO::getKey(test_obj, 0), DBO::getVal(test_obj, 0));
	  if(DBO::compareObjects(new_obj, test_obj)){
	    dbg_printf("Add Method 2\n");
	    dbg_printf("[add] Successfully counter incremented!\nKey: %s Val: %s\nvs\n Key: %s Val: %s\n",
		       DBO::getKey(new_obj, 0), DBO::getVal(new_obj, 0),
		       DBO::getKey(test_obj, 0), DBO::getVal(test_obj, 0));
	    DBO::setCounter(test_obj, DBO::getCounter(test_obj)+1);
	    dbg_assert(DBO::getCounter(test_obj) >= 1);
	    return 0;
	  }
	}
      }
    }
    if(find_slot){
      dbg_printf("Add Method 3\n");
      uint test_loc = DBF::setObjInSlot(dbf, new_obj, new_obj_size, slot_cords[0], slot_cords[1], tag);
      slot* s = DBF::getSlot(dbf, slot_cords[1], slot_cords[0]);
      dbg_assert(DBF::getObjLoc(*s) == test_loc);
	    
      db_object* tester = (db_object*)(dbf->region + DBF::getObjLoc(*s));
      dbg_assert(DBO::getDel(tester)==ACTIVE);
      dbg_assert(DBF::isSlotFree(*s) ==ACTIVE);
      dbg_assert(DBF::getSlotTag(*s) ==tag);
      return 0;
    }

    
    start=dbf->num_subtables;
    did_add = DBF::addSubTable(dbf, dbf->num_subtables);
    num_subtables = dbf->num_subtables;
    //debug start
    dbg_printf("[Add] new subtable start/num_subtable: %u/%u\n", start, num_subtables);
    //debug end
  }
  return -1;
}



uint DBF::delObject(db_file* dbf,
		    int num_keys,
		    uint* key_len, uint* val_len,
		    unsigned char** key, unsigned char** val){
  int retries = dbf->retries;
  int num_subtables = dbf->num_subtables;
  int init_size = dbf->init_size;
  uint hashvals[num_keys][dbf->retries];
  uint tag[num_keys];
  
  uint key_hash, val_hash;



  
  int ret=0;
  for(int i=0;i<num_keys;i++){
    for(int j=0;j<retries;j++){
    
      key_hash = hashBytes((uint)DBF::getSeedN(dbf, j), key_len[i], (uint8_t*)key[i]);
      val_hash = hashBytes((uint)DBF::getSeedN(dbf, j), val_len[i], (uint8_t*)val[i]);
      hashvals[i][j] = combine_hash(key_hash, val_hash);


    }
    key_hash = hashBytes((uint)DBF::getSeedN(dbf, tag_seed), key_len[i], (uint8_t*)key[i]);
    val_hash = hashBytes((uint)DBF::getSeedN(dbf, tag_seed), val_len[i], (uint8_t*)val[i]);
    tag[i]= (combine_hash(key_hash, val_hash))&TAG_MASK;
  }
  
  for(int i=0;i<num_subtables;i++){
    for(int j=0;j<num_keys;j++){
      for(int k=0;k<retries;k++){
	int slot_num = DBF::getSlotIndex(hashvals[j][k], init_size, i);
	
	dbg_assert(slot_num < (init_size<<i));
	slot* s = DBF::getSlot(dbf, slot_num, i);
	if(DBF::isSlotFree(*s) == ACTIVE){
	  
	  if(tag[j] == DBF::getSlotTag(*s)){
	    uint obj_loc = DBF::getObjLoc(*s) ;
	    dbg_printf("[Del] deleteing object from slot %u -> %x\n", slot_num, tag[j]);
	    db_object* test_obj = DBF::getObject(dbf, obj_loc);
	    dbg_assert(DBO::getDel(test_obj) == ACTIVE);
	    if(DBO::compareAll(num_keys, NULL,  key_len, val_len, key, val, test_obj)){
#ifdef first_hit
	      int obj_count = DBO::getCounter(test_obj);


	      dbg_assert(obj_count >= 0);
	      if(obj_count){
		dbg_printf("[Del] count version! \nKey: %s Val: %s\nvs\n Key: %s Val: %s\n",
			   key[0], val[0],
			   DBO::getKey(test_obj, 0), DBO::getVal(test_obj, 0));
		DBO::setCounter(test_obj, obj_count-1);
		return 1;
	      }else{
#endif


		dbg_printf("[Del] count version! \nKey: %s Val: %s\nvs\n Key: %s Val: %s\n",
			   key[0], val[0],
			   DBO::getKey(test_obj, 0), DBO::getVal(test_obj, 0));
	
		*s ^= ACTIVE;
		*s = 2;
		dbg_assert(DBF::isSlotFree(*s) == FREE);
		dbg_assert(*s != 0UL);

		ret = 1;
		DBF::unsetRemainingSlots(dbf,
					 test_obj,
					 obj_loc,
					 DBO::getNKeys(test_obj),
					 j);

		DBO::setDel(test_obj, FREE);
		DBF::freeBlock(dbf, test_obj, obj_loc);

#ifdef first_hit
		return 1;
	      }
#endif
	    }
	    else{

	      for(int m=0;m<num_keys;m++){
		dbg_printf("[Find] Miscompare[%d]: Key[%u]: %s and Val[%u]: %s vs Key[%u]: %s and Val[%u]: %s\n",
			   m,
			   key_len[m], key[m], val_len[m], val[m],
			   DBO::getKeyLen(test_obj, m), DBO::getKey(test_obj, m),
			   DBO::getValLen(test_obj, m), DBO::getVal(test_obj, m));
	      }

	    }
	  }
	}
	else if(*s == (0UL)){
	  return ret;
	}
      }
    }
  }
  return ret;
}

void DBF::toString(db_file* dbf){
  fprintf(stderr,"db_file {\n");
  if(dbf->num_subtables){
    fprintf(stderr,"subtable_starts[%u] = ",dbf->num_subtables);

    for(int i=0;i<dbf->num_subtables-1;i++){
      fprintf(stderr,"%u ", dbf->subtable_starts[i]);
    }
    fprintf(stderr,"%u\n", dbf->subtable_starts[dbf->num_subtables-1]);
  }
  else{
    fprintf(stderr,"subtable_starts[%u] = \n",dbf->num_subtables);
  }
  fprintf(stderr,"num_tables: %u\ninit_size: %u\nfile_size: %u\nfirst_free: %u\nis_last_free: %u\nretries: %u\nseeds: %x\nregion: %p\n\t}\n",
	  dbf->num_subtables,
	  dbf->init_size,
	  dbf->file_size,
	  dbf->first_free[0],
	  dbf->is_last_free,
	  dbf->retries,
	  dbf->seeds,
	  dbf->region);
}


uint DBF::hob(uint i) {
  i--;
  i |= (i >>  1);
  i |= (i >>  2);
  i |= (i >>  4);
  i |= (i >>  8);
  i |= (i >> 16);
  i++;
  return __builtin_ctz(i);

}
//calculates which slot in the seglist a given block
//should be in based on its size.
uint DBF::calculateI (uint size){
  uint ret = DBF::hob(size>>seg_list_log);
  if(ret < seg_list_size){
    return ret;
  }
  return seg_list_size-1;
}

