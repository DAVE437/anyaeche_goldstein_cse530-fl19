#include "helpers.h"


void HLP::freeCheck(void* p){
  if(p){
    free(p);
  }
}

uint HLP::nextPow2(uint val){
  val--;
  val |= val >> 1;
  val |= val >> 2;
  val |= val >> 4;
  val |= val >> 8;
  val |= val >> 16;
  val++;
  return val;
}
uint HLP::byteCompare(unsigned char* a, unsigned char* b, uint len){
  dbg_printf("[byteCompare][%u]\n\t%p -> %p\nvs\n\t%p -> %p\n", len, a,a, b, b);
  unsigned long* a_ul = (unsigned long*)a;
  unsigned long* b_ul = (unsigned long*)b;
  uint i;
  for(i=0;i<(len&(~0x7));i+=sizeof(unsigned long)){
    if(a_ul[i>>3]!=b_ul[i>>3]){
      return 0;
    }
  }

  for(;i<len;i++){
    if(a[i]!=b[i]){
      return 0;
    }
  }
  return 1;
}

void HLP::byteCopy(unsigned char* a, unsigned char* b, uint len){
  dbg_printf("[byteCopy Before][%u->%u]\n\t%p -> %s\nvs\n\t%p -> %s\n", len, (len&(~0x7)), a,a, b, b);
  unsigned long* a_ul = (unsigned long*)a;
  unsigned long* b_ul = (unsigned long*)b;
  uint i=0;
  for(i=0;i<(len&(~0x7));i+=sizeof(unsigned long)){
    a_ul[i>>3]=b_ul[i>>3];
  }
  for(;i<len;i++){
    a[i]=b[i];
  }
  dbg_printf("[byteCopy After][%u->%u]\n\t%p -> %s\nvs\n\t%p -> %s\n", len, (len&(~0x7)), a,a, b, b);
}

void HLP::bkwdCopy(unsigned char* a, unsigned char* b, uint len){
  unsigned long* a_ul = (unsigned long*)a;
  unsigned long* b_ul = (unsigned long*)b;
  uint i;
  for(i=len-1;i>=(len&~0x7);i--){
    b[i]=a[i];
  }
  for(i=len;i>=0;i-=sizeof(unsigned long)){
    b[i] = a[i];
  }
}

uint HLP::round(uint val, uint allignment){
  return ((val+(allignment-1))/allignment)*allignment;
}

void HLP::scan(uint* arr, int len){
  dbg_printf("[Scan] [len: %d] -> %u ", len, arr[0]);
  for(int i = 1;i<len;i++){
    dbg_printf("%u ", arr[i]);
    arr[i]+=arr[i-1];
  }
  dbg_printf("\n");
  
}
