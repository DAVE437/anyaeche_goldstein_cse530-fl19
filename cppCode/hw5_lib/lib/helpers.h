#pragma once

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <errno.h>

extern int verbose;

typedef unsigned int uint;

#define dbg_a
//#define dbg_p
//#define dbg_l
#ifdef dbg_p
#define dbg_printf(...) fprintf(stderr, __VA_ARGS__)
#else
#define dbg_printf(...) 
#endif
#ifdef dbg_a
#define dbg_assert(...)  assert(__VA_ARGS__)
#else
#define dbg_assert(...)
#endif

#define min(X, Y)  ((X) < (Y) ? (X) : (Y))
#define max(X, Y)  ((X) < (Y) ? (Y) : (X))




namespace HLP{
  //returns 1 if equals, 0 otherwise
  uint byteCompare(unsigned char* a, unsigned char* b, uint len);
  void byteCopy(unsigned char* a, unsigned char* b, uint len);
  void freeCheck(void* p);
  void bkwdCopy(unsigned char* a, unsigned char* b, uint len);
  uint round(uint val, uint allignment);
  uint nextPow2(uint val);
  void scan(uint* arr, int len);
};
