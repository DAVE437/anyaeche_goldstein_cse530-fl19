#pragma once

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#define max_hash_size 255
typedef unsigned int uint;
namespace HASH {
uint murmur3_32(const uint8_t* key, size_t len, uint seed);

};
