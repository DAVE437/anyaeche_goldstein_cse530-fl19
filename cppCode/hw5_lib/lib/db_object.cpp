#include "db_object.h"

//getters


obj_node* DBO::newObjNode(db_object* obj){
  obj_node* new_node = (obj_node*)calloc(1, sizeof(obj_node));
  new_node->obj = obj;
  return new_node;
}
obj_node* DBO::addNode(obj_node* list, obj_node* new_node){
  new_node->next = list;
  return new_node;
}
void DBO::freeList(obj_node* list){
  obj_node* temp = list;
  obj_node* next = NULL;
  while(temp){
    next = temp->next;
    free(temp);
    temp=next;
  }
}

uint __attribute__ ((noinline)) DBO::getKeyLen(db_object* obj, int key_num){
  if(key_num){
    dbg_printf("getting key len of %d: == %u-%u -> %u\n",key_num, DBO::getKeyLoc(obj, key_num), DBO::getKeyLoc(obj, key_num-1),(DBO::getKeyLoc(obj, key_num) - DBO::getKeyLoc(obj, key_num-1)));
     return (DBO::getKeyLoc(obj, key_num) - DBO::getKeyLoc(obj, key_num-1));
  }
   else{
  return DBO::getKeyLoc(obj, key_num);
   }

}

uint DBO::getValLen(db_object* obj, int key_num){
  uint ret;
  if(key_num){
   ret= DBO::getValLoc(obj, key_num) - DBO::getValLoc(obj, key_num-1);
   //   dbg_assert(ret==32);
   return ret;
  }
  ret =  DBO::getValLoc(obj, key_num);
  //  dbg_assert(ret==32);
  return ret;
}

uint DBO::getNKeys(db_object* obj){
  return (obj->obj_count&byte_mask);
}

uint DBO::getCounter(db_object* obj){
  return (obj->obj_count>>(byte_shift));
}


unsigned long DBO::getFooter(db_object* obj){
  unsigned long pfooter =(*((unsigned long*)(((unsigned char*)obj)+DBO::getTotalLen(obj)-sizeof(unsigned long))));
  dbg_assert(!(pfooter&0x7));
  return pfooter;
}
unsigned char* DBO::getKey(db_object* obj, int key_num){

  int offset=0;
  if(key_num){
    offset = DBO::getKeyLoc(obj, key_num-1);
  }
  return ((unsigned char*)obj)+DBO::getPaddingLen(obj) + offset;
  //  return obj->free_or_alloc.alloc.key;
}
unsigned char* DBO::getVal(db_object* obj, int val_num){

  int offset=0;
  if(val_num){
    offset = DBO::getValLoc(obj, val_num-1);
  }
  return  (((unsigned char*)obj)+DBO::getPaddingLen(obj) + DBO::getKeyLoc(obj, DBO::getNKeys(obj)-1) + offset);
}
uint DBO::getNextFree(db_object* obj){
  return obj->free_or_alloc.free.n_free;
}
uint DBO::getPrevFree(db_object* obj){
  return obj->free_or_alloc.free.p_free;
}

unsigned char DBO::getDel(db_object* obj){
  return (unsigned char)(obj->total_len&del_bit);
}

unsigned char DBO::getIsPrevFree(db_object* obj){
  return (unsigned char)(!!(obj->total_len&prev_del_bit));
}

uint DBO::getSubtable(db_object* obj, int key_num){
  return (unsigned char)((obj->key_loc[key_num]&key_subtable_mask)>>key_subtable_shift);
}

unsigned char DBO::getKeyType(db_object* obj, int key_num){
  return (unsigned char)(obj->key_loc[key_num]&key_type_mask);
}

uint __attribute__ ((noinline)) DBO::getKeyLoc(db_object* obj, int key_num){
  //  dbg_assert((obj->key_loc[key_num]>>(byte_shift)) == (32<<key_num));
  return obj->key_loc[key_num]>>(byte_shift);
}

unsigned char DBO::getValType(db_object* obj, int val_num){
  return DBO::getKeyType(obj, val_num);
}

uint DBO::getValLoc(db_object* obj,int val_num){
  uint ret =  *((uint*)(((char*)obj)+block_data_size + (DBO::getNKeys(obj)+val_num)*sizeof(uint)));
  //    dbg_assert(ret == (32<<val_num));
  return ret;
}

uint DBO::getAllDataLen(db_object* obj){
  return DBO::getValLoc(obj, DBO::getNKeys(obj)-1) +DBO::getKeyLoc(obj, DBO::getNKeys(obj)-1);
}

uint DBO::getDataLen(db_object* obj, int key_num){
  return DBO::getValLen(obj, key_num) + DBO::getKeyLen(obj, key_num);
}

uint DBO::getPaddingLen(db_object* obj){
  return block_data_size + (DBO::getNKeys(obj)<<1)*sizeof(uint);
}

uint DBO::getTotalLen(db_object* obj){

  uint ret = obj->total_len&total_len_mask;
  if(DBO::getDel(obj)==ACTIVE){
    dbg_assert(ret > DBO::getAllDataLen(obj));
  }
  dbg_assert(!(ret&0xf));
  return ret;
}


//setters


void DBO::setNKeys(db_object* obj, uint val){
  dbg_assert(val< MAX_UNSIGNED_CHAR);
  uint count_val = DBO::getCounter(obj);
  obj->obj_count = (count_val<<(byte_shift)) | val;
}

void DBO::setCounter(db_object* obj, uint val){

  unsigned char nkey = obj->obj_count&byte_mask;
  obj->obj_count = val<<(byte_shift);
  obj->obj_count |= nkey;

}


void DBO::setNextFree(db_object* obj, uint val){
  obj->free_or_alloc.free.n_free=val;
}
void DBO::setPrevFree(db_object* obj, uint val){
  obj->free_or_alloc.free.p_free=val;
}

void DBO::setDel(db_object* obj, unsigned char val){
  val = !!val;
  obj->total_len = (obj->total_len&(~del_bit)) | (val);
}

void DBO::setIsPrevFree(db_object* obj, unsigned char val){
  val = !!val;
  obj->total_len = (obj->total_len&(~prev_del_bit)) | (val<<1);

}

uint DBO::setSubtable(db_object* obj, uint val, int key_num){
  uint key_data = obj->key_loc[key_num]&(~key_subtable_mask);
  obj->key_loc[key_num] = (key_data) | (val<<key_subtable_shift);
}

void DBO::setKeyType(db_object* obj, unsigned char val, int key_num){
  uint key_data = obj->key_loc[key_num]&(~key_type_mask);
  obj->key_loc[key_num] = (key_data) | val;

}

void DBO::setKeyLoc(db_object* obj, uint val, int key_num){
  unsigned char metaData = obj->key_loc[key_num]&(byte_mask);
  obj->key_loc[key_num] = val<<(byte_shift);
  obj->key_loc[key_num] |= metaData;
}

void DBO::setValType(db_object* obj, unsigned char val, int val_num){
  DBO::setKeyType(obj, val, val_num);
}

void DBO::setValLoc(db_object* obj, uint val, int val_num){
  *((uint*)(((char*)obj)+block_data_size + (DBO::getNKeys(obj)+val_num)*sizeof(uint)))= val;
  //  obj->val_loc[val_num]=val;
}
void DBO::setTotalLen(db_object* obj, uint val){
  dbg_assert(!(val&0x7));
  int old_bits = obj->total_len & all_bits_mask;
  obj->total_len=val;
  obj->total_len|=old_bits;
  dbg_assert(DBO::getDel(obj) == (old_bits & del_bit));
}

void DBO::setFooter(db_object* obj, unsigned long val){
  dbg_assert(!(val&0x7));
  memcpy((((unsigned char*)obj)+DBO::getTotalLen(obj))-sizeof(unsigned long), &val, sizeof(unsigned long));
}

db_object* DBO::createObject(int num_keys,
			     uint* key_len, uint* val_len, 
			     unsigned char* key_type,
			     unsigned char** key, unsigned char** val){
  uint key_scanned[num_keys];
  uint val_scanned[num_keys];
  dbg_printf("Keys[%d -> %zd==%zd==%zd\n", num_keys, num_keys*sizeof(uint), sizeof(key_scanned), sizeof(val_scanned));

  HLP::byteCopy((unsigned char*)key_scanned, (unsigned char*)key_len, num_keys*sizeof(uint));
  HLP::byteCopy((unsigned char*)val_scanned, (unsigned char*)val_len, num_keys*sizeof(uint));
  HLP::scan(key_scanned, num_keys);
  HLP::scan(val_scanned, num_keys);
  for(int i=0;i<num_keys;i++){
    dbg_printf("[%d] key: %d, scan: %d\n", i, key_len[i], key_scanned[i]);
    dbg_printf("[%d] val: %d, scan: %d\n", i, val_len[i], val_scanned[i]);

  }
  
  int obj_size = key_scanned[num_keys-1]+val_scanned[num_keys-1]+block_data_size + (num_keys<<1)*sizeof(uint);
  obj_size = HLP::round(obj_size, aligned);
  dbg_printf("Obj Size: %d (padding size: %zd)\n", obj_size,block_data_size + (num_keys<<1)*sizeof(uint));
  db_object* new_obj = (db_object*)calloc(obj_size, sizeof(unsigned char));
  DBO::setNKeys(new_obj, num_keys);

  for(volatile int i=0;i<num_keys;i++){
    dbg_printf("setting vars: %d\n", i);
    DBO::setKeyLoc(new_obj, key_scanned[i], i);
    DBO::setValLoc(new_obj, val_scanned[i], i);
    DBO::setKeyType(new_obj, key_type[i], i);
    dbg_assert(key_scanned[i] < key_max);
  }


  
    for(volatile int i=0;i<num_keys;i++){
      dbg_printf( "%d: doing key[%u==%u <= %u==%u] (prev[%d]: %u)!\n",i, DBO::getKeyLen(new_obj, i), key_len[i], key_scanned[i], DBO::getKeyLoc(new_obj, i), (i-1)+(i==0),DBO::getKeyLoc(new_obj, (i-1)+(i==0)));
    HLP::byteCopy(DBO::getKey(new_obj, i), key[i], DBO::getKeyLen(new_obj, i));
    dbg_printf( "%d: doing val[%u==%u <= %u==%u]!\n",i, DBO::getValLen(new_obj, i), val_len[i], val_scanned[i], DBO::getValLoc(new_obj, i));

    HLP::byteCopy(DBO::getVal(new_obj, i), val[i], DBO::getValLen(new_obj, i));
  }
  DBO::setTotalLen(new_obj, HLP::round(obj_size, aligned));
  DBO::setDel(new_obj, ACTIVE);
  DBO::setIsPrevFree(new_obj, PREV_ACTIVE);
  DBO::setCounter(new_obj, 0);

  #ifdef dbg_a
  for(volatile int i=0;i<num_keys;i++){
    dbg_printf("ComparingKey[%d]: %d vs %d\n", i, key_scanned[i], DBO::getKeyLoc(new_obj, i));
    dbg_assert(key_scanned[i] == DBO::getKeyLoc(new_obj, i));
    dbg_printf("ComparingVal[%d]: %d vs %d\n", i, val_scanned[i], DBO::getValLoc(new_obj, i));
    dbg_assert(val_scanned[i] == DBO::getValLoc(new_obj, i));
    dbg_assert(!memcmp(key[i], DBO::getKey(new_obj, i), DBO::getKeyLen(new_obj,i)));
    dbg_assert(!memcmp(val[i], DBO::getVal(new_obj, i), DBO::getValLen(new_obj,i)));
    dbg_assert(HLP::byteCompare(key[i], DBO::getKey(new_obj, i), DBO::getKeyLen(new_obj,i)));
    dbg_assert(HLP::byteCompare(val[i], DBO::getVal(new_obj, i), DBO::getValLen(new_obj,i)));
  }
  //  DBO::toString(new_obj);
  #endif

  return new_obj;
}
uint DBO::compareObjects(db_object* a, db_object* b){
  int num_keys = DBO::getNKeys(a);

  if(num_keys != DBO::getNKeys(b)){
    return 0;
  }
  dbg_printf("Comparing Num Keys: %d\n", num_keys);
  for(int i=0;i<num_keys;i++){
    if(DBO::getKeyLoc(a, i)!=DBO::getKeyLoc(b, i)){
      dbg_printf("Keys loc[%d] not equal: %u vs %u!\n", i, DBO::getKeyLoc(a, i),DBO::getKeyLoc(b, i));
      return 0;
    }
    if(DBO::getValLoc(a, i)!=DBO::getValLoc(b, i)){
      dbg_printf("Vals loc[%d] not equal: %u vs %u!\n", i, DBO::getValLoc(a, i),DBO::getValLoc(b, i));
      return 0;
    }
  }
  if(!HLP::byteCompare(DBO::getKey(a, 0), DBO::getKey(b,0), DBO::getKeyLoc(a, num_keys-1))){
    dbg_printf("Keys not equal!\n");
    return 0;
  }
  if(!HLP::byteCompare(DBO::getVal(a, 0), DBO::getVal(b,0), DBO::getValLoc(a, num_keys-1))){
    dbg_printf("Vals not equal!\n");
    return 0;
  }
  return 1;
}

uint DBO::compareAll(int num_keys,
		     int* key_slots,
		     uint* key_len, uint* val_len, 
		     unsigned char** key, unsigned char** val,
		     db_object* obj){

  if(key_slots){
    for(int i=0;i<num_keys;i++){
      if(!DBO::compareFields(key_len[key_slots[i]], val_len[key_slots[i]],
			    key[key_slots[i]], val[key_slots[i]],
			    DBO::getKeyLen(obj, key_slots[i]),
			    DBO::getValLen(obj, key_slots[i]),
			    DBO::getKey(obj, key_slots[i]),
			    DBO::getVal(obj, key_slots[i]))){
	return 0;
      }
    }
    
  }
  else{
    for(int i=0;i<num_keys;i++){
      if(!DBO::compareFields(key_len[i], val_len[i],
			     key[i], val[i],
			     DBO::getKeyLen(obj, i),
			     DBO::getValLen(obj, i),
			     DBO::getKey(obj, i),
			     DBO::getVal(obj, i))){
	return 0;
      }
    }
  }
  return 1;
}
uint DBO::compareFields(uint a_key_len, uint a_val_len, 
			unsigned char* a_key, unsigned char* a_val,
			uint b_key_len, uint b_val_len, 
			unsigned char* b_key, unsigned char* b_val){
  if(a_key_len != b_key_len){
    dbg_printf("Key Lens not equals %u vs %u\n", a_key_len, b_key_len);
    return 0;
  }
  if(a_val_len != b_val_len){
    dbg_printf("Val Lens not equals %u vs %u\n", a_val_len, b_val_len);
    return 0;
  }
  if(!HLP::byteCompare(a_key,b_key, a_key_len)){
    dbg_printf("Keys not equals %s vs %s\n", a_key, b_key);
    return 0;
  }
  dbg_printf("Vals test last %s vs %s\n", a_val, b_val);
  return HLP::byteCompare(a_val, b_val, a_val_len);
}

void DBO::toString(db_object* obj){
  printf("db_object {\n");
  printf("total_len: %u\n", DBO::getTotalLen(obj));
  printf("key_locs[%d]: { %u ", DBO::getNKeys(obj),DBO::getKeyLoc(obj,0));
  for(int i=1;i<DBO::getNKeys(obj);i++){
    printf("%u ", DBO::getKeyLoc(obj, i));
  }
  printf("}\n");
  printf("val_locs[%d]: { %u ", DBO::getNKeys(obj),DBO::getValLoc(obj,0));
  for(int i=1;i<DBO::getNKeys(obj);i++){
    printf("%u ", DBO::getValLoc(obj, i));
  }
  printf("}\n");
  printf("key_lens[%d]: { %u ", DBO::getNKeys(obj),DBO::getKeyLen(obj,0));
  for(int i=1;i<DBO::getNKeys(obj);i++){
    printf("%u ", DBO::getKeyLen(obj, i));
  }
  printf("}\n");

  printf("val_lens[%d]: { %u ", DBO::getNKeys(obj),DBO::getValLen(obj,0));
  for(int i=1;i<DBO::getNKeys(obj);i++){
    printf("%u ", DBO::getValLen(obj, i));
  }
  printf("}\n");
  printf("data_len: %u\n", DBO::getDataLen(obj,0));
  printf("all_data_len: %u\n", DBO::getAllDataLen(obj));
  printf("pred_len: %u\n", DBO::getPaddingLen(obj));
  printf("key_type: %c\n", DBO::getKeyType(obj,0));
  printf("val_type: %c\n", DBO::getValType(obj,0));
  printf("is_del: %d\n", (int)DBO::getDel(obj));
  printf("prev_free: %c\n", DBO::getIsPrevFree(obj));
  if(DBO::getDel(obj)==ACTIVE&&
     DBO::getKeyLoc(obj, DBO::getNKeys(obj)-1)<MAX_PRINT&&
    DBO::getValLoc(obj, DBO::getNKeys(obj)-1)<MAX_PRINT){
    printf("(K,V)[%d]: {\n", DBO::getNKeys(obj));
    for(int i=0;i<DBO::getNKeys(obj);i++){
      printf("Key[%d] ->[%p -> %s]\n",i, DBO::getKey(obj, i), DBO::getKey(obj, i));
      printf("Val[%d] ->[%p -> %s]\n",i,  DBO::getVal(obj, i), DBO::getVal(obj, i));
      printf("\n");
    }
    printf("\t}\n");


  }
  else{
    printf("next_free: %d\n", DBO::getNextFree(obj));
    printf("prev_free: %d\n", DBO::getPrevFree(obj));
    printf("footer: %lu\n", DBO::getFooter(obj));
  }
  printf("\t}\n");
}



  //dbg_printf("[Create Obj]: size: %u==%u, nfields: %u==%u, loc==len(k,v): %u==%u and %u==%u, data_size: %u==%u/%u==(%u+%u == %u)\nKey: %s\nVal: %s\n", obj_size, DBO::getTotalLen(new_obj), num_keys,DBO::getNKeys(new_obj), DBO::getKeyLoc(new_obj, 0), DBO::getKeyLen(new_obj, 0), DBO::getValLoc(new_obj, 0), DBO::getValLen(new_obj, 0), DBO::getDataLen(new_obj, 0), DBO::getAllDataLen(new_obj), DBO::getTotalLen(new_obj), DBO::getPaddingLen(new_obj), DBO::getAllDataLen(new_obj),DBO::getPaddingLen(new_obj)+DBO::getAllDataLen(new_obj), DBO::getKey(new_obj, 0), DBO::getVal(new_obj, 0));
