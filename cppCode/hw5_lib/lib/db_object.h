#pragma once

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <errno.h>
#include "helpers.h"
#include "hash.h"


#define NIL 0

#define FREE 0
#define ACTIVE 1

#define PREV_FREE 0
#define PREV_ACTIVE 1


#define aligned 16
#define MAX_UNSIGNED_CHAR 0xff
#define total_len_mask (~0xf)
#define all_bits_mask 0xf
#define del_bit 1
#define prev_del_bit 2
#define key_type_bit 4
#define val_type_bit 8

#define MAX_PRINT 0xff


typedef struct db_object{
  uint total_len;
  uint obj_count;
  uint key_loc[1]; //stores end loc of each obj
  uint val_loc[1]; //stores end loc of each obj

  union free_or_alloc {
    struct alloc{
      unsigned char key[0];
      unsigned char val[0];
    } alloc;
    struct free{
      uint n_free;
      uint p_free;
      //this is just to gurantee wont write over
      //next/prev with foot (this is why 16 byte min obj size aswell)
      unsigned long footer;
    } free;
  } free_or_alloc;
}db_object;

typedef struct obj_node{
  db_object* obj;
  struct obj_node* next;
}obj_node;


#define key_max  (1<<24)-1
#define byte_mask 0xff
#define byte_shift 8
#define key_type_mask 0xf
#define key_subtable_mask 0xf0
#define key_subtable_shift 4

#define block_data_size 8
#define meta_data_size 16
#define min_block_size 32


namespace DBO{
  obj_node* newObjNode(db_object* obj);
  obj_node* addNode(obj_node* list, obj_node* new_node);
  void freeList(obj_node* list);
  
  

  
  uint getSubtable(db_object* obj, int key_num);
  uint getPaddingLen(db_object* obj);
  uint getDataLen(db_object* obj, int key_num);
  uint getAllDataLen(db_object* obj);
  
  uint getKeyLen(db_object* obj, int key_num);
    
  uint getValLen(db_object* obj, int key_num);
  uint getNKeys(db_object* obj);
  unsigned long getFooter(db_object* obj);
  uint getCounter(db_object* obj);
  uint getNextFree(db_object* obj);
  uint getPrevFree(db_object* obj);
  unsigned char getDel(db_object* obj);
  unsigned char getIsPrevFree(db_object* obj);
  unsigned char getKeyType(db_object* obj, int key_num);
  uint getKeyLoc(db_object* obj, int key_num);
  unsigned char getValType(db_object* obj, int key_num);
  uint getValLoc(db_object* obj, int key_num);
  uint getTotalLen(db_object* obj);
  unsigned char* getKey(db_object* obj, int key_num);
  unsigned char* getVal(db_object* val, int key_num);


  uint setSubtable(db_object* obj, uint val, int key_num);
  void setNKeys(db_object* obj, uint val);
  void setCounter(db_object* obj, uint val);
  void setFooter(db_object* obj, unsigned long val);
  void setNextFree(db_object* obj, uint val);
  void setPrevFree(db_object* obj, uint val);
  void setDel(db_object* obj, unsigned char val);
  void setIsPrevFree(db_object* obj, unsigned char val);
  void setKeyType(db_object* obj, unsigned char val, int key_num);
  void setKeyLoc(db_object* obj, uint val, int key_num);
  void setValType(db_object* obj, unsigned char val, int key_num);
  void setValLoc(db_object* obj, uint val, int key_num);
  void setTotalLen(db_object* obj, uint val);
  db_object* createObject(int num_keys,
			  uint* key_len, uint* val_len,
			  unsigned char* key_type,
			  unsigned char** key, unsigned char** val);

  //todo
  uint compareObjects(db_object* a, db_object* b);
  uint compareAll(int num_keys,
		  int* key_slots,
		  uint* key_len, uint* val_len, 
		  unsigned char** key, unsigned char** val,
		  db_object* b);
  
  uint compareFields(uint a_key_len, uint a_val_len, 
		    unsigned char* a_key, unsigned char* a_val,
		    uint b_key_len, uint b_val_len, 
		    unsigned char* b_key, unsigned char* b_val);
  void toString(db_object* obj);
  
};
