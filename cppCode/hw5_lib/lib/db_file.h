#pragma once

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <unistd.h>
#include <assert.h>
#include <errno.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include "db_object.h"



extern int verbose;

//#define first_hit

#define seg_list_log 5
//basically the larger the range of items, the
//larger this should be
#define seg_list_size 8
#define max_subtables 32
#define name_size 16
#define min_object_size 16
#define INT_MASK 0xffffffff
#define TAG_MASK 0x7fffffff
#define NO_CHANGE 0
#define CHANGE 1
#define SLOT_TAKEN 1
#define init_file_size (4096<<10)
#define AT_SUBTABLE ((db_object*)(-1UL))

typedef unsigned int uint;
typedef struct db_file{
  //will enforce 8 byte alignment here
  uint subtable_starts[max_subtables];
  uint first_free[seg_list_size]; //3
  uint last_free[seg_list_size]; //3
  uint num_subtables; //1
  uint init_size; //2
  uint is_last_free; //4
  uint retries; //5
  //each char in this int will store a seperate seed
  uint seeds; //6
  uint file_end; //7
  uint file_size;

  int fd;
  unsigned char* region;
}db_file;


#define tag_seed 3
#define int_fields 7
#define recover_fields_val ((int)((max_subtables+int_fields+(seg_list_size<<1))*sizeof(uint)))
#define recover_fields ((((recover_fields_val)+aligned-1)/aligned)*aligned)

typedef unsigned long slot;
//0-1 bit is bool of object taken or not
//1-31 is objects tag
//32-63 is objects location

#define combine_hash(X, Y) (((X)*17)*((Y)))

namespace DBF{
  uint hashBytes(uint seed, uint b_len, uint8_t* bytes);
  //gets seed for a given hash try
  char getSeedN(db_file* dbf, int seed_num);
  //returns if slot is free
  uint isSlotFree(slot s);
  //gets tag for a slot
  uint getSlotTag(slot s);
  //get object location of a slot
  uint getObjLoc(slot s);

  slot* getSlot(db_file* dbf, uint slot_num, uint tnum);
  //adds a subtable to the list
  int addSubTable(db_file* dbf, uint tnum);
  //gets location in file of a subtable
  uint getSlotLoc(db_file* dbf, uint tnum, uint slot_num);
  //get slot for hash_val in a given slot
  uint getSlotIndex(uint hash_val, uint init_size, uint tnum);


  void setFooterFile(db_file* dbf, db_object* obj, uint obj_loc);
  

  unsigned long getFooter(db_file* dbf, uint loc);


  //coalesce two free blocks
  void coalesce(db_file* dbf, db_object* obj, uint obj_loc);

  //free a block
  void freeBlock(db_file* dbf, db_object* obj, uint obj_loc);

  //get an object from file
  db_object* getObject(db_file* dbf, uint obj_loc);

  //get next adjacent block from file
  db_object* getNextObject(db_file* dbf, db_object* obj, uint obj_loc);

  //get prev adjacent block from file
  db_object* getPrevObject(db_file* dbf, db_object* obj, uint obj_loc);

  //add/remove from freelist
  void addToList(db_file* dbf, db_object* obj, uint obj_loc);
  void removeFromList(db_file* dbf, db_object* obj);

  //finds a free block to place
  uint findFreeFit(db_file* dbf, uint obj_size);
  
  //break a block apart
  void place(db_file* dbf, db_object* old_obj, uint old_loc, db_object* new_obj);

  obj_node* findObject(db_file* dbf,
			uint num_keys,
			int* key_slots,
			uint* key_len, uint* val_len,
			unsigned char** key, unsigned char** val);

  uint addObject(db_file* dbf, db_object* new_obj);
  uint delObject(db_file* dbf,
		 int num_keys,
		 uint* key_len, uint* val_len,
		 unsigned char** key, unsigned char** val);



  //write/read from a position with error checking
  void writeWrapper(unsigned char* src, uint nbytes, db_file* dbf, uint pos);
  void extendFile(db_file* dbf);

  //initializes a new file
  db_file* initDBF(uint isize, uint retries, char* file_name, char* db_name);
  void cleanupDBF(db_file* dbf);
  
  //recover file struct from file
  db_file* recoverFile(int fd);

  uint hashObject(db_file* dbf, db_object* obj, uint seed_num, int key_num);
  void toString(db_file* dbf);
  int dbg_is_in_list(db_file* dbf, int loc);
  uint calculateI (uint size);
  uint hob(uint i);

  //todo
  void setRemainingSlots(db_file* dbf, db_object* obj, uint obj_loc, int num_keys);
  void unsetRemainingSlots(db_file* dbf, db_object* obj, uint obj_loc, int num_keys, int ignore_key);
  uint setObjInSlot(db_file* dbf, db_object* new_obj, int new_obj_size, int subtable_num, int slot_num, uint tag);
};
