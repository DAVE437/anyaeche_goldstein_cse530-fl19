//standard includes 
#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <sched.h>
#include <stdarg.h>
#include <errno.h>
#include <unistd.h>
#include <stdbool.h>
#include <assert.h>
#include <fstream>

//helper includes
#include "../utils/arg.h"




#include "../lib/db_file.h"
#include "../lib/db.h"
#include "../lib/helpers.h"
#include "../lib/db_object.h"


//arguments
int verbose=0;
char* dbPath=NULL;
char* tName=NULL;
int echo=0;
int newt=0;
int reset = 0;
int unique =1;
int tests = 15;
int extra = 0;
char* trace=NULL;
int max_nkeys=1;
int min_key = 0, max_key= 0, min_val=0, max_val=0;
#define Version "0.1"
#define isize (1<<20)
#define max_number_keys 31
#define perm_num 10
#define retries 3

static ArgOption args[] = {
  // Kind, 	  Method,	name,	        reqd,  variable,	 help
  { KindOption,   Integer,	"-v", 	        0,     &verbose, 	 "verbose mode" },
  { KindOption,   Set, 		"--echo",       0,     &echo,            "echo args" },
  { KindOption,   Integer,      "--ntests",     0,     &tests,      	 "give as power of 2" },
  { KindOption,   Set,          "--reset",      0,     &reset,      	 "reset db in question"},
  { KindOption,   Set,          "--extra",      0,     &extra,      	 "extra db in question"},
  { KindOption,   Integer,      "--minkey",     0,     &min_key,      	 "give as power of 2" },
  { KindOption,   Integer,      "--maxkey",     0,     &max_key,      	 "give as power of 2" },
  { KindOption,   Integer,      "--minval",     0,     &min_val,      	 "give as power of 2" },
  { KindOption,   Integer,      "--maxval",     0,     &max_val,      	 "give as power of 2" },
  { KindOption,   Integer,      "--nkeys",     0,      &max_nkeys,      	 "max_num_keys per object" },
  { KindOption,   String,      "--db",          0,     &dbPath,          "Dir that will actually server as the database" },
  { KindOption,   String,      "--table",       0,     &tName,           "Name of the table to use" },
  { KindHelp,     Help, 	"-h" },
  { KindEnd }
};
static ArgDefs argp = { args, "Args Processor", Version, NULL };


#define MAX_KEY_SIZE (1<<6)
#define MAX_VAL_SIZE (1<<10)
#define MIN_VAL_SIZE (1<<3)
#define MIN_KEY_SIZE (1<<3)
void genRandStr(unsigned char* loc, int len){
  for(int i=0;i<len;i++){
    loc[i]=(rand()%26) + 65;
  }
}
void printSN(unsigned long val, char* pout){

  int p = 0;
  unsigned long temp = val;
  double div = 1;
  while(val/10){
    val=val/10;
    div=div*10;
    p++;
  }
  double f = temp/div;
  sprintf(pout,"%.2lf*10^%d", f, p);
}
void randomizeOrder(int* order, int len){
  for(int i=0;i<(len);i++){
    int loc_1 = rand()%len;
    int loc_2 = rand()%len;
    int temp = order[loc_1];
    order[loc_1]=order[loc_2];
    order[loc_2]=temp;
  }
}
void fullTest(int test_len){
  unsigned char types[max_number_keys] = {0};
  int test_keys[max_number_keys];
  int key_perms[max_number_keys];
  for(int i=0;i<max_number_keys;i++){
    test_keys[i] = i;
    key_perms[i] = i;
  }
  int alloc_len = test_len<<1;
  unsigned char*** keys = (unsigned char***)calloc(alloc_len, sizeof(unsigned char**));
  unsigned char*** vals = (unsigned char***)calloc(alloc_len, sizeof(unsigned char**));

  uint** key_lens = (uint**)calloc(alloc_len, sizeof(uint*));
  uint** val_lens = (uint**)calloc(alloc_len, sizeof(uint*));
  int* nkeys=(int*)calloc(alloc_len, sizeof(uint));
  int* order = (int*)calloc(alloc_len, sizeof(int));
  unsigned long alloc1 = 0, alloc2=0;
  fprintf(stderr,"Testing with %d Objects (%d At a Given Time)\n", alloc_len, test_len);
  fprintf(stderr,"Setup\n");
  int comp =0;
  int marker = alloc_len/100;
  for(int i=0;i<alloc_len;i++){
    if(i>comp*marker){
      fprintf(stderr,"\r%d%% Complete", comp);
      comp++;
    }
    nkeys[i]=rand()%max_nkeys;
    nkeys[i]+=(nkeys[i]==0);
    key_lens[i] = (uint*)calloc(nkeys[i], sizeof(uint));
    val_lens[i] = (uint*)calloc(nkeys[i], sizeof(uint));
    keys[i] = (unsigned char**)calloc(nkeys[i], sizeof(unsigned char*));
    vals[i] = (unsigned char**)calloc(nkeys[i], sizeof(unsigned char*));
    order[i] = i;
    for(int j=0;j<nkeys[i];j++){
      key_lens[i][j]=(rand()%max_key)+min_key;
      val_lens[i][j] = (rand()%max_val)+min_val;
      if(i<test_len){
	alloc1 += key_lens[i][j];
	alloc1 += val_lens[i][j];
      }else{
	alloc2 += key_lens[i][j];
	alloc2 += val_lens[i][j];
      }
      keys[i][j] = (unsigned char*)calloc(key_lens[i][j], sizeof(unsigned char));
      vals[i][j] = (unsigned char*)calloc(val_lens[i][j], sizeof(unsigned char));


      if(unique){
	genRandStr(vals[i][j]+sizeof(uint), val_lens[i][j]-(1+sizeof(uint)));
	memcpy(vals[i][j], &i, sizeof(uint));
      }
      else{
	genRandStr(vals[i][j], val_lens[i][j]-1);
      }
      genRandStr(keys[i][j], key_lens[i][j]-1);
      
    }
  }
  
  DB::initDB(dbPath);
  db_file* dbf = DBF::initDBF(isize, retries, tName, dbPath);

  if(verbose>1){
    DBF::toString(dbf);
  }

  char pout1[32]="", pout2[32]="";
  printSN(max(alloc1, alloc2), pout2);
  printSN(alloc1 + alloc2, pout1);
  fprintf(stderr,"\r%d%% Complete\n\nTest Will Contain %s Bytes (%s At a Given Time)\n\n", 100, pout1, pout2);


  fprintf(stderr,"Testing Insert and Delete\n");
  comp=0;
  marker = test_len/100;
  for(int i=0;i<test_len;i++){
    if(i>comp*marker){
      fprintf(stderr,"\r%d%% Complete", comp);
      comp++;
    }
    db_object* obj = DBO::createObject(nkeys[order[i]],
				       key_lens[order[i]],
				       val_lens[order[i]],
				       (unsigned char*)types,
				       keys[order[i]],
				       vals[order[i]]);
    DBF::addObject(dbf, obj);

    assert(DBF::findObject(dbf,
			   nkeys[order[i]],
			   (int*)test_keys,
			   key_lens[order[i]],
			   val_lens[order[i]],
			   keys[order[i]],
			   vals[order[i]])!=NULL);
    if(extra){
      for(int t=0;t<nkeys[order[i]];t++){

	int tkeys = 1;
	key_perms[0] = t;
	assert(DBF::findObject(dbf,
			       tkeys,
			       (int*)key_perms,
			       key_lens[order[i]],
			       val_lens[order[i]],
			       keys[order[i]],
			       vals[order[i]])!=NULL);

      }
      key_perms[0] = 0;
      for(int t=0;t<perm_num;t++){
	int tkeys= rand()%nkeys[order[i]];
	tkeys+=(tkeys==0);
	randomizeOrder(key_perms, tkeys);
	assert(DBF::findObject(dbf,
			       tkeys,
			       (int*)key_perms,
			       key_lens[order[i]],
			       val_lens[order[i]],
			       keys[order[i]],
			       vals[order[i]])!=NULL);

      }
      for(int t=0;t<nkeys[order[i]];t++){
	key_perms[t]=t;
      }
    }
      
  

        assert(DBF::delObject(dbf,
			      nkeys[order[i]],
			      key_lens[order[i]],
			      val_lens[order[i]],
			      keys[order[i]],
			      vals[order[i]]));
        assert(DBF::findObject(dbf,
			   nkeys[order[i]],
			   (int*)test_keys,
			   key_lens[order[i]],
			   val_lens[order[i]],
			   keys[order[i]],
			   vals[order[i]])==NULL);

  }

  fprintf(stderr,"\r%d%% Complete\n\n", 100);
  randomizeOrder(order, test_len);
  DBF::cleanupDBF(dbf);
  dbf = DBF::initDBF(isize, retries, tName, dbPath);

  
  fprintf(stderr,"Testing Insert\n");
  comp=0;
  marker = test_len/100;
  for(int i=0;i<test_len;i++){
    if(i>comp*marker){
      fprintf(stderr,"\r%d%% Complete", comp);
      comp++;
    }
    db_object* obj = DBO::createObject(nkeys[order[i]],
				       key_lens[order[i]],
				       val_lens[order[i]],
				       types,
				       keys[order[i]],
				       vals[order[i]]);
    DBF::addObject(dbf, obj);
    assert(DBF::findObject(dbf,
			   nkeys[order[i]],
			   (int*)test_keys,
			   key_lens[order[i]],
			   val_lens[order[i]],
			   keys[order[i]],
			   vals[order[i]])!=NULL);
  

      if(extra){
      for(int t=0;t<nkeys[order[i]];t++){

	int tkeys = 1;
	key_perms[0] = t;
	assert(DBF::findObject(dbf,
			       tkeys,
			       (int*)key_perms,
			       key_lens[order[i]],
			       val_lens[order[i]],
			       keys[order[i]],
			       vals[order[i]])!=NULL);

      }
      key_perms[0] = 0;
      for(int t=0;t<perm_num;t++){
	int tkeys= rand()%nkeys[order[i]];
	tkeys+=(tkeys==0);
	randomizeOrder(key_perms, tkeys);
	assert(DBF::findObject(dbf,
			       tkeys,
			       (int*)key_perms,
			       key_lens[order[i]],
			       val_lens[order[i]],
			       keys[order[i]],
			       vals[order[i]])!=NULL);

      }
      for(int t=0;t<nkeys[order[i]];t++){
	key_perms[t]=t;
      }
    }
  }

  fprintf(stderr,"\r%d%% Complete\n\n", 100);
  randomizeOrder(order, test_len);
  DBF::cleanupDBF(dbf);
  dbf = DBF::initDBF(isize, retries, tName, dbPath);
  fprintf(stderr,"Testing Delete\n");
  comp =0;
  for(int i=0;i<test_len;i++){
    if(i>comp*marker){
      fprintf(stderr,"\r%d%% Complete", comp);
      comp++;
    }
    if(unique){
      assert(DBF::findObject(dbf,
			     nkeys[order[i+test_len]],
			     (int*)test_keys,
			     key_lens[order[i+test_len]],
			     val_lens[order[i+test_len]],
			     keys[order[i+test_len]],
			     vals[order[i+test_len]])==NULL);
    }
    
    assert(DBF::findObject(dbf,
			   nkeys[order[i]],
			   (int*)test_keys,
			   key_lens[order[i]],
			   val_lens[order[i]],
			   keys[order[i]],
			   vals[order[i]])!=NULL);



        if(extra){
      for(int t=0;t<nkeys[order[i]];t++){

	int tkeys = 1;
	key_perms[0] = t;
	assert(DBF::findObject(dbf,
			       tkeys,
			       (int*)key_perms,
			       key_lens[order[i]],
			       val_lens[order[i]],
			       keys[order[i]],
			       vals[order[i]])!=NULL);

      }
      key_perms[0] = 0;
      for(int t=0;t<perm_num;t++){
	int tkeys= rand()%nkeys[order[i]];
	tkeys+=(tkeys==0);
	randomizeOrder(key_perms, tkeys);
	assert(DBF::findObject(dbf,
			       tkeys,
			       (int*)key_perms,
			       key_lens[order[i]],
			       val_lens[order[i]],
			       keys[order[i]],
			       vals[order[i]])!=NULL);

      }
      for(int t=0;t<nkeys[order[i]];t++){
	key_perms[t]=t;
      }
    }

    assert(DBF::delObject(dbf,
			  nkeys[order[i]],
			  key_lens[order[i]],
			  val_lens[order[i]],
			  keys[order[i]],
			  vals[order[i]]));
    if(unique){
      assert(DBF::findObject(dbf,
			     nkeys[order[i]],
			     (int*)test_keys,
			     key_lens[order[i]],
			     val_lens[order[i]],
			     keys[order[i]],
			     vals[order[i]])==NULL);
    }
  }
  fprintf(stderr,"\r%d%% Complete\n\n", 100);
  randomizeOrder(order, test_len);
  DBF::cleanupDBF(dbf);
  dbf = DBF::initDBF(isize, retries, tName, dbPath);
  fprintf(stderr,"Testing Re-Insert\n");
  comp=0;
  marker = test_len/100;
  for(int i=0;i<test_len;i++){
    if(i>comp*marker){
      fprintf(stderr,"\r%d%% Complete", comp);
      comp++;
    }
        db_object* obj = DBO::createObject(nkeys[order[i]],
				       key_lens[order[i]],
				       val_lens[order[i]],
				       (unsigned char*)types,
				       keys[order[i]],
				       vals[order[i]]);

    DBF::addObject(dbf, obj);
    assert(DBF::findObject(dbf,
			   nkeys[order[i]],
			   (int*)test_keys,
			   key_lens[order[i]],
			   val_lens[order[i]],
			   keys[order[i]],
			   vals[order[i]])!=NULL);
        if(extra){
      for(int t=0;t<nkeys[order[i]];t++){

	int tkeys = 1;
	key_perms[0] = t;
	assert(DBF::findObject(dbf,
			       tkeys,
			       (int*)key_perms,
			       key_lens[order[i]],
			       val_lens[order[i]],
			       keys[order[i]],
			       vals[order[i]])!=NULL);

      }
      key_perms[0] = 0;
      for(int t=0;t<perm_num;t++){
	int tkeys= rand()%nkeys[order[i]];
	tkeys+=(tkeys==0);
	randomizeOrder(key_perms, tkeys);
	assert(DBF::findObject(dbf,
			       tkeys,
			       (int*)key_perms,
			       key_lens[order[i]],
			       val_lens[order[i]],
			       keys[order[i]],
			       vals[order[i]])!=NULL);

      }
      for(int t=0;t<nkeys[order[i]];t++){
	key_perms[t]=t;
      }
    }

  }
  fprintf(stderr,"\r%d%% Complete\n\n", 100);
  randomizeOrder(order, test_len);
  DBF::cleanupDBF(dbf);
  dbf = DBF::initDBF(isize, retries, tName, dbPath);
  fprintf(stderr,"Testing Insert New and Delete Old\n");
  comp=0;
  marker = test_len/100;
  for(int i=test_len;i<alloc_len;i++){
    if((i-test_len)>comp*marker){
      fprintf(stderr,"\r%d%% Complete", comp);
      comp++;
    }
    assert(DBF::findObject(dbf,
			   nkeys[order[i-test_len]],
			   (int*)test_keys,
			   key_lens[order[i-test_len]],
			   val_lens[order[i-test_len]],
			   keys[order[i-test_len]],
			   vals[order[i-test_len]])!=NULL);


        if(extra){
      for(int t=0;t<nkeys[order[i-test_len]];t++){

	int tkeys = 1;
	key_perms[0] = t;
	assert(DBF::findObject(dbf,
			       tkeys,
			       (int*)key_perms,
			       key_lens[order[i-test_len]],
			       val_lens[order[i-test_len]],
			       keys[order[i-test_len]],
			       vals[order[i-test_len]])!=NULL);

      }
      key_perms[0] = 0;
      for(int t=0;t<perm_num;t++){
	int tkeys= rand()%nkeys[order[i-test_len]];
	tkeys+=(tkeys==0);
	randomizeOrder(key_perms, tkeys);
	assert(DBF::findObject(dbf,
			       tkeys,
			       (int*)key_perms,
			       key_lens[order[i-test_len]],
			       val_lens[order[i-test_len]],
			       keys[order[i-test_len]],
			       vals[order[i-test_len]])!=NULL);

      }
      for(int t=0;t<nkeys[order[i-test_len]];t++){
	key_perms[t]=t;
      }
    }

    assert(DBF::delObject(dbf,
			  nkeys[order[i-test_len]],
					  key_lens[order[i-test_len]],
			  val_lens[order[i-test_len]],
			  keys[order[i-test_len]],
			  vals[order[i-test_len]]));
    if(unique){
      assert(DBF::findObject(dbf,
			     nkeys[order[i-test_len]],
			     (int*)test_keys,
			     key_lens[order[i-test_len]],
			     val_lens[order[i-test_len]],
			     keys[order[i-test_len]],
			     vals[order[i-test_len]])==NULL);
    }

        db_object* obj = DBO::createObject(nkeys[order[i]],
				       key_lens[order[i]],
				       val_lens[order[i]],
				       (unsigned char*)types,
				       keys[order[i]],
				       vals[order[i]]);

    DBF::addObject(dbf, obj);
    assert(DBF::findObject(dbf,
			   nkeys[order[i]],
			   (int*)test_keys,
			   key_lens[order[i]],
			   val_lens[order[i]],
			   keys[order[i]],
			   vals[order[i]])!=NULL);


        if(extra){
      for(int t=0;t<nkeys[order[i]];t++){

	int tkeys = 1;
	key_perms[0] = t;
	assert(DBF::findObject(dbf,
			       tkeys,
			       (int*)key_perms,
			       key_lens[order[i]],
			       val_lens[order[i]],
			       keys[order[i]],
			       vals[order[i]])!=NULL);

      }
      key_perms[0] = 0;
      for(int t=0;t<perm_num;t++){
	int tkeys= rand()%nkeys[order[i]];
	tkeys+=(tkeys==0);
	randomizeOrder(key_perms, tkeys);
	assert(DBF::findObject(dbf,
			       tkeys,
			       (int*)key_perms,
			       key_lens[order[i]],
			       val_lens[order[i]],
			       keys[order[i]],
			       vals[order[i]])!=NULL);

      }
      for(int t=0;t<nkeys[order[i]];t++){
	key_perms[t]=t;
      }
    }

  }

  
  fprintf(stderr,"\r%d%% Complete\n\n", 100);
  randomizeOrder(order+test_len, test_len);
  DBF::cleanupDBF(dbf);
  dbf = DBF::initDBF(isize, retries, tName, dbPath);
  fprintf(stderr,"Testing Delete All\n");
  comp =0;
  for(int i=0;i<test_len;i++){
    if(i>comp*marker){
      fprintf(stderr,"\r%d%% Complete", comp);
      comp++;
    }
    assert(DBF::findObject(dbf,
			   nkeys[order[i+test_len]],
			   (int*)test_keys,
			   key_lens[order[i+test_len]],
			   val_lens[order[i+test_len]],
			   keys[order[i+test_len]],
			   vals[order[i+test_len]])!=NULL);
    assert(DBF::delObject(dbf,
			  nkeys[order[i+test_len]],
					  key_lens[order[i+test_len]],
			  val_lens[order[i+test_len]],
			  keys[order[i+test_len]],
			  vals[order[i+test_len]]));
    if(unique){
      assert(DBF::findObject(dbf,
			     nkeys[order[i+test_len]],
			     (int*)test_keys,
			     key_lens[order[i+test_len]],
			     val_lens[order[i+test_len]],
			     keys[order[i+test_len]],
			     vals[order[i+test_len]])==NULL);
    }
  }
  fprintf(stderr,"\r%d%% Complete\n\n", 100);
  fprintf(stderr,"Testing No Finds\n");
  comp =0;
  marker = alloc_len/100;
  for(int i=0;i<alloc_len;i++){
    if(i>comp*marker){
      fprintf(stderr,"\r%d%% Complete", comp);
      comp++;
    }
    assert(DBF::findObject(dbf,
			   nkeys[order[i]],
			   (int*)test_keys,
			   key_lens[order[i]],
			   val_lens[order[i]],
			   keys[order[i]],
			   vals[order[i]])==NULL);
  }
  fprintf(stderr,"\r%d%% Complete\n\n", 100);
  fprintf(stderr,"All Test Done!\n");
  DBF::cleanupDBF(dbf);
  free(keys);
  free(vals);
  free(key_lens);
  free(val_lens);

}
int main(int argc, char** argv){
  srand(time(NULL));
  trace=(char*)calloc(64,sizeof(char));
  dbPath=(char*)calloc(64,sizeof(char));
  tName=(char*)calloc(64, sizeof(char));
  ArgParser* ap = createArgumentParser(&argp);
  int ok = parseArguments(ap, argc, argv);
  if (ok){
    fprintf(stderr,"Error parsing args\n");
    return -1;
  }
  if(max_nkeys>max_number_keys){
    fprintf(stderr,"less than 31 keys per object please!\n");
    return -1;
  }
  if((!min_val) || (!min_key)){
    fprintf(stderr,"keys and vals must be >= 0\n");
    return -1;
  }
  if((min_val > max_val) || (min_key > max_key)){
    fprintf(stderr,"min vals must be >= max vals\n");
    return -1;
  }
  if(reset){
    if(system("rm -rf ../db/*")==-1){
      fprintf(stderr,"Error reseting db\n");
      return -1;
    }
  }
  min_val = (1<<min_val);
  max_val = (1<<max_val);
  min_key = (1<<min_key);
  max_key = (1<<max_key);

  //adding room for counter to make items unique
  //this it not necessary but makes asserts easier
  if(unique){
    min_val += sizeof(int);
    max_val += sizeof(int);
  }
  
  tests=1<<tests;
  fullTest(tests);
}
